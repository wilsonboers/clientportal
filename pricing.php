<?php
// Config includen om met de MySQL database verbinding te maken
include("config.php");			

if (!is_logged_in()) redirect();
else { 
	$result = $_GET["result"];
	if (isset($result)) $result ="<br><div class=\"alert alert-warning\">$result</div>";

	$query = mysqli_query($mysqli, "select plan, period from plans where uid='".$_SESSION["uidGO"]."' and endDate IS NULL and period!='One-off' and period!='Set-up'") or die(mysqli_error($mysqli));
	$row = mysqli_fetch_array($query);
	$plan = $row["plan"];
	$period = $row["period"];
	
	$res		= mysqli_query($mysqli, "SELECT * FROM users WHERE uid='".$_SESSION['uidGO']."'");
	$myrow 		= mysqli_fetch_array($res);
	$account	= $myrow["account"];	
	$country	= $myrow["country"];
	$contact	= $myrow["contact"];
	$company	= $myrow["company"];
	$vat		= $myrow["vat"];
	$phonenumber= $myrow["phonenumber"];
	$startDay	= $myrow["startDay"];
	$startMonth	= $myrow["startMonth"];
	$endDate	= $myrow["endDate"];
	$currMonth	= date("m");
	$currDay	= date("d");
	$today 	= date("Y-m-d");
		
	if (($currMonth<=$startMonth) && ($currDay<$startDay)) {
		$currYear=date('Y', strtotime("-1 years", strtotime($today)));
		$nextYear=date("Y");
		$nextYearMonth=date("Y");
		$nextMonth=date("m");
		$lastMonth=date('m', strtotime("-1 months", strtotime($today)));
		$lastMonthYear=date('Y', strtotime("-1 months", strtotime($today)));
	}
	else {
		$currYear= date("Y");
		$nextYear=date('Y', strtotime("+1 years", strtotime($today)));
		$nextYearMonth=date('Y', strtotime("+1 months", strtotime($today)));
		$nextMonth = date('m', strtotime("+1 months", strtotime($today)));
		$lastMonth=date("m");
		$lastMonthYear=date("Y");
	}
	

	if ($endDate!="") $reac = 1;
	else $reac = 0;	
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoDashboard</title>
<link rel="shortcut icon" href="favicon.ico" />

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendor/bootstrap-social/bootstrap-social.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<style>
hr {
	margin-top:2px !important;
	margin-bottom:2px !important;
	color:#333 !important;
}
img {
	margin-left:5px;
	margin-top:1px;
}
.borderless td, .borderless th {
    border: none !important;
}
.btn-select {
	min-width:160px !important;
}
.non-highlight {
	top: 5px !important;
}
.col-lg-4 {
	padding-left: 2px !important;
	padding-right: 2px !important;
}
.col-md-4 {
	padding-left: 2px !important;
	padding-right: 2px !important;
}
.currency {
    display: inline-block;
    margin-top: 10px;
    vertical-align: top;
    font-size: 2rem;
    font-weight: 700;
}

.currency {
	padding:0 !important;
}
.duration {
	vertical-align:top !important;
	padding:0 !important;
}
.value {
    font-size: 3rem;
    font-weight: 300;
	vertical-align:bottom !important;
}
/* Skills Progess Bar */
.progress {
	margin: 15px;
}
</style>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>

<body id="page">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="GoDashboardLogo220x50.png">
            </div>
<?php include("topnav.php"); ?>
            <div class="navbar-default sidebar" role="navigation">
<?php include("sidenav.php"); ?>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                                        <div style="width:100%">
 						<?php
                            	echo "<div class=\"modal fade\" id=\"myModalREA\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"top: 31px !important; display: none;\">
						 <form role=\"form\" method=\"post\" action=\"$rea_page\">
							<div class=\"modal-dialog\">
										<div class=\"modal-content\">
											<div class=\"modal-header\">
												<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
												<h4 class=\"modal-title\" id=\"myModalLabel\">"; 
												echo "Are you sure you would like to re-activate your subscription?
												</div>
												<div class=\"modal-footer\">
												<button id=\"submit\" name=\"submit\" class=\"btn btn-primary\" type=\"submit\">Re-activate subscription</button>"; 
												echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button></div>";
										echo "</div>
									</div>
								   </form>
								</div>";
			?>
            			<?php echo $result; ?></div>      
                    &nbsp;
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
			<?php 
            if (($_GET['change']=="success") || ($_GET['pay']=="success")) {
            ?>
            <div class="row">
                    <div class="col-lg-12"> 
                            <div class="panel panel-warning">
                                <div class="panel-heading" style="text-align:center;">
                                Success! Thank you for subscribing with us (again)! You will receive an invoice per e-mail. If you have any questions please contact us!                 
                                </div>
                            </div>                
                    </div>
            </div>
            <?php
            }                      
            ?>                    
              <div class="row">                        
              <div class="col-lg-12" align="center">    
                    <div class="btn-group" data-toggle="buttons">  
					  <label class="btn btn-default <?php if (($period=="year") || ($period=="")) echo "active"; ?>"><input type="radio" name="options" id="yearly" autocomplete="off" <?php if (($period=="year") || ($period=="")) echo "checked"; ?>> Yearly</label> 
                      <label class="btn btn-default <?php if ($period=="month") echo "active"; ?>"><input type="radio" name="options" id="monthly" autocomplete="off" <?php if ($period=="month") echo "checked"; ?>> Monthly</label>
                    </div>
                    <br><br>
               </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
               <!-- /progressbar when account is changed or payed -->
                          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 51px !important; display: none;">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <h4 class="modal-title" id="myModalLabel">In Progress</h4>
                                  </div>
                                  <div class="modal-body">
                                  <div class="progress">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar"
                                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                        </div>
                                      </div>
                                      
                                  </div>
                              </div>
                              <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->
                </div>
                <!-- /.col-lg-12 -->
            </div>                     
            <div class="row">
             <!-- start pricing options FRONT -->               
              <div class="month" <?php if (($period=="year") || ($period=="")) echo "hidden"; ?>>             
              <div class="col-lg-12"> 	
               <!-- SHOW PRICE ITEM -->				
				<?php 
				if (($period=="month") and ($plan=="STARTER")) {
                	echo "<div class=\"col-md-4 col-lg-4\">";
				}
                else { 
                	echo "<div class=\"col-md-4 col-lg-4 non-highlight\">";
				} 
				?>
						<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>STARTER</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs1Large,0); ?></span>
								<span class="duration">/mo</span>
							</div>
                            <hr>
                            <p class="text-muted"><small>€<?php echo round($costs5Large/12,0); ?> /mo (when billed annually)</small></p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 5 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 5 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 1 hour</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support: e-mail</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> No reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> 2 Data Insights</li>
						</ul>
						<div class="panel-footer text-center paybuttons" id="starter_month">
                        <?php
						if ($period=="month") {
							if (($plan==NULL) or ($plan!="STARTER")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_starter\">ACTIVATE</a>";
							elseif ($plan=="STARTER") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_starter\">ACTIVATE</a>";
						 ?>                            
                        </div>
                        <div class="panel-footer text-center payplan" id="price_starter_month" hidden>
                        <!--payment option-->
                        <?php 
                        	$act_plan 	= "STARTER";
							$act_period = "month";						
							include("activate.php"); 
						?>
                        <!-- /.payment option -->                        
                        </div>
					</div>
				</div>					
                <!-- /SHOW PRICE ITEM -->                     				
                <!-- SHOW PRICE ITEM -->				
				<?php 
				if (($period=="month") and ($plan=="PRO")) {
                	echo "<div class=\"col-md-4 col-lg-4\">";
				}
                else { 
                	echo "<div class=\"col-md-4 col-lg-4 non-highlight\">";
				} 
				?>
						<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>PRO</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs3Large,0); ?></span>
								<span class="duration">/mo</span>
							</div>
                            <hr>
                            <p class="text-muted"><small>€<?php echo round($costs7Large/12,0); ?> /mo (when billed annually)</small></p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 10 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 10 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 2 hours</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support: e-mail with priority</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Enhanced reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> 3 Data Insights</li>
						</ul>
						<div class="panel-footer text-center paybuttons" id="pro_month">
                        <?php
						if ($period=="month") {
							if (($plan==NULL) or ($plan!="PRO")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_pro\">ACTIVATE</a>";
							elseif ($plan=="PRO") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_pro\">ACTIVATE</a>";
						 ?>                            
                        </div>
                        <div class="panel-footer text-center payplan" id="price_pro_month" hidden>
                        <!--payment option-->
                        <?php 
                        	$act_plan 	= "PRO";
							$act_period = "month";						
							include("activate.php"); 
						?>
                        <!-- /.payment option -->                        
                        </div>
					</div>
				</div>					
                <!-- /SHOW PRICE ITEM -->
				<!-- SHOW PRICE ITEM -->                
				<?php 
				if (($period=="month") and ($plan=="PREMIUM")) {
                	echo "<div class=\"col-md-4 col-lg-4\">";
				}
                else { 
                	echo "<div class=\"col-md-4 col-lg-4 non-highlight\">";
				} 
				?>
						<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>ULTIMATE</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs4Large,0); ?></span>
								<span class="duration">/mo</span>
							</div>
                            <hr>
                            <p class="text-muted"><small>€<?php echo round($costs8Large/12,0); ?> /mo (when billed annually)</small></p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 25 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 25 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 4 hours</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support:  dedicated manager</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Enhanced reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> 5 Data Insights</li>
						</ul>
						<div class="panel-footer text-center paybuttons" id="premium_month">
						<?php
						if ($period=="month") {
							if (($plan==NULL) or ($plan!="PREMIUM")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_premium\">ACTIVATE</a>";
							elseif ($plan=="PREMIUM") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_premium\">ACTIVATE</a>";
                     	?>
                        </div>
                        <div class="panel-footer text-center payplan" id="price_premium_month" hidden>
						<!--payment option-->
                        <?php 
                        	$act_plan 	= "PREMIUM";
							$act_period = "month";						
							include("activate.php"); 
						?>
                        <!-- /.payment option -->                        
                        </div>
					</div>                   
                  </div>
				  <!-- /SHOW PRICE ITEM -->
                  </div>
                </div>                  
<!-- start pricing options BACK --> 
                <div class="year" <?php if ($period=="month") echo "hidden"; ?>>                          
                <div class="col-lg-12">                
               <!-- SHOW PRICE ITEM -->				
				<?php 
				if (($period=="year") and ($plan=="STARTER")) {
                	echo "<div class=\"col-md-4 col-lg-4\">";
				}
                else { 
                	echo "<div class=\"col-md-4 col-lg-4 non-highlight\">";
				} 
				?>
						<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>STARTER</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs5Large/12,0); ?></span>
								<span class="duration">/mo</span>
							</div>
                            <hr>
                            <p class="text-muted"><small>(billed annually)</small></p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 5 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 5 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 1 hour</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support: e-mail</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> No reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> 2 Data Insights</li>
						</ul>
						<div class="panel-footer text-center paybuttons" id="starter_year">
                        <?php
						if ($period=="year") {
							if (($plan==NULL) or ($plan!="STARTER")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_starter\">ACTIVATE</a>";
							elseif ($plan=="STARTER") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_starter\">ACTIVATE</a>";
						 ?>                            
                        </div>
                        <div class="panel-footer text-center payplan" id="price_starter_year" hidden>
                        <!--payment option-->
                        <?php 
                        	$act_plan 	= "STARTER";
							$act_period = "year";						
							include("activate.php"); 
						?>
                        <!-- /.payment option -->                        
                        </div>
					</div>
				</div>					
                <!-- /SHOW PRICE ITEM -->               			
                <!-- SHOW PRICE ITEM -->				
				<?php 
				if (($period=="year") and ($plan=="PRO")) {
                	echo "<div class=\"col-md-4 col-lg-4\">";
				}
                else { 
                	echo "<div class=\"col-md-4 col-lg-4 non-highlight\">";
				} 
				?>
						<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>PRO</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs7Large/12,0); ?></span>
								<span class="duration">/mo</span>
							</div>
                            <hr>
                            <p class="text-muted"><small>(billed annually)</small></p>
						</div>
						<ul class="list-group list-group-flush text-center" >
							<li class="list-group-item"><i class="icon-ok text-info"></i> 10 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 10 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 2 hours</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support: e-mail with priority</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Enhanced reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> 3 Data Insights</li>
						</ul>
						<div class="panel-footer text-center paybuttons" id="pro_year">
                        <?php
						if ($period=="year") {
							if (($plan==NULL) or ($plan!="PRO")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_pro\">ACTIVATE</a>";
							elseif ($plan=="PRO") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_pro\">ACTIVATE</a>";
						 ?>                            
                        </div>
                        <div class="panel-footer text-center payplan" id="price_pro_year" hidden> 
                        <!--payment option-->
                        <?php 
                        	$act_plan 	= "PRO";
							$act_period = "year";						
							include("activate.php"); 
						?>
                        <!-- /.payment option -->
						</div>	
					</div>
				</div>
				<!-- /SHOW PRICE ITEM -->   
				<!-- SHOW PRICE ITEM -->   
				<?php 
				if (($period=="year") and ($plan=="PREMIUM")) {
                	echo "<div class=\"col-md-4 col-lg-4\">";
				}
                else { 
                	echo "<div class=\"col-md-4 col-lg-4 non-highlight\">";
				} 
				?>
						<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>ULTIMATE</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs8Large/12,0); ?></span>
								<span class="duration">/mo</span>
							</div>
                            <hr>
                            <p class="text-muted"><small>(billed annually)</small></p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 25 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 25 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 4 hours</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support:  dedicated manager</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Enhanced reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> 5 Data Insights</li>
						</ul>
						<div class="panel-footer text-center paybuttons" id="premium_year">
                        <?php
                  		if ($period=="year") {
							if (($plan==NULL) or ($plan!="PREMIUM")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_premium\">ACTIVATE</a>";
							elseif ($plan=="PREMIUM") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_premium\">ACTIVATE</a>";
						?>
                        </div>
                        <div class="panel-footer text-center payplan" id="price_premium_year" hidden>
                        <!--payment option-->
                        <?php 
                        	$act_plan 	= "PREMIUM";
							$act_period = "year";						
							include("activate.php"); 
						?>
                        <!-- /.payment option -->	
                        </div>				
                      </div>                
                  </div>
				  <!-- /SHOW PRICE ITEM -->
           		</div>
               </div>			          
			</div>
			<!-- einde pricing -->
            <br>
			<div class="row">
                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            History and current plan
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
 									<?php 	
                                        $query = mysqli_query($mysqli, "select * from plans where uid='".$_SESSION["uidGO"]."' order by id DESC") or die(mysqli_error($mysqli));
                                        $meerdan = mysqli_num_rows($query);
                                        if ($meerdan > 0) {
											echo "<thead>
												<tr>
													<th>Plan</th>
													<th>Period</th>
													<th>Start date</th>
													<th>End date</th>
													<th>Amount</th>
												</tr>
											</thead>";										
                                            while ($row = mysqli_fetch_array($query)) {
												$plan = $row["plan"];
												$period = $row["period"];
                                                $startDate = $row["startDate"];
												$startDate = date("d M Y", strtotime($startDate));
												$endDate = $row["endDate"];
												if ($endDate!=NULL) $endDate = date("d M Y", strtotime($endDate));
												$amount = $row["amount"];

                                                echo "<tr>
                                                       <td>$plan</td>
                                                       <td>$period</td>
													   <td>$startDate</td>
													   <td>$endDate</td>
													   <td>€ $amount</td>
                                                      </tr>";
                                            }
                                        }
										else echo "<tr>
                                                       <td>You currently have a trial account and have all options available as described in the Ultimate plan, with exception of DataGuru consult.</td>
												   </tr>";
                                    ?>                                   
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                </div>
            </div> 
		</div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Flot Charts JavaScript -->
    <script src="../vendor/flot/excanvas.min.js"></script>
    <script src="../vendor/flot/jquery.flot.js"></script>
    <script src="../vendor/flot/jquery.flot.pie.js"></script>
    <script src="../vendor/flot/jquery.flot.resize.js"></script>
    <script src="../vendor/flot/jquery.flot.time.js"></script>
    <script src="../vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>
    <!-- <script src="../data/flot-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <script>
	function myTimeout10() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '10%');}, 500); // WAIT 1 second
		  return(myVar);
	}
	function myTimeout40() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '40%');}, 500); // WAIT 1 second
		  return(myVar);
	}
	function myTimeout90() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '90%');}, 500); // WAIT 1 second
		  return(myVar);
	}
	function postPayment(confirm) {
		myTimeout10();
		myTimeout40();
		myTimeout90();
		
		$(':button').prop('disabled', true); // Disable all the buttons
		$('a').attr('disabled', true);
  		$('#myModal').modal("show");
		
		//check which pricing was confirmed
		if (confirm=="confirm1") {
	    	var costs=$('#costs1').val();	
			var costs_after=$('#costs_after1').val();
			var date=$('#date1').val();
			var plan=$('#plan1').val();
			var period=$('#period1').val();
			var rec=$('#rec1').val();
		}
		else if (confirm=="confirm2") {
	    	var costs=$('#costs2').val();	
			var costs_after=$('#costs_after2').val();
			var date=$('#date2').val();
			var plan=$('#plan2').val();
			var period=$('#period2').val();
			var rec=$('#rec2').val();
		}
		else if (confirm=="confirm3") {
	    	var costs=$('#costs3').val();	
			var costs_after=$('#costs_after3').val();
			var date=$('#date3').val();
			var plan=$('#plan3').val();
			var period=$('#period3').val();
			var rec=$('#rec3').val();
		}
		else if (confirm=="confirm4") {
	    	var costs=$('#costs4').val();	
			var costs_after=$('#costs_after4').val();
			var date=$('#date4').val();
			var plan=$('#plan4').val();
			var period=$('#period4').val();
			var rec=$('#rec4').val();
		}
		else if (confirm=="confirm5") {
	    	var costs=$('#costs5').val();	
			var costs_after=$('#costs_after5').val();
			var date=$('#date5').val();
			var plan=$('#plan5').val();
			var period=$('#period5').val();
			var rec=$('#rec5').val();
		}
		else if (confirm=="confirm6") {
	    	var costs=$('#costs6').val();	
			var costs_after=$('#costs_after6').val();
			var date=$('#date6').val();
			var plan=$('#plan6').val();
			var period=$('#period6').val();
			var rec=$('#rec6').val();
		}
		else if (confirm=="confirm7") {
	    	var costs=$('#costs7').val();	
			var costs_after=$('#costs_after7').val();
			var date=$('#date7').val();
			var plan=$('#plan7').val();
			var period=$('#period7').val();
			var rec=$('#rec7').val();
		}
		else if (confirm=="confirm8") {
	    	var costs=$('#costs8').val();	
			var costs_after=$('#costs_after8').val();
			var date=$('#date8').val();
			var plan=$('#plan8').val();
			var period=$('#period8').val();
			var rec=$('#rec8').val();
		}
		else if (confirm=="confirm9") {
	    	var costs=$('#costs9').val();	
			var costs_after=$('#costs_after9').val();
			var date=$('#date9').val();
			var plan=$('#plan9').val();
			var period=$('#period9').val();
			var rec=$('#rec9').val();
		}
		else if (confirm=="confirm10") {
	    	var costs=$('#costs10').val();	
			var costs_after=$('#costs_after10').val();
			var date=$('#date10').val();
			var plan=$('#plan10').val();
			var period=$('#period10').val();
			var rec=$('#rec10').val();
		}

		$.post( 
			"plans/change.php",
			{ uid: <?php echo $_SESSION['uidGO']; ?>,
			  account: '<?php echo $account; ?>',
			  costs: costs,
			  costs_after: costs_after,
			  date: date,
			  plan: plan,
			  period: period,
			  rec: rec },
			function(data) {
				//alert(data);
				console.log(data);
				if ( data.length !== 0 ) {
					window.location = 'pricing.php?change=success';
				}
				//show error
				else {
					window.location = 'pricing.php?change=error';
				}
			}
		 );
	}
	
	$(document).ready(function(){
	  $("#confirm1").click(function(){
		postPayment('confirm1');			
	  });
	  $("#confirm2").click(function(){
		postPayment('confirm2');			
	  });
	  $("#confirm3").click(function(){
		postPayment('confirm3');			
	  });
	  $("#confirm4").click(function(){
		postPayment('confirm4');			
	  });
	  $("#confirm5").click(function(){
		postPayment('confirm5');			
	  });
	  $("#confirm6").click(function(){
		postPayment('confirm6');			
	  });
	  $("#confirm7").click(function(){
		postPayment('confirm7');			
	  });
	  $("#confirm8").click(function(){
		postPayment('confirm8');			
	  });
	  $("#confirm9").click(function(){
		postPayment('confirm9');			
	  });
	  $("#confirm10").click(function(){
		postPayment('confirm10');			
	  });
	  
	  $(".cancel").click(function(){
		$('.paybuttons').fadeIn();
		$('.payplan').hide();						
	  });
      $("input[id='monthly']").change(function(){
		$('.year').hide();        
		$('.month').fadeIn();        
	  });
	  $("input[id='yearly']").change(function(){
		$('.month').hide();           
		$('.year').fadeIn();
      });
	  $("#month_starter").click(function(){
		$('.paybuttons').fadeIn();
		$('#starter_month').hide();
		$('.payplan').hide();		          
		$('#price_starter_month').fadeIn();		
      });
	  $("#month_pro").click(function(){
		$('.paybuttons').fadeIn();
		$('#pro_month').hide();
		$('.payplan').hide();					
		$('#price_pro_month').fadeIn(); 		
      });
	  $("#month_premium").click(function(){
		$('.paybuttons').fadeIn();
		$('#premium_month').hide();
		$('.payplan').hide();			
		$('#price_premium_month').fadeIn(); 		
      });
	  $("#year_starter").click(function(){
		$('.paybuttons').fadeIn();
		$('#starter_year').hide();
		$('.payplan').hide();
		$('#price_starter_year').fadeIn(); 		
      });
	  $("#year_pro").click(function(){
		$('.paybuttons').fadeIn();		
		$('#pro_year').hide();
		$('.payplan').hide();
		$('#price_pro_year').fadeIn(); 		
      });
	  $("#year_premium").click(function(){
		$('.paybuttons').fadeIn();			
		$('#premium_year').hide();
		$('.payplan').hide();
		$('#price_premium_year').fadeIn(); 		
      });
	});
    </script>
</body>
</html>
<?php } ?>