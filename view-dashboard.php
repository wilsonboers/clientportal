<?php
include("config.php");

echo "<script src=\"../vendor/jquery/jquery.min.js\"></script>";
		
if (!is_logged_in()) redirect();
else {
  $redirect=base64_decode($_GET["d"]);
  
  $token = sso($_SESSION['unameGO'],"https://godashboard.nl/go/client/end.php");
  $klipfolioCompanyId = "2739cb1f0c6c0c6c0950b5d3533fcb0f";// Your Klipfolio Company ID
		
  //sso klipfolio
  echo "<script>
  function initSSO(url){
		$.ajax({	 
			url : \"https://account.godashboard.nl/users/sso_auth\",
			type: \"post\",
			xhrFields:{
				withCredentials:true
			},
			headers:{
				\"KF-SSO\":\"$token\",
				\"KF-Company\":\"$klipfolioCompanyId\"
			},
			dataType:\"json\",
		   
			// on success, the data object will contain information about
			// the authenticated user
			// ----------------------------------------------------------
			success : function(data){
				window.location.href = '$redirect';
			},
		 
			// on error, err.responseJSON will contain an error code
			// -----------------------------------------------------
			error : function(err){
				window.location.href = 'logout.php';
			}
		})
	};	
	
	//sso op achtergrond herstellen en redirect naar index
	$(initSSO());
	
	</script>";
}

?>