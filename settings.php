<?php
// Config includen om met de MySQL database verbinding te maken
include("config.php");			

if (!is_logged_in()) redirect();
else {
	$result = $_GET["result"];
	if (isset($result)) $result ="<br><div class=\"alert alert-warning\">$result</div>";
	
	if ((isset($_POST["submit"])) or (isset($_POST["save"]))) {
		$password		= $_POST["password"];
		$password2		= $_POST["password2"];		
		$contact		= $_POST["contact"];		
		$company		= $_POST["company"];		
		$country		= $_POST["country"];		
		$vat			= $_POST["vat"];
		$phonenumber	= $_POST["phonenumber"];
		$email			= $_POST["email"];			
		$website		= $_POST["website"];		
			
		if ($password!="") {
			if ($password==$password2) {
				$password = md5($password);
				mysqli_query($mysqli, "UPDATE users SET country='$country', 
													contact='$contact',
													company='$company',
													vat='$vat',
													phonenumber='$phonenumber',													
													website='$website',
													password='$password',
													email='$email' WHERE uid='".$_SESSION['uidGO']."'");
				$result = "<br><div class=\"alert alert-success\">Settings updated.</div>";
			}
			else {
				mysqli_query($mysqli, "UPDATE users SET country='$country', 
													contact='$contact',
													company='$company',
													vat='$vat',
													phonenumber='$phonenumber',													
													website='$website',
													password='$password',
													email='$email' WHERE uid='".$_SESSION['uidGO']."'");
				$result = "<br><div class=\"alert alert-danger\">Settings updated. Passwords did not match and have not been saved!</div>";
			}
		}
		else {
			mysqli_query($mysqli, "UPDATE users SET country='$country', 
													contact='$contact',
													company='$company',
													vat='$vat',
													phonenumber='$phonenumber',													
													website='$website',
													email='$email' WHERE uid='".$_SESSION['uidGO']."'");
				$result = "<br><div class=\"alert alert-success\">Settings updated.</div>";
		}
	}

	$res		= mysqli_query($mysqli, "SELECT * FROM users WHERE uid='".$_SESSION['uidGO']."'");
	$myrow 			= mysqli_fetch_array($res);
	$uid			= $myrow["uid"];
	$account		= $myrow["account"]; //free, dev, pro
	$uname			= $myrow["uname"];	
	$country		= $myrow["country"];
	$contact		= $myrow["contact"];
	$company		= $myrow["company"];
	$vat			= $myrow["vat"];
	$phonenumber	= $myrow["phonenumber"];
	$website		= $myrow["website"];	
	$email			= $myrow["email"];
	$a_check		= $myrow["access"];

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoDashboard</title>
<link rel="shortcut icon" href="favicon.ico" />

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendor/bootstrap-social/bootstrap-social.css" rel="stylesheet">
    
    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
img {
	margin-left:5px;
	margin-top:1px;
}
</style>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="GoDashboardLogo220x50.png">
            </div>
<?php include("topnav.php"); ?>
            <div class="navbar-default sidebar" role="navigation">
<?php include("sidenav.php"); ?>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    &nbsp;
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-body">
                        <div style="width:100%">
						<?php
                            	echo "<div class=\"modal fade\" id=\"myModalREA\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"top: 31px !important; display: none;\">
						 <form role=\"form\" method=\"post\" action=\"$rea_page\">
							<div class=\"modal-dialog\">
										<div class=\"modal-content\">
											<div class=\"modal-header\">
												<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
												<h4 class=\"modal-title\" id=\"myModalLabel\">"; 
												echo "Are you sure you would like to re-activate your subscription?
												</div>
												<div class=\"modal-footer\">
												<button id=\"submit\" name=\"submit\" class=\"btn btn-primary\" type=\"submit\">Re-activate subscription</button>"; 
												echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button></div>";
										echo "</div>
									</div>
								   </form>
								</div>";
			?>      
			<?php echo $result; ?></div>
 							<form role="form" action="settings.php" method="post" id="myForm">                       
                            <div class="row">                        
                            <div class="col-lg-12">    
                                  
                         			<div class="form-group">
                                        <label>Username</label>
                                        <p class="form-control-static"><?php echo $uname ?></p>
                                    </div>
                                    <div class="form-group">
                                        <label>New password</label>
                                        <input class="form-control" type="password" name="password" id="password">
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm new password</label>
                                        <input class="form-control" type="password" name="password2" id="password2">
                                    </div>                                                                     
                                    <div class="form-group">
                                        <label>Contact name</label>
                                        <input class="form-control" name="contact" id="contact" value="<?php echo $contact ?>" required>
                                    </div>                                    
                                    <div class="form-group">
                                        <label>Company</label>
                                        <input class="form-control" name="company" id="company" value="<?php echo $company ?>">
                                    </div> 
                                        <div class="form-group">
                                            <label>Country</label>
                  							<?php include("sel-country.php"); ?>
                                        </div>      
                                    <div class="form-group">
                                        <label>VAT</label>
                                        <input class="form-control" name="vat" id="vat" value="<?php echo $vat ?>">
                                    </div>                              
                                    <div class="form-group">
                                        <label>Phonenumber</label>
                                        <input class="form-control" name="phonenumber" id="phonenumber" value="<?php echo $phonenumber ?>">
                                    </div> 
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input class="form-control" name="email" id="email" value="<?php echo $email ?>" required>
                                    </div>                                    
                                    <div class="form-group">
                                        <label>Website</label>
                                        <input class="form-control" name="website" id="website" value="<?php echo $website ?>">
                                    </div>
                                    <button type="submit" class="btn btn-success" id="save" name="save">Save</button>

                                </div>
                                
                              </div>
                          </form>                          
                         </div>
                        <!-- /.panel-footer -->
                    </div>
                    <!-- /.panel .chat-panel -->
                </div>
                          <div class="col-lg-4">
          <div class="panel panel-default">
<?php 
$query = mysqli_query($mysqli, "select account, registration, startDay, endDate, reactivated from users where uid='".$_SESSION["uidGO"]."'") or die(mysqli_error($mysqli));
$row = mysqli_fetch_array($query);
$account = $row["account"];
$startDay = $row["startDay"];
$endDate = $row["endDate"];
$reactivated = $row["reactivated"];

if ($account=="FREE") { 
	$registration = strtotime($row["registration"]);
	$now = time(); // or your date as well
	$datediff = $now - $registration;
	$exp = 14 - round($datediff / (60 * 60 * 24));
?>
          <div class="panel-heading">
             <?php echo "$exp"; ?> days trial left
          </div>
          <div class="panel-body">
          <a href="pricing.php" class="btn btn-block btn-primary">VIEW PRICING</a>
 </div>
<?php
}   
elseif ($endDate!="") { 
	$registration = strtotime($row["endDate"]);
	$now = time(); // or your date as well
	$datediff = $registration - $now;
	$exp = round($datediff / (60 * 60 * 24));
?>
          <div class="panel-heading">
             <?php echo "$exp"; ?> days left
          </div>
          <div class="panel-body">
<?php
		  $rea_page = "plans/reactivate.php?p=".basename($_SERVER['PHP_SELF'])."";	 
		  echo "
					   <button class=\"btn btn-block btn-primary\" data-toggle=\"modal\" data-target=\"#myModalREA\">
									RE-ACTIVATE ACCOUNT
								</button>
								<div class=\"modal fade\" id=\"myModalREA\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"top: 31px !important; display: none;\">
						 <form role=\"form\" method=\"post\" action=\"$rea_page\">
							<div class=\"modal-dialog\">
										<div class=\"modal-content\">
											<div class=\"modal-header\">
												<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
												<h4 class=\"modal-title\" id=\"myModalLabel\">"; 
												echo "Are you sure you would like to re-activate your subscription?
												</div>
												<div class=\"modal-footer\">
												<button id=\"submit\" name=\"submit\" class=\"btn btn-primary\" type=\"submit\">Re-activate subscription</button>"; 
												echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button></div>";
										echo "</div>
									</div>
								   </form>
								</div>";
			?>      
 </div>
<?php
}
else {
	$query = mysqli_query($mysqli, "select period from plans where uid='".$_SESSION["uidGO"]."' and endDate IS NULL and period!='One-off' and period!='Set-up'") or die(mysqli_error($mysqli));
	$row = mysqli_fetch_array($query);
	$period = $row["period"];
	
	$qy = mysqli_query($mysqli, "SELECT startDay,startMonth FROM users WHERE uid='".$_SESSION["uidGO"]."'");
	$gegevens = mysqli_fetch_array($qy);
	   
	$startDay = $gegevens['startDay'];
	$startMonth = $gegevens['startMonth'];
	$today = date("d");
	$tomonth = date("m");
	$date = date("Y-m-d");
	if ($period=="month") {
		if ($today>=$startDay) $month = date('M Y', strtotime("+1 months", strtotime($date)));
		else $month = date("M Y");
		
		$renewDate = "".$startDay." ".$month."";
	}
	else {
		if (($today>=$startDay) and ($tomonth>=$startMonth)) $Y = date('Y', strtotime("+1 years", strtotime($date)));
		else $Y = date("Y");
		
		$month = date('M', strtotime("$Y-$startMonth-01"));
		
		$renewDate = "".$startDay." ".$month." $Y";
	}
	
	$end_page = "plans/end.php?p=".basename($_SERVER['PHP_SELF'])."";	
?>
          <div class="panel-heading">
               Subscribed <em><small>(renewal on <?php echo $renewDate ?>)</small></em>
          </div>
          <div class="panel-body">
                    <a href="pricing.php" class="btn btn-block btn-primary">UPGRADE / DOWNGRADE</a><br>
<?php             
							echo "
					   <button class=\"btn btn-block btn-default\" data-toggle=\"modal\" data-target=\"#myModalEND\">
									End subscription
								</button>
								<div class=\"modal fade\" id=\"myModalEND\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"top: 31px !important; display: none;\">
						 <form role=\"form\" method=\"post\" action=\"$end_page\">
							<div class=\"modal-dialog\">
										<div class=\"modal-content\">
											<div class=\"modal-header\">
												<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
												<h4 class=\"modal-title\" id=\"myModalLabel\">"; 
												echo "Are you sure you would like to end your subscription?<br>This will take effect $renewDate
												</div>
												<div class=\"modal-footer\">
												<button id=\"submit\" name=\"submit\" class=\"btn btn-danger\" type=\"submit\">End subscription</button>"; 
												echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button></div>";
										echo "</div>
									</div>
								   </form>
								</div>";      
?>      
            
                                     
		  </div>
<?php
}     
?>  
             </div>                           
          </div>      
                <!-- /.col-lg-4 -->
            </div>				
					
				</div>								
					
				</div>
			<!-- einde pricing -->            

            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
<?php } ?>