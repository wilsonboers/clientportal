<?php 
// Config includen om met de MySQL database verbinding te maken
include("../config.php");
require_once '../plans/class.phpmailer.php';
require_once '../plans/class.smtp.php';

// If you're using Composer, use Composer's autoload:
require_once('../../stripe/init.php');

\Stripe\Stripe::setApiKey("sk_live_IyZCez0TNnkBlak8k5qvVXvJ");

// Retrieve the request's body and parse it as JSON
$input = @file_get_contents("php://input");
$event_json = json_decode($input);

// Verify the event by fetching it from Stripe
$event = \Stripe\Event::retrieve($event_json->id);

// Do something with $event
$sourceid = $event["data"]["object"]["id"];
$client_secret = $event["data"]["object"]["client_secret"];
$status = $event["data"]["object"]["status"];

$qy = mysqli_query($mysqli,"SELECT * FROM renew_payment_ideal WHERE sourceid='$sourceid' and client_secret='$client_secret' LIMIT 1");
$count = mysqli_num_rows($qy);
//if it's not a renewal payment exit
if ($count!=1) exit;

if ($status=="chargeable") {
	$qy = mysqli_query($mysqli, "SELECT * FROM stripe_ideal WHERE sourceid='$sourceid' and client_secret='$client_secret' and active='0'") or die(mysqli_error($mysqli));
	$count = mysqli_num_rows($qy);
	
	if ($count==1) {		
		$gegevens = mysqli_fetch_array($qy);		
		$user = $gegevens['user']; 	  
		$s = $gegevens['s']; 	  
		$p = $gegevens['p'];
		$amount = $gegevens['amount'];

		try
		{
			$charge = \Stripe\Charge::create(array(
			  "amount" => $amount,
			  "currency" => "eur",
			  "source" => "$sourceid",
			));

			mysqli_query($mysqli,"UPDATE stripe_ideal SET active='1' WHERE sourceid='$sourceid' and client_secret='$client_secret'");
			
			mysqli_query($mysqli,"UPDATE renew_payment_ideal SET payed='1' WHERE sourceid='$sourceid' and client_secret='$client_secret'");	
			
			//stuur e-mail
			$subject = "Account verlengd, betaling via ideal.";
			$message = "User: $user<br>	
				Betaald: ".number_format(round($amount/100,2),2)."";
			
			$mail_office = new PHPMailer();
			$mail_office->IsSMTP();
			// set smtp login
			$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
			// $mail->SMTPDebug  = 2;
			$mail_office->SMTPAuth = true;	  // enable SMTP authentication
			$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
			$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
			$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
			$mail_office->Password = "e9DcdmHE!"; // SMTP account password
			$mail_office->IsHTML();
			$mail_office->SetFrom('noreply@godashboard.nl', 'GoDashboard');
			
			$mail_office->AddAddress("dr@godashboard.nl");
			$mail_office->Subject = $subject;
			$mail_office->Body = $message;
			
			$mail_office->Send();				  			  		
		}
		catch(Exception $e)
		{
		  mysqli_query($mysqli, "INSERT INTO stripe_errors (email,message) VALUES ('$user','".$e->getMessage()."')") or die(mysqli_error($mysqli));
		}
	}
}
elseif (($status=="canceled") || ($status=="failed")) {
	$qy = mysqli_query($mysqli, "SELECT * FROM stripe_ideal WHERE sourceid='$sourceid' and client_secret='$client_secret' and active='0'") or die(mysqli_error($mysqli));
	$count = mysqli_num_rows($qy);
	
	if ($count==1) {
		mysqli_query($mysqli,"UPDATE stripe_ideal SET active='1', failed='1' WHERE sourceid='$sourceid' and client_secret='$client_secret'");	
	}
}
?>