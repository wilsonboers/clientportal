<?php 
// Config includen om met de MySQL database verbinding te maken
include("../config.php");
require_once '../plans/class.phpmailer.php';
require_once '../plans/class.smtp.php';

$b64 = base64_decode($_GET['s']);
$qy = mysqli_query($mysqli, "SELECT setup FROM paylog WHERE uid='".$_SESSION["uidGO"]."' and s='$b64' ORDER BY id DESC") or die(mysqli_error($mysqli));
$gegevens = mysqli_fetch_array($qy);
	
$setup = $gegevens['setup']; 	  
$setup_ct = round(100*$setup,0); //kosten in centen

if (base64_decode($_GET['s'])==1) {
	$plan = "monthstarter"; //name of plan
	$name = "STARTER";
	$period = "month";
	$costs = "$costs_starter_month";
	$abonnement = "Starter (maand)";
}
elseif (base64_decode($_GET['s'])==2) {
	$plan = "monthvalue";
	$name = "VALUE";
	$period = "month";
	$costs = "$costs_value_month";
	$abonnement = "Starter (maand)";
}
elseif (base64_decode($_GET['s'])==3) {
	$plan = "monthpro";
	$name = "PRO";
	$period = "month";
	$costs = "$costs_pro_month";
	$abonnement = "Professional (maand)";
}
elseif (base64_decode($_GET['s'])==4) {
	$plan = "monthpremium"; //ultimate
	$name = "PREMIUM";
	$period = "month";
	$costs = "$costs_premium_month";
	$abonnement = "Ultimate (maand)";
}
elseif (base64_decode($_GET['s'])==5) {
	$plan = "yearstarter";
	$name = "STARTER";
	$period = "year";
	$costs = "$costs_starter_year";
	$abonnement = "Starter (jaar)";
}
elseif (base64_decode($_GET['s'])==6) {
	$plan = "yearvalue";
	$name = "VALUE";
	$period = "year";
	$costs = "$costs_value_year";
	$abonnement = "Starter (jaar)";
}
elseif (base64_decode($_GET['s'])==7) {
	$plan = "yearpro";
	$name = "PRO";
	$period = "year";
	$costs = "$costs_pro_year";
	$abonnement = "Professional (jaar)";
}
elseif (base64_decode($_GET['s'])==8) {
	$plan = "yearpremium"; //ultimate
	$name = "PREMIUM";
	$period = "year";
	$costs = "$costs_premium_year";
	$abonnement = "Ultimate (jaar)";
}

// If you're using Composer, use Composer's autoload:
require_once('../../stripe/init.php');

// Be sure to replace this with your actual test API key
// (switch to the live key later)
\Stripe\Stripe::setApiKey("sk_live_IyZCez0TNnkBlak8k5qvVXvJ");

try
{
  $customer = \Stripe\Customer::create(array(
    'email' => $_POST['stripeEmail'],
    'source'  => $_POST['stripeToken'],
	'plan' => $plan,            
	'account_balance' => $setup_ct,
    'description' => 'GODASHBOARD'
  ));
  
  //$logData = serialize($customer);
  //mysqli_query($mysqli, "INSERT INTO log (log) VALUES ('$logData')");
  
  $customerid = $customer["id"];
  $subscriptionid = $customer["subscriptions"]["data"][0]["id"];
  
  mysqli_query($mysqli, "INSERT INTO stripe_subscriptions (email,token,subscriptionid) VALUES ('".$_POST['stripeEmail']."','".$_POST['stripeToken']."','$subscriptionid')") or die(mysqli_error($mysqli));
  $subid = mysqli_insert_id($mysqli);
  
  mysqli_query($mysqli,"UPDATE users SET account='stripe',customerid_stripe='$customerid',subid='$subid',startDay='".date("d")."',startMonth='".date("m")."' WHERE uid='".$_SESSION["uidGO"]."'");

  mysqli_query($mysqli, "INSERT INTO plans (uid,plan,period,startDate,amount) VALUES ('".$_SESSION["uidGO"]."','$name','Set-up','".date("Y-m-d")."','$setup')") or die(mysqli_error($mysqli));

  mysqli_query($mysqli, "INSERT INTO plans (uid,plan,period,startDate,amount) VALUES ('".$_SESSION["uidGO"]."','$name','$period','".date("Y-m-d")."','$costs')") or die(mysqli_error($mysqli));


	//stuur e-mail
	$subject = "Payment by stripe";
	$message = "User: ".$_SESSION["uidGO"]."";
	
	$mail_office = new PHPMailer();
	$mail_office->IsSMTP();
	// set smtp login
	$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
	// $mail->SMTPDebug  = 2;
	$mail_office->SMTPAuth = true;	  // enable SMTP authentication
	$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
	$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
	$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
	$mail_office->Password = "e9DcdmHE!"; // SMTP account password
	$mail_office->IsHTML();
	$mail_office->SetFrom('noreply@godashboard.nl', 'GoDashboard');
	
	$mail_office->AddAddress("dr@godashboard.nl");
	$mail_office->Subject = $subject;
	$mail_office->Body = $message;
	
	$mail_office->Send();

	//start ActiveCam
	$ac_url = 'https://iventure.api-us1.com';
	
	$ac_params = array(
		"api_key"      => "8bdd980664a4ae416aafa99123e598956dcfea1c12722e19b50c6d31873a183ad32b188b",
		"api_action"   => "contact_list",
		"api_output"   => "serialize",
		"filters[email]" => "".$_SESSION['unameGO']."",
		"full"	 		=> 0
	);
	
	// This section takes the input fields and converts them to the proper format
	$ac_query = "";
	foreach( $ac_params as $ac_key => $ac_value ) $ac_query .= $ac_key . '=' . urlencode($ac_value) . '&';
	$ac_query = rtrim($ac_query, '& ');
	
	// clean up the url
	$ac_url = rtrim($ac_url, '/ ');
	
	// define a final API request - GET
	$ac_api = $ac_url . '/admin/api.php?' . $ac_query;
	
	$ac_request = curl_init($ac_api); // initiate curl object
	curl_setopt($ac_request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
	curl_setopt($ac_request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
	curl_setopt($ac_request, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ac_request, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
	
	$ac_response = (string)curl_exec($ac_request); // execute curl fetch and store results in $response
	
	curl_close($ac_request); // close curl object
	
	$ac_result = unserialize($ac_response);
	$ac_contactid = $ac_result[0]["id"];
	
	//add to list Klant Godashboard and add tags with payment info
	$ac_params = array(
		"api_key"      => "8bdd980664a4ae416aafa99123e598956dcfea1c12722e19b50c6d31873a183ad32b188b",
		"api_action"   => "contact_edit",
		"api_output"   => "serialize",
		"overwrite"   => 0
	);
	
	$ac_post = array(
		"id"                    => $ac_contactid,
		"tags"					=> "klant,$name,$period,stripe",
		"p[16]"                 => 16, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
		"status[16]"            => 1 // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 1)
	);
	
	// This section takes the input fields and converts them to the proper format
	$ac_query = "";
	foreach( $ac_params as $ac_key => $ac_value ) $ac_query .= $ac_key . '=' . urlencode($ac_value) . '&';
	$ac_query = rtrim($ac_query, '& ');
	
	// This section takes the input data and converts it to the proper format
	$ac_data = "";
	foreach( $ac_post as $ac_key => $ac_value ) $ac_data .= $ac_key . '=' . urlencode($ac_value) . '&';
	$ac_data = rtrim($ac_data, '& ');
	
	// clean up the url
	$ac_url = rtrim($ac_url, '/ ');
	
	// define a final API request - GET
	$ac_api = $ac_url . '/admin/api.php?' . $ac_query;
	
	$ac_request = curl_init($ac_api); // initiate curl object
	curl_setopt($ac_request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
	curl_setopt($ac_request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
	curl_setopt($ac_request, CURLOPT_POSTFIELDS, $ac_data); // use HTTP POST to send form data
	curl_setopt($ac_request, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ac_request, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
			
	$ac_response = (string)curl_exec($ac_request); // execute curl post and store results in $response
	
	// additional options may be required depending upon your server configuration
	// you can find documentation on curl options at http://www.php.net/curl_setopt
	curl_close($ac_request); // close curl object
	
	//update status from list Proefaccount website
	
	$ac_params = array(
		"api_key"      => "8bdd980664a4ae416aafa99123e598956dcfea1c12722e19b50c6d31873a183ad32b188b",
		"api_action"   => "contact_edit",
		"api_output"   => "serialize",
		"overwrite"   => 0
	);
	
	$ac_post = array(
		"id"                    => $ac_contactid,
		"p[10]"                 => 10, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
		"status[10]"            => 2 // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 1)
	);
	
	// This section takes the input fields and converts them to the proper format
	$ac_query = "";
	foreach( $ac_params as $ac_key => $ac_value ) $ac_query .= $ac_key . '=' . urlencode($ac_value) . '&';
	$ac_query = rtrim($ac_query, '& ');
	
	// This section takes the input data and converts it to the proper format
	$ac_data = "";
	foreach( $ac_post as $ac_key => $ac_value ) $ac_data .= $ac_key . '=' . urlencode($ac_value) . '&';
	$ac_data = rtrim($ac_data, '& ');
	
	// clean up the url
	$ac_url = rtrim($ac_url, '/ ');
	
	// define a final API request - GET
	$ac_api = $ac_url . '/admin/api.php?' . $ac_query;
	
	$ac_request = curl_init($ac_api); // initiate curl object
	curl_setopt($ac_request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
	curl_setopt($ac_request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
	curl_setopt($ac_request, CURLOPT_POSTFIELDS, $ac_data); // use HTTP POST to send form data
	curl_setopt($ac_request, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ac_request, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
			
	$ac_response = (string)curl_exec($ac_request); // execute curl post and store results in $response
	
	// additional options may be required depending upon your server configuration
	// you can find documentation on curl options at http://www.php.net/curl_setopt
	curl_close($ac_request); // close curl object
	
	//remove tag proefaccount
	$ac_params = array(
		"api_key"      => "8bdd980664a4ae416aafa99123e598956dcfea1c12722e19b50c6d31873a183ad32b188b",
		"api_action"   => "contact_tag_remove",
		"api_output"   => "serialize"
	);
	
	$ac_post = array(
		"id"                    => $ac_contactid,
		"tags"               	=> "Proefaccount"
	);
	
	// This section takes the input fields and converts them to the proper format
	$ac_query = "";
	foreach( $ac_params as $ac_key => $ac_value ) $ac_query .= $ac_key . '=' . urlencode($ac_value) . '&';
	$ac_query = rtrim($ac_query, '& ');
	
	// This section takes the input data and converts it to the proper format
	$ac_data = "";
	foreach( $ac_post as $ac_key => $ac_value ) $ac_data .= $ac_key . '=' . urlencode($ac_value) . '&';
	$ac_data = rtrim($ac_data, '& ');
	
	// clean up the url
	$ac_url = rtrim($ac_url, '/ ');
	
	// define a final API request - GET
	$ac_api = $ac_url . '/admin/api.php?' . $ac_query;
	
	$ac_request = curl_init($ac_api); // initiate curl object
	curl_setopt($ac_request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
	curl_setopt($ac_request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
	curl_setopt($ac_request, CURLOPT_POSTFIELDS, $ac_data); // use HTTP POST to send form data
	curl_setopt($ac_request, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ac_request, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
			
	$ac_response = (string)curl_exec($ac_request); // execute curl post and store results in $response
	
	// additional options may be required depending upon your server configuration
	// you can find documentation on curl options at http://www.php.net/curl_setopt
	curl_close($ac_request); // close curl object
	
	//end of ActiveCam 
	//start add to insightly
	$result = mysqli_query($mysqli,"SELECT CONTACT_ID, ORGANISATION_ID, OPPORTUNITY_ID FROM attempt WHERE email='".$_SESSION["unameGO"]."' ORDER BY id DESC LIMIT 1");
	$numrows = mysqli_num_rows($result);
	if ($numrows == 1) {
		$gegevens = mysqli_fetch_array($result);
		$OPPORTUNITY_ID = $gegevens['OPPORTUNITY_ID'];
		$CONTACT_ID = $gegevens['CONTACT_ID'];
		
		date_default_timezone_set("UTC");
		$ac_url = "https://api.insight.ly";
		$username = "b3b8e648-2bfc-4bb7-b281-5cac6d17b72d";
		$password = "";
		
		//update oppertunity state
		$ac_post = array(
			"OPPORTUNITY_ID"        => $OPPORTUNITY_ID,
			"FOR_OPPORTUNITY_STATE"	=> "Won",
			"DATE_CHANGED_UTC"		  => date("Y-m-d H:i:s")
		);
		$ac_data = json_encode($ac_post);			
		
		// define a final API request - GET
		$ac_api = $ac_url . "/v2.1/OpportunityStateChange/$OPPORTUNITY_ID";
		
		$ac_request = curl_init($ac_api); // initiate curl object
		curl_setopt($ac_request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($ac_request, CURLOPT_USERPWD, $username . ":" . $password);
		curl_setopt($ac_request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		curl_setopt($ac_request, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ac_request, CURLOPT_POSTFIELDS, $ac_data); // use HTTP POST to send form data
		curl_setopt($ac_request, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ac_request, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($ac_data)));
				
		$ac_response = (string)curl_exec($ac_request); // execute curl post and store results in $response
						
		// additional options may be required depending upon your server configuration
		// you can find documentation on curl options at http://www.php.net/curl_setopt
		curl_close($ac_request); // close curl object
	}
		  
  header("Location: ../".$_GET['p']."?pay=success");
  exit;
}
catch(Exception $e)
{
  mysqli_query($mysqli, "INSERT INTO stripe_errors (email,message) VALUES ('".$_POST['stripeEmail']."','".$e->getMessage()."')") or die(mysqli_error($mysqli));
  
  header("Location: ../".$_GET['p']."?pay=error&message=".urlencode($e->getMessage())."");
  exit;
}

?>