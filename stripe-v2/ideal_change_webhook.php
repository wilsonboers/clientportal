<?php 
// Config includen om met de MySQL database verbinding te maken
include("../config.php");
require_once '../plans/class.phpmailer.php';
require_once '../plans/class.smtp.php';

// If you're using Composer, use Composer's autoload:
require_once('../../stripe/init.php');

\Stripe\Stripe::setApiKey("sk_live_IyZCez0TNnkBlak8k5qvVXvJ");

// Retrieve the request's body and parse it as JSON
$input = @file_get_contents("php://input");
$event_json = json_decode($input);

// Verify the event by fetching it from Stripe
$event = \Stripe\Event::retrieve($event_json->id);

// Do something with $event
$sourceid = $event["data"]["object"]["id"];
$client_secret = $event["data"]["object"]["client_secret"];
$status = $event["data"]["object"]["status"];

$qy = mysqli_query($mysqli,"SELECT * FROM change_ideal WHERE sourceid='$sourceid' and client_secret='$client_secret' LIMIT 1");
$count = mysqli_num_rows($qy);
//if it's not a change payment exit
if ($count!=1) exit;

if ($status=="chargeable") {
	$qy = mysqli_query($mysqli, "SELECT * FROM stripe_ideal WHERE sourceid='$sourceid' and client_secret='$client_secret' and active='0'") or die(mysqli_error($mysqli));
	$count = mysqli_num_rows($qy);
	
	if ($count==1) {		
		$gegevens = mysqli_fetch_array($qy);		
		$user = $gegevens['user']; 	  
		$s = $gegevens['s']; 	  
		$p = $gegevens['p'];
		$amount = $gegevens['amount'];

		try
		{
			$charge = \Stripe\Charge::create(array(
			  "amount" => $amount,
			  "currency" => "eur",
			  "source" => "$sourceid",
			));

			mysqli_query($mysqli,"UPDATE stripe_ideal SET active='1' WHERE sourceid='$sourceid' and client_secret='$client_secret'");
			
			$qy = mysqli_query($mysqli, "SELECT * FROM change_ideal WHERE uid='$user' and s='$s' ORDER BY id DESC") or die(mysqli_error($mysqli));
			$gegevens = mysqli_fetch_array($qy);
			
			$costs = $gegevens['costs'];
			$costs_after = $gegevens['costs_after'];
			$oneoff = $gegevens['oneoff'];
			$date = $gegevens['date'];
			$plan = $gegevens['plan'];
			$period = $gegevens['period'];
			$rec = $gegevens['rec'];
			
			$array = explode("-", $date);
			$day = $array[0];
			$month = $array[1];
			$year = $array[2];
			$newDate = "$year-$month-$day";
		
			//endDate = direct, nieuwe maandelijkse kosten
			if (($costs!="") and ($rec==1)) mysqli_query($mysqli,"UPDATE plans SET endDate='".date("Y-m-d")."' WHERE uid='$user' and endDate IS NULL") or die(mysqli_error($mysqli));
			else mysqli_query($mysqli,"UPDATE plans SET endDate='$newDate' WHERE uid='$user' and endDate IS NULL") or die(mysqli_error($mysqli));//endDate = startdate van nieuwe plan
			
			//eenmalige kosten, alleen indien u[grade en van jaarlijks naar maandelijks //nr. 7
			if ($oneoff!="") mysqli_query($mysqli, "INSERT INTO plans (uid,plan,period,startDate,amount) VALUES ('$user','$plan','One-off','".date("Y-m-d")."','$oneoff')") or die(mysqli_error($mysqli));
			
			//eenmalige kosten
			if (($costs!="") and ($rec==0)) mysqli_query($mysqli, "INSERT INTO plans (uid,plan,period,startDate,amount) VALUES ('$user','$plan','One-off','".date("Y-m-d")."','$costs')") or die(mysqli_error($mysqli));
			
			//maandelijks tot startdatum nieuwe plan
			if (($costs!="") and ($rec==1)) mysqli_query($mysqli, "INSERT INTO plans (uid,plan,period,startDate,endDate,amount) VALUES ('$user','$plan','month','".date("Y-m-d")."','$newDate','$costs')") or die(mysqli_error($mysqli));
			
			//recurring kosten
			mysqli_query($mysqli, "INSERT INTO plans (uid,plan,period,startDate,amount) VALUES ('$user','$plan','$period','$newDate','$costs_after')") or die(mysqli_error($mysqli));
			
			//stuur e-mail
			$subject = "Wijziging plan, betaling via ideal eenmalige kosten voltooid.";
			if (($costs!="") and ($rec==0)) {
				$message = "User: $user<br>	
				Eenmalige kosten: $costs<br>
				Startdatum nieuwe plan: $newDate<br>
				Plan: $plan<br>
				Period: $period<br>
				Kosten na startdatum: $costs_after";
			}
			elseif (($costs!="") and ($rec==1)) {
				$message = "User: $user<br>";
				if ($oneoff!="") $message .= "Eenmalige kosten: $oneoff<br>";
				$message .= "Maandelijkse kosten tot nieuwe startdatum: $costs<br>
				Startdatum nieuwe plan: $newDate<br>
				Plan: $plan<br>
				Period: $period<br>
				Kosten na startdatum: $costs_after";
			}
			else {
				$message = "User: $user<br>	
				Startdatum nieuwe plan: $newDate<br>
				Plan: $plan<br>
				Period: $period<br>
				Kosten na startdatum: $costs_after";
			}
			
			$mail_office = new PHPMailer();
			$mail_office->IsSMTP();
			// set smtp login
			$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
			// $mail->SMTPDebug  = 2;
			$mail_office->SMTPAuth = true;	  // enable SMTP authentication
			$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
			$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
			$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
			$mail_office->Password = "e9DcdmHE!"; // SMTP account password
			$mail_office->IsHTML();
			$mail_office->SetFrom('noreply@godashboard.nl', 'GoDashboard');
			
			$mail_office->AddAddress("dr@godashboard.nl");
			$mail_office->Subject = $subject;
			$mail_office->Body = $message;
			
			$mail_office->Send();				 
				
		}
		catch(Exception $e)
		{
		  mysqli_query($mysqli, "INSERT INTO stripe_errors (email,message) VALUES ('$user','".$e->getMessage()."')") or die(mysqli_error($mysqli));
		}
	}
}
elseif (($status=="canceled") || ($status=="failed")) {
	$qy = mysqli_query($mysqli, "SELECT * FROM stripe_ideal WHERE sourceid='$sourceid' and client_secret='$client_secret' and active='0'") or die(mysqli_error($mysqli));
	$count = mysqli_num_rows($qy);
	
	if ($count==1) {
		mysqli_query($mysqli,"UPDATE stripe_ideal SET active='1', failed='1' WHERE sourceid='$sourceid' and client_secret='$client_secret'");	
	}
}
?>