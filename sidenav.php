<div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
        <li>
        <br>
            <a href="getstarted.php"><i class="fa fa-paper-plane fa-fw"></i> Getting started</a>
        </li>
        <li>
            <a href="gallery.php"><i class="fa fa-th fa-fw"></i> Data Visual Gallery</a>
        </li>        
        <li>
            <a href="settings.php"><i class="fa fa-gear fa-fw"></i> Account Settings</a>
        </li>                          
        <li>
            <a href="support.php"><i class="fa fa-life-ring fa-fw"></i> Support</a>
        </li>                     
        <li>
            <a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
        </li>
    </ul>
</div>