<?php
include("config.php");			

// Alle session variabelen unsetten 
foreach ($_SESSION as $var => $val) 
{ 
    unset($_SESSION[$var]); 
} 

// Alles weghalen 
session_destroy(); 

// Gebruiker redirecten nadat sessie is beeindigd
echo "<meta http-equiv='refresh' content='0;URL=login.php'>"; 
?>