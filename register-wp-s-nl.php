<?php
include("config.php");

if ($_SESSION['userFROM']=="") $_SESSION['userFROM'] = $_SERVER['HTTP_REFERER']; 
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoDashboard Client Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
.login-panel {
    margin-top: 10% !important;
}
.margintop {
    margin-top: 1% !important;
}
/* Skills Progess Bar */
.progress {
	margin: 15px;
}
#error_email, #error_retype, #error_phone, #error_contact {
	color:#ff0000;
}
</style>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>
<body>

    <div class="container">
       <div class="row">               
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">                 
                    <div class="panel-body">
                        <div style="width:100%"></div>
                        <form role="form" method="post" action="../integrations/register-v2.php">
                            <fieldset>
<?php                             
                           echo "<fieldset>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" placeholder=\"E-mail\" id=\"email\" name=\"email\" type=\"email\" required> <span id=\"error_email\"></span>
                                </div>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" placeholder=\"Coupon code\" id=\"coupon\" name=\"coupon\" type=\"text\"> <span id=\"error_coupon\"></span>
                                </div>
                                <div class=\"form-group\">
                                    <div class=\"g-recaptcha\" data-sitekey=\"6LcbRwoUAAAAALUc7Gr69cONqTITgBlYmf_agG-w\"></div>
                                </div>
								<div class=\"form-group\">
                                    <small><a href=\"https://godashboard.nl/TermsOfServices112016.pdf\" target=\"_blank\">Door te registreren verklaart u de voorwaarden te hebben gelezen en daarmee akkoord te zijn.</a></small>
                                </div>
                                <button id=\"submit\" name=\"submit\" value=\"\" class=\"btn btn-lg btn-success btn-block\" type=\"submit\">ACTIVEER PROEFACCOUNT!</button>";
?>
                            </fieldset>
                        </form>
                <!-- /progressbar when account is changed or payed -->
                          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 51px !important; display: none;">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <h4 class="modal-title" id="myModalLabel">Account aanmaken..</h4>
                                  </div>
                                  <div class="modal-body">
                                  <div class="progress">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar"
                                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"><span id="setup">0%</span>
                                        </div>
                                      </div>
                                      
                                  </div>
                              </div>
                              <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->                       
                    </div>
                </div>
              </div>
        </div>
    </div>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script>
	function myTimeout10() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
  
		  var myVar = setTimeout(function() {$progressBar.css('width', '10%'); $('#setup').text('10%');}, 2000); // WAIT 10 seconds
		  	  		  
		  return(myVar);
	}
	function myTimeout40() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '30%'); $('#setup').text('30%');}, 4000); // WAIT 4 second

		  return(myVar);
	}
	function myTimeout90() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '50%'); $('#setup').text('50%');}, 6000); // WAIT 3 second

		  return(myVar);
	}
	function myTimeout95() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '70%'); $('#setup').text('70%');}, 7500); // WAIT 3 second
		  
		  return(myVar);
	}
	function myTimeout100() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '85%'); $('#setup').text('85%');}, 9000); // WAIT 3 second
		  
		  return(myVar);
	}
	function myTimeout110() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '95%'); $('#setup').text('Almost done :)');}, 10000); // WAIT 2 second
		  		  
		  return(myVar);
	}
	function showprogress() {
		  myTimeout10();
		  myTimeout40();
		  myTimeout90();
		  myTimeout95();
		  myTimeout100();
		  myTimeout110();
		  
		  $('#myModal').modal("show");		
	}
	function isValidEmailAddress(emailAddress) {
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		return pattern.test(emailAddress);
	};
	
	$(document).ready(function () {
		$("#submit").click(function (){		
			var hasEmail= $("#email").val();
			
			//check e-mail format
			if (!isValidEmailAddress(hasEmail)) {	
				$( "#error_email" ).text( "Onjuist e-mailadres" ).show();
				event.preventDefault();
			}
			else $( "#error_email" ).text( "" ).show();
			
			//check everything		
			if ((hasEmail) && (isValidEmailAddress(hasEmail))) {					
				showprogress();
			}
			else event.preventDefault();
		});
	});
	$(window).on("unload", function(e) {
		$("#submit").unbind("click");
	});
	</script>    
</body>

</html>
