<style>
#myModalDEBIT .modal-dialog, #myModalINVOICE .modal-dialog {
    width: 350px;
}
#myModalDEBIT .modal-header, #myModalINVOICE .modal-header {
    text-align: center;
    background-color: #e8e9eb;
}
#myModalDEBIT .modal-body, #myModalINVOICE .modal-body {		
	text-align:center !important;
	padding-top: 20px !important;	
	padding-right: 50px !important;	
	padding-left: 50px !important;
	padding-bottom: 50px !important;
}
.Header-loggedInBar {
    height: 1px;
    background-image: -webkit-radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
    background-image: radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
	background-color: #B7B7B7;
	width: 50%;
	margin-left:25%;
	margin-top: 0px;
	margin-bottom: 0px;
}
#myModalLabel {	
	margin: 1px !important;
}
.stripe-button-el span {
	display: none !important;
}
.stripe-button-el {
	display: none !important;
}
.center-block {
    float: none;
    margin-left: auto;
    margin-right: auto;
}

.input-group .icon-addon .form-control {
    border-radius: 0;
}

.icon-addon {
    position: relative;
    color: #555;
    display: block;
}

.icon-addon:after,
.icon-addon:before {
    display: table;
    content: " ";
}

.icon-addon:after {
    clear: both;
}

.icon-addon.addon-md .glyphicon,
.icon-addon .glyphicon, 
.icon-addon.addon-md .fa,
.icon-addon .fa {
    position: absolute;
    z-index: 2;
    left: 10px;
    font-size: 14px;
    width: 20px;
    margin-left: -2.5px;
    text-align: center;
    padding: 10px 0;
    top: 1px
}

.icon-addon.addon-lg .form-control {
    line-height: 1.33;
    height: 46px;
    font-size: 18px;
    padding: 10px 16px 10px 40px;
}

.icon-addon.addon-sm .form-control {
    height: 30px;
    padding: 5px 10px 5px 28px;
    font-size: 12px;
    line-height: 1.5;
}

.icon-addon.addon-lg .fa,
.icon-addon.addon-lg .glyphicon {
    font-size: 18px;
    margin-left: 0;
    left: 11px;
    top: 4px;
}

.icon-addon.addon-md .form-control,
.icon-addon .form-control {
    padding-left: 30px;
    float: left;
    font-weight: normal;
}

.icon-addon.addon-sm .fa,
.icon-addon.addon-sm .glyphicon {
    margin-left: 0;
    font-size: 12px;
    left: 5px;
    top: -1px
}

.icon-addon .form-control:focus + .glyphicon,
.icon-addon:hover .glyphicon,
.icon-addon .form-control:focus + .fa,
.icon-addon:hover .fa {
    color: #2580db;
}
.btn-info {
    position: relative;
    border-radius: 4px;
    background-color: #3ea8e5;
    background-image: -webkit-linear-gradient(top,#44b1e8,#3098de);
    background-image: linear-gradient(-180deg,#44b1e8,#3098de);
    box-shadow: 0 1px 0 0 rgba(46,86,153,.15),inset 0 1px 0 0 rgba(46,86,153,.1),inset 0 -1px 0 0 rgba(46,86,153,.4);
    font-size: 17px;
    line-height: 21px;
    height: 37px;
    font-weight: 700;
    text-shadow: 0 -1px 0 rgba(0,0,0,.12);
    color: #fff;
    cursor: pointer;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
</style>
<?php 		
	$res		= mysqli_query($mysqli, "SELECT * FROM users WHERE uid='".$_SESSION['uidGO']."'");
	$myrow 		= mysqli_fetch_array($res);
	$account	= $myrow["account"];	
	$country	= $myrow["country"];
	$contact	= $myrow["contact"];
	$company	= $myrow["company"];
	$vat		= $myrow["vat"];
	$phonenumber= $myrow["phonenumber"];
	$endDate	= $myrow["endDate"];
	if ($endDate!="") $reac = 1;
	else $reac = 0;
?>
<!--START STRIPE BUTTON AND ALERT BOX-->
<form action="stripe/index.php?p=<?php echo "".basename($_SERVER['PHP_SELF'])."&reac=$reac"; ?>" method="POST" style="margin-bottom:5px !important;">
<button type="submit" class="btn btn-block btn-social btn-instagram">    
 <i class="fa fa-credit-card"></i> Pay with card
<script
src="https://checkout.stripe.com/checkout.js" class="stripe-button"
data-key="pk_live_SAXNGqm8RSm1yEgjcIIaguaQ"
data-image="GO.png"
data-email="<?php echo "".$_SESSION["unameGO"].""; ?>"
data-name="GoDashboard account"
data-description="Monthly subscription"
data-amount="2000"
data-currency="eur"
data-label="Pay with card">
</script>            
</button>
</form>
<!--START DIRECT DEBIT BUTTON AND ALERT BOX-->
<a class="btn btn-block btn-social btn-instagram" style="margin-bottom: 5px !important;"" data-toggle="modal" data-target="#myModalDEBIT">
      <i class="fa fa-money"></i> Pay with direct debit
</a>
<div class="modal fade" id="myModalDEBIT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 31px !important; display: none;">
						 <form role="form" method="post" action="direct-debit/index.php?p=<?php echo "".basename($_SERVER['PHP_SELF'])."&reac=$reac"; ?>">
							<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close " data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel"><b>GoDashboard account </b></h4><span class="text-muted small">Monthly subscription
                                                <hr class="Header-loggedInBar">
                                                <?php echo "".$_SESSION["unameGO"].""; ?>
											</div>
											<div class="modal-body">
                                            	<fieldset>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Account number" id="account" name="account" type="text" required>
                                                        <label for="account" class="glyphicon glyphicon-credit-card" rel="tooltip" title="account"></label>           
                                                        </div>
                                                    </div>
  													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Account holder" id="accountholder" name="accountholder" type="text" required>
                                                        <label for="accountholder" class="glyphicon glyphicon-user" rel="tooltip" title="accountholder"></label>           
                                                        </div>
                                                    </div>
                                                 <?php if ($company=="") { ?>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Company name" id="company" name="company" type="text">
                                                        <label for="company" class="glyphicon glyphicon-home" rel="tooltip" title="company"></label>           
                                                        </div>
                                                    </div>
                                                 <?php } ?>
                                                 <?php if ($vat=="") { ?>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="VAT" id="vat" name="vat" type="text">
                                                        <label for="vat" class="glyphicon glyphicon-usd" rel="tooltip" title="vat"></label>           
                                                        </div>
                                                    </div>
                                                 <?php } ?>
                                                 <?php if ($country=="xx") { ?>
													<div class="form-group">
														<div class="icon-addon addon-sm">
														<!--countries-->
														<?php include("sel-country.php"); ?>
     	  												<!-- /.countries sel -->
                                                        <label for="country" class="glyphicon glyphicon-globe" rel="tooltip" title="country"></label>           
                                                        </div>
                                                    </div>
                                                 <?php } ?>
                                                    <div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Phonenumber" id="phone" name="phone" type="text" value="<?php echo $phonenumber ?>" required>
                                                        <label for="phone" class="glyphicon glyphicon-phone" rel="tooltip" title="phonenumber"></label>           
                                                        </div>
                                                    </div>                                                  
                                                 </fieldset>
                                                 <button id="submit" name="submit" class="btn btn-block btn-info" type="submit">Direct debit €20.00</button>
												</div>
                                         </div>
									</div>
								   </form>
								</div>                         
 <!--START INVOICE BUTTON AND ALERT BOX-->
<a class="btn btn-block btn-social btn-instagram" style="margin-bottom: 5px !important;"" data-toggle="modal" data-target="#myModalINVOICE">
      <i class="fa fa-file-text-o"></i> Pay with invoice
</a>
<div class="modal fade" id="myModalINVOICE" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 31px !important; display: none;">
						 <form role="form" method="post" action="invoice/index.php?p=<?php echo "".basename($_SERVER['PHP_SELF'])."&reac=$reac"; ?>">
							<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close " data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel"><b>GoDashboard account </b></h4><span class="text-muted small">Monthly subscription
                                                <hr class="Header-loggedInBar">
                                                <?php echo "".$_SESSION["unameGO"].""; ?>
											</div>
											<div class="modal-body">
                                                 <fieldset>
                                                 <?php if ($company=="") { ?>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Company name" id="company" name="company" type="text">
                                                        <label for="company" class="glyphicon glyphicon-home" rel="tooltip" title="company"></label>           
                                                        </div>
                                                    </div>
                                                 <?php } ?>
                                                 <?php if ($vat=="") { ?>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="VAT" id="vat" name="vat" type="text">
                                                        <label for="vat" class="glyphicon glyphicon-usd" rel="tooltip" title="vat"></label>           
                                                        </div>
                                                    </div>
                                                 <?php } ?>
                                                 <?php if ($country=="xx") { ?>
													<div class="form-group">
														<div class="icon-addon addon-sm">
														<!--countries-->
														<?php include("sel-country.php"); ?>
     	  												<!-- /.countries sel -->
                                                        <label for="country" class="glyphicon glyphicon-globe" rel="tooltip" title="country"></label>           
                                                        </div>
                                                    </div>
                                                 <?php } ?>
                                                    <div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Phonenumber" id="phone" name="phone" type="text" value="<?php echo $phonenumber ?>" required>
                                                        <label for="phone" class="glyphicon glyphicon-phone" rel="tooltip" title="phonenumber"></label>           
                                                        </div>
                                                    </div>                                                   
                                                 </fieldset>
                                                 <button id="submit" name="submit" class="btn btn-block btn-info" type="submit">Invoice me €20.00</button>
												</div>
                                         </div>
									</div>
								   </form>
								</div>	                                	 
