<?php
// Config includen om met de MySQL database verbinding te maken
include("config.php");			

if (!is_logged_in()) redirect();
else {
	$result = $_GET["result"];
	if (isset($result)) $result ="<br><div class=\"alert alert-warning\">$result</div>";

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoDashboard</title>
<link rel="shortcut icon" href="favicon.ico" />

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
img {
	margin-left:5px;
	margin-top:1px;
}
.row .show-grid {
	margin: 0;
}
.hoverDiv:hover {background: #f5f5f5 !important;}
</style>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="GoDashboardLogo220x50.png">
            </div>
<?php include("topnav-v2.php"); ?>
            <div class="navbar-default sidebar" role="navigation">
<?php include("sidenav-v2.php"); ?>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12" align="center">
                                        <div style="width:100%">
						<?php
                            	echo "<div class=\"modal fade\" id=\"myModalREA\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"top: 31px !important; display: none;\">
						 <form role=\"form\" method=\"post\" action=\"$rea_page\">
							<div class=\"modal-dialog\">
										<div class=\"modal-content\">
											<div class=\"modal-header\">
												<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
												<h4 class=\"modal-title\" id=\"myModalLabel\">"; 
												echo "Are you sure you would like to re-activate your subscription?
												</div>
												<div class=\"modal-footer\">
												<button id=\"submit\" name=\"submit\" class=\"btn btn-primary\" type=\"submit\">Re-activate subscription</button>"; 
												echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button></div>";
										echo "</div>
									</div>
								   </form>
								</div>";
			?>  			<?php echo $result; ?></div>                  
                    <h2>Start adding Data Visuals to your Dashboard right now!</h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->                     
            
            <div class="row">
                <div class="col-lg-12">
				<?php 	
					$x=0;
					$query = mysqli_query($mysqli, "select * from services where active='1' order by name ASC") or die(mysqli_error($mysqli));
					while ($row = mysqli_fetch_array($query)) {						
                    	$id = $row["id"];						
                    	$name = $row["name"];
						$icon = $row["icon"];
						if ($x % 4 == 0) echo "</div>";						
						if (($x==0) || ($x % 4 == 0)) echo "<div class=\"row show-grid\">";						
						echo "<a href=\"service-v2.php?id=$id&name=$name\"><div class=\"col-md-3 hoverDiv\" align=\"center\"><div><img src=\"../integrations/images/".$icon.".png\" width=\"44\" height=\"44\"></div><h5>$name</h5></div></a>";

						$x++;
					}

					if ($x % 4 == 0) echo "</div>";					
					elseif (($x-3) % 4 == 0) echo "<div class=\"col-md-3\" style=\"min-height:102px\">&nbsp;</div></div>";	
					elseif (($x-2) % 4 == 0) echo "<div class=\"col-md-6\" style=\"min-height:102px\">&nbsp;</div></div>";	
					else echo "<div class=\"col-md-9\" style=\"min-height:102px\">&nbsp;</div></div><br>";
						
                    //einde normale gallery
					echo "<div align=\"center\"><h2>Or start by connecting to your data</h2></div>";         						
					echo "<div class=\"row show-grid\">";					
					echo "<a href=\"editor.php?datasource=1&type=1\" target=\"editor\"><div class=\"col-md-3 hoverDiv\" align=\"center\"><div><i class=\"fa fa-upload fa-3x\" aria-hidden=\"true\"></i></div><h5>Upload a File<br> Excel, XML, CSV, JSON</h5></div></a>";
					echo "<a href=\"editor.php?datasource=1&type=2\" target=\"editor\"><div class=\"col-md-3 hoverDiv\" align=\"center\"><div><i class=\"fa fa-cloud fa-3x\" aria-hidden=\"true\"></i></div><h5>Connect to cloud services using RESTful web services.</h5></div></a>";
					echo "<a href=\"editor.php?datasource=1&type=3\"target=\"editor\" ><div class=\"col-md-3 hoverDiv\" align=\"center\"><div><i class=\"fa fa-database fa-3x\" aria-hidden=\"true\"></i></div><h5>Connect to MySQL, MSSQL, Oracle, and more.</h5></div></a>";
					echo "<a href=\"editor.php?datasource=1&type=4\" target=\"editor\"><div class=\"col-md-3 hoverDiv\" align=\"center\"><div><i class=\"fa fa-folder-open fa-3x\" aria-hidden=\"true\"></i></div><h5>Retrieve files stored on an FTP / SFTP server.</h5></div></a>";
					 echo "</div>";	
					?>

              		</div>
                    <br><br>
        	</div>
        </div>				
					
				</div>								
					
				</div>
			<!-- einde pricing -->            

            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
<?php } ?>