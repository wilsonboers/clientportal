<style>
#myModalCHANGEMETHOD .modal-dialog {
    width: 350px;
}
#myModalCHANGEMETHOD .modal-header {
    text-align: center;
    background-color: #e8e9eb;
}
#myModalCHANGEMETHOD .modal-body {		
	text-align:center !important;
	padding-top: 20px !important;	
	padding-right: 50px !important;	
	padding-left: 50px !important;
	padding-bottom: 50px !important;
}
.Header-loggedInBar {
    height: 1px;
    background-image: -webkit-radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
    background-image: radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
	background-color: #B7B7B7;
	width: 50%;
	margin-left:25%;
	margin-top: 0px;
	margin-bottom: 0px;
}
#myModalLabel {	
	margin: 1px !important;
}
.stripe-button-el span {
	display: none !important;
}
.stripe-button-el {
	display: none !important;
}
.center-block {
    float: none;
    margin-left: auto;
    margin-right: auto;
}

.input-group .icon-addon .form-control {
    border-radius: 0;
}

.icon-addon {
    position: relative;
    color: #555;
    display: block;
}

.icon-addon:after,
.icon-addon:before {
    display: table;
    content: " ";
}

.icon-addon:after {
    clear: both;
}

.icon-addon.addon-md .glyphicon,
.icon-addon .glyphicon, 
.icon-addon.addon-md .fa,
.icon-addon .fa {
    position: absolute;
    z-index: 2;
    left: 10px;
    font-size: 14px;
    width: 20px;
    margin-left: -2.5px;
    text-align: center;
    padding: 10px 0;
    top: 1px
}

.icon-addon.addon-lg .form-control {
    line-height: 1.33;
    height: 46px;
    font-size: 18px;
    padding: 10px 16px 10px 40px;
}

.icon-addon.addon-sm .form-control {
    height: 30px;
    padding: 5px 10px 5px 28px;
    font-size: 12px;
    line-height: 1.5;
}

.icon-addon.addon-lg .fa,
.icon-addon.addon-lg .glyphicon {
    font-size: 18px;
    margin-left: 0;
    left: 11px;
    top: 4px;
}

.icon-addon.addon-md .form-control,
.icon-addon .form-control {
    padding-left: 30px;
    float: left;
    font-weight: normal;
}

.icon-addon.addon-sm .fa,
.icon-addon.addon-sm .glyphicon {
    margin-left: 0;
    font-size: 12px;
    left: 5px;
    top: -1px
}

.icon-addon .form-control:focus + .glyphicon,
.icon-addon:hover .glyphicon,
.icon-addon .form-control:focus + .fa,
.icon-addon:hover .fa {
    color: #2580db;
}
.btn-info {
    position: relative;
    border-radius: 4px;
    background-color: #3ea8e5;
    background-image: -webkit-linear-gradient(top,#44b1e8,#3098de);
    background-image: linear-gradient(-180deg,#44b1e8,#3098de);
    box-shadow: 0 1px 0 0 rgba(46,86,153,.15),inset 0 1px 0 0 rgba(46,86,153,.1),inset 0 -1px 0 0 rgba(46,86,153,.4);
    font-size: 17px;
    line-height: 21px;
    height: 37px;
    font-weight: 700;
    text-shadow: 0 -1px 0 rgba(0,0,0,.12);
    color: #fff;
    cursor: pointer;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
.fa-credit-card-alt, .fa-cc-stripe, .fa-university {
	font-size:smaller !important;
}
</style>
<?php
if ($account!="FREE") {
	echo "<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModalCHANGEMETHOD\" class=\"btn btn-default btn-xs\">CHANGE PAYMENT METHOD</a>";
							?>
	<!--START STRIPE BUTTON IDEAL-->
	<div class="modal fade" id="myModalCHANGEMETHOD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 31px !important; display: none;">
	<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close " data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="myModalLabel"><b>Select payment method </b></h4>
						<hr class="Header-loggedInBar">
						<?php echo "".$_SESSION["unameGO"].""; ?>
					</div>
					<div class="modal-body">
					<?php include("plans/method_change.php") ?>
					</div>
				 </div>
			</div>
		   </form>
		</div>
<?php } ?>
