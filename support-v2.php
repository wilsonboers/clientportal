<?php
// Config includen om met de MySQL database verbinding te maken
include("config.php");			
require_once 'plans/class.phpmailer.php';
require_once 'plans/class.smtp.php';

if (!is_logged_in()) redirect();
else {
	$result = $_GET["result"];
	if (isset($result)) $result ="<br><div class=\"alert alert-warning\">$result</div>";

	if (isset($_POST["access"])) {
		$a_check		= $_POST["a_check"];
		if ($a_check==true) $a_check=1;
		else $a_check=0;
		mysqli_query($mysqli, "UPDATE users SET access='$a_check' WHERE uid='".$_SESSION['uidGO']."'");
		$result = "<br><div class=\"alert alert-success\">Grant access setting updated.</div>";	
	}
	$res		= mysqli_query($mysqli, "SELECT access FROM users WHERE uid='".$_SESSION['uidGO']."'");
	$myrow 			= mysqli_fetch_array($res);
	$a_check		= $myrow["access"];
	
	if (isset($_POST["submit"])) {
		$subject = $_POST["subject"];	
		$message = $_POST["message"];	
		$email = $_POST["email"];
		$phone = $_POST["phone"];
		$company = $_POST["company"];
		$contact = $_POST["contact"];

		//radom ww maken + opsturen daarna md5 opslaan
		$tekens = "0123456789";  
		$ticketnr = "";  
		while(strlen($ticketnr)<5) 
		{  
			$ticketnr .= substr($tekens, rand(0,strlen($tekens)),1);  
		} 
			
		$ticketnr .= $_SESSION['uidGO'];
			
		mysqli_query($mysqli, "INSERT INTO support (uid,ticketnr, datetime,company,contact,email,phonenumber,subject,message) VALUES ('".$_SESSION['uidGO']."','$ticketnr','".date("Y-m-d H:i:s")."','$company','$contact','$email','$phone','$subject','$message')") or die(mysqli_error($mysqli));
		
		$subject_livechat = "Support request, $ticketnr";
		$subject_klant = "Support request received, $ticketnr";

		$message_livechat = "User:  ".$_SESSION['unameGO']."<br><br>
		<b>Contact information</b><br>
		Company:  $company<br>
		Contact:  $contact<br>
		Email:  $email<br>
		Phonenumber:  $phone<br><br>
		<b>Support / question</b><br>
		Subject:  $subject<br>
		Message:  $message<br>";
		$message_klant = "Hi $contact,<br><br>
		A request for support has been created. A representative will follow-up with you as soon as possible!<br><br>-- GoDashboard";
				
		/* Uitgezet ivm livechat auto reply ontvangst
		//stuur e-mail naar naar klant
		$mail_office = new PHPMailer();
		$mail_office->IsSMTP();
		// set smtp login
		$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
		// $mail->SMTPDebug  = 2;
		$mail_office->SMTPAuth = true;	  // enable SMTP authentication
		$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
		$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
		$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
		$mail_office->Password = "e9DcdmHE!"; // SMTP account password
		$mail_office->IsHTML();
		$mail_office->SetFrom("support@godashboard.nl", "GoDashboard");
		
		$mail_office->AddAddress("$email");
		$mail_office->Subject = $subject_klant;
		$mail_office->Body = $message_klant;
		
		$mail_office->Send();		*/

		
		//stuur e-mail naar naar livechat
		$mail_office = new PHPMailer();
		$mail_office->IsSMTP();
		// set smtp login
		$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
		// $mail->SMTPDebug  = 2;
		$mail_office->SMTPAuth = true;	  // enable SMTP authentication
		$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
		$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
		$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
		$mail_office->Password = "e9DcdmHE!"; // SMTP account password
		$mail_office->IsHTML();
		$mail_office->SetFrom("$email", "$contact");
		
		$mail_office->AddAddress("6291541@tickets.livechatinc.com");
		$mail_office->Subject = $subject_livechat;
		$mail_office->Body = $message_livechat;
		
		$mail_office->Send();
		
		//stuur e-mail naar naar support
		$mail_office = new PHPMailer();
		$mail_office->IsSMTP();
		// set smtp login
		$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
		// $mail->SMTPDebug  = 2;
		$mail_office->SMTPAuth = true;	  // enable SMTP authentication
		$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
		$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
		$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
		$mail_office->Password = "e9DcdmHE!"; // SMTP account password
		$mail_office->IsHTML();
		$mail_office->SetFrom("$email", "$contact");
		
		$mail_office->AddAddress("hallo@godashboard.nl");
		$mail_office->Subject = $subject_livechat;
		$mail_office->Body = $message_livechat;
		
		$mail_office->Send();
		
		$result ="<br><div class=\"alert alert-success\">Your ticketnr is: $ticketnr. A representative will follow-up with you as soon as possible!</div>";
		
	}
	else {
		$res		= mysqli_query($mysqli, "SELECT * FROM users WHERE uid='".$_SESSION['uidGO']."'");
		$myrow 			= mysqli_fetch_array($res);
		$contact		= $myrow["contact"];
		$company		= $myrow["company"];
		$vat			= $myrow["vat"];
		$phonenumber	= $myrow["phonenumber"];
		$website		= $myrow["website"];	
		$email			= $myrow["email"];
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoDashboard</title>
<link rel="shortcut icon" href="favicon.ico" />

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
	/* Skills Progess Bar */
	.progress {
		margin: 15px;
	}
	img {
		margin-left:5px;
		margin-top:1px;
	}
	</style>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="GoDashboardLogo220x50.png">
            </div>
<?php include("topnav-v2.php"); ?>
            <div class="navbar-default sidebar" role="navigation">
<?php include("sidenav-v2.php"); ?>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">         
            <div class="row">
                <div class="col-lg-12">
                    &nbsp;
                </div>
                <!-- /.col-lg-12 -->
            </div>
                        <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                                        <div style="width:100%">
						<?php
                            	echo "<div class=\"modal fade\" id=\"myModalREA\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"top: 31px !important; display: none;\">
						 <form role=\"form\" method=\"post\" action=\"$rea_page\">
							<div class=\"modal-dialog\">
										<div class=\"modal-content\">
											<div class=\"modal-header\">
												<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
												<h4 class=\"modal-title\" id=\"myModalLabel\">"; 
												echo "Are you sure you would like to re-activate your subscription?
												</div>
												<div class=\"modal-footer\">
												<button id=\"submit\" name=\"submit\" class=\"btn btn-primary\" type=\"submit\">Re-activate subscription</button>"; 
												echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button></div>";
										echo "</div>
									</div>
								   </form>
								</div>";
			?>			<?php echo $result; ?></div>                      
               <!-- /progressbar when account is changed or payed -->
                          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 51px !important; display: none;">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <h4 class="modal-title" id="myModalLabel">In Progress</h4>
                                  </div>
                                  <div class="modal-body">
                                  <div class="progress">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar"
                                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                        </div>
                                      </div>
                                      
                                  </div>
                              </div>
                              <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->            
            <div class="row">
                <div class="col-lg-8">
                   <div class="panel panel-default">
                     <div class="panel-body">
                     <?php 
					 	if (!isset($_POST["submit"])) {
					 ?>
                        <div style="width:100%"><h4>Please fill out form below</h4></div><br>
                        <form role="form" method="post" action="support.php">
                            <fieldset>
<?php                             
                           echo "<fieldset>";
						   if ($company!="$contact") {
							   echo "
                                <div class=\"form-group\">
								 	<label>Company</label>
                                    <input class=\"form-control\" placeholder=\"Company name\" id=\"company\" name=\"company\" type=\"text\" value=\"".$company."\">
                                </div>";
						   }					   
                           echo "<div class=\"form-group\">
						    		<label>Contact</label>
                                    <input class=\"form-control\" placeholder=\"Contact name\" id=\"contact\" name=\"contact\" type=\"text\" required value=\"".$contact."\">
                                </div>
                                <div class=\"form-group\">
								 	<label>E-mail</label>
                                    <input class=\"form-control\" placeholder=\"E-mail\" id=\"email\" name=\"email\" type=\"email\" required value=\"".$_SESSION['unameGO']."\">
                                </div>
                                <div class=\"form-group\">
								 	<label>Phonenumber</label>
                                    <input class=\"form-control\" placeholder=\"Phonenumber\" id=\"phone\" name=\"phone\" type=\"text\" required value=\"".$phonenumber."\">
                                </div>
								<div class=\"form-group\">
								 	<label>Subject</label>
                                    <input class=\"form-control\" placeholder=\"Subject\" id=\"subject\" name=\"subject\" type=\"text\" required autofocus>
                                </div>
								<div class=\"form-group\">
                                     <label>Message / support question</label>
                                     <textarea class=\"form-control\" rows=\"10\" id=\"message\" name=\"message\"></textarea>
                                        </div>
                                <button id=\"submit\" name=\"submit\" value=\"\" class=\"btn btn-success\" type=\"submit\">Submit</button>";
?>
                            </fieldset>
                        </form>

                <?php 
						}
						else {
							echo "Support request received!";
						}
				?>                
                </div>
              </div>
            </div>
            <div class="col-lg-4">
            <div class="panel panel-default">
<?php 
$query = mysqli_query($mysqli, "select account, registration, startDay, endDate, reactivated from users where uid='".$_SESSION["uidGO"]."'") or die(mysqli_error($mysqli));
$row = mysqli_fetch_array($query);
$account = $row["account"];
$startDay = $row["startDay"];
$endDate = $row["endDate"];
$reactivated = $row["reactivated"];

if ($account=="FREE") { 
	$registration = strtotime($row["registration"]);
	$now = time(); // or your date as well
	$datediff = $now - $registration;
	$exp = 14 - round($datediff / (60 * 60 * 24));
?>
          <div class="panel-heading">
             <?php echo "$exp"; ?> days trial left
          </div>
          <div class="panel-body">
          <a href="account-v2.php#pricing" class="btn btn-block btn-primary">VIEW PRICING</a>
 </div>
<?php
}   
elseif ($endDate!="") { 
	$registration = strtotime($row["endDate"]);
	$now = time(); // or your date as well
	$datediff = $registration - $now;
	$exp = round($datediff / (60 * 60 * 24));
?>
          <div class="panel-heading">
             <?php echo "$exp"; ?> days left
          </div>
          <div class="panel-body">
<?php
		  $rea_page = "plans/reactivate.php?p=".basename($_SERVER['PHP_SELF'])."";	 
		  echo "
					   <button class=\"btn btn-block btn-primary\" data-toggle=\"modal\" data-target=\"#myModalREA\">
									RE-ACTIVATE ACCOUNT
								</button>
								<div class=\"modal fade\" id=\"myModalREA\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"top: 31px !important; display: none;\">
						 <form role=\"form\" method=\"post\" action=\"$rea_page\">
							<div class=\"modal-dialog\">
										<div class=\"modal-content\">
											<div class=\"modal-header\">
												<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
												<h4 class=\"modal-title\" id=\"myModalLabel\">"; 
												echo "Are you sure you would like to re-activate your subscription?
												</div>
												<div class=\"modal-footer\">
												<button id=\"submit\" name=\"submit\" class=\"btn btn-primary\" type=\"submit\">Re-activate subscription</button>"; 
												echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button></div>";
										echo "</div>
									</div>
								   </form>
								</div>";
			?>      
 </div>
<?php
}
else {
	$query = mysqli_query($mysqli, "select period from plans where uid='".$_SESSION["uidGO"]."' and endDate IS NULL and period!='One-off' and period!='Set-up'") or die(mysqli_error($mysqli));
	$row = mysqli_fetch_array($query);
	$period = $row["period"];
	
	$qy = mysqli_query($mysqli, "SELECT startDay,startMonth FROM users WHERE uid='".$_SESSION["uidGO"]."'");
	$gegevens = mysqli_fetch_array($qy);
	   
	$startDay = $gegevens['startDay'];
	$startMonth = $gegevens['startMonth'];
	$today = date("d");
	$tomonth = date("m");
	$date = date("Y-m-d");
	if ($period=="month") {
		if ($today>=$startDay) $month = date('M Y', strtotime("+1 months", strtotime($date)));
		else $month = date("M Y");
		
		$renewDate = "".$startDay." ".$month."";
	}
	else {
		if (($today>=$startDay) and ($tomonth>=$startMonth)) $Y = date('Y', strtotime("+1 years", strtotime($date)));
		else $Y = date("Y");
		
		$month = date('M', strtotime("$Y-$startMonth-01"));
		
		$renewDate = "".$startDay." ".$month." $Y";
	}
	
	$end_page = "plans/end.php?p=".basename($_SERVER['PHP_SELF'])."";	
?>
          <div class="panel-heading">
               Subscribed <?php /*?><em><small>(renewal on <?php echo $renewDate ?>)</small></em><?php */?>
          </div>
          <div class="panel-body">
                    <a href="account-v2.php?#pricing" class="btn btn-block btn-primary">UPGRADE / DOWNGRADE</a><br>
<?php             
							echo "
					   <button class=\"btn btn-block btn-default\" data-toggle=\"modal\" data-target=\"#myModalEND\">
									End subscription
								</button>
								<div class=\"modal fade\" id=\"myModalEND\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"top: 31px !important; display: none;\">
						 <form role=\"form\" method=\"post\" action=\"$end_page\">
							<div class=\"modal-dialog\">
										<div class=\"modal-content\">
											<div class=\"modal-header\">
												<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
												<h4 class=\"modal-title\" id=\"myModalLabel\">"; 
												echo "Are you sure you would like to end your subscription?<br>This will take effect $renewDate
												</div>
												<div class=\"modal-footer\">
												<button id=\"submit\" name=\"submit\" class=\"btn btn-danger\" type=\"submit\">End subscription</button>"; 
												echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button></div>";
										echo "</div>
									</div>
								   </form>
								</div>";      
?>      
            
                                     
		  </div>
<?php
}     
?>  
             </div>                           
          </div>      
          <div class="col-lg-4">
                   <div class="panel panel-default">
                     <div class="panel-body">
                        <h4>Contact</h4>
                        <p><a href="https://calendly.com/godashboard/60min-consult/" target="_blank">Data Guru Consult: Book here</a></p>
                        <p>Telefoon: <span class="text-success">085 303 72 58</span></p>
                        <p>E-mail: <span class="text-success">hallo@godashboard.nl</span></p>
                        <hr>
                        <h4>Location</h4>
                        <p>GoDashboard (4de etage)</p>
                        <p>Anna van buerenplein 41</p>
                        <p>2595 DA DEN HAAG</p>
                        <p>The Netherlands</p>
                        <hr>
						<h4>Postal address</h4>
						<p>GoDashboard</p>
						<p>Postbus 11766</p>
						<p>2502 AT DEN HAAG</p>
                        <p>The Netherlands</p>
					</div>
				   </div>
                  <div class="panel panel-default">
                  <div class="panel-body">
                      <form role="form" action="support-v2.php" method="post" id="myAccess">
                      <div class="row">                        
                      <div class="col-lg-12">
                              <div class="form-group">
                                  <h4>Grant Access</h4>
                                  <p>Sometimes the best way to resolve an issue or validate a Data Visual, data source or formula, is to allow a member of the Support Team to sign in to your account. By signing in, the Support Agent is able to share your experience.</p>
                                  <div class="form-group">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="a_check" id="a_check" <?php if ($a_check=="1") echo "checked"; ?>> <b>Grant Access to Support</b>
                                        </label>
                                  </div>
                                  <button type="submit" class="btn btn-default" id="access" name="access">Save</button>
                              </div>
                        </div>
                      </div>
                      </form>
                    </div> 
               	   </div> 
			</div>
        </div>    							
	</div>								
</div>					

    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script>

	function myTimeout10() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '10%');}, 500); // WAIT 1 second
		  return(myVar);
	}
	function myTimeout40() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '40%');}, 500); // WAIT 1 second
		  return(myVar);
	}
	function myTimeout90() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '90%');}, 500); // WAIT 1 second
		  return(myVar);
	}
    $(document).ready(function () {
	  	$("#submit").click(function(){
			myTimeout10();
			myTimeout40();
			myTimeout90();	
			$('#myModal').modal("show");			
	  	});

		// JavaScript Document
		<!-- Start of LiveChat (www.livechatinc.com) code -->
		window.__lc = window.__lc || {};
		window.__lc.license = 6291541;
		(function() {
		  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
		  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
		<!-- End of LiveChat code -->
	});
</script>
</body>

</html>
<?php } ?>