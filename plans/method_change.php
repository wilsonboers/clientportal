<style>
#myModalCHANGE2 .modal-dialog, #myModalCHANGE1 .modal-dialog {
    width: 350px;
}
#myModalCHANGE2 .modal-header, #myModalCHANGE1 .modal-header {
    text-align: center;
    background-color: #e8e9eb;
}
#myModalCHANGE2 .modal-body, #myModalCHANGE1 .modal-body {		
	text-align:center !important;
	padding-top: 20px !important;	
	padding-right: 50px !important;	
	padding-left: 50px !important;
	padding-bottom: 50px !important;
}
.Header-loggedInBar {
    height: 1px;
    background-image: -webkit-radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
    background-image: radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
	background-color: #B7B7B7;
	width: 50%;
	margin-left:25%;
	margin-top: 0px;
	margin-bottom: 0px;
}
#myModalLabel {	
	margin: 1px !important;
}
.stripe-button-el span {
	display: none !important;
}
.stripe-button-el {
	display: none !important;
}
.center-block {
    float: none;
    margin-left: auto;
    margin-right: auto;
}

.input-group .icon-addon .form-control {
    border-radius: 0;
}

.icon-addon {
    position: relative;
    color: #555;
    display: block;
}

.icon-addon:after,
.icon-addon:before {
    display: table;
    content: " ";
}

.icon-addon:after {
    clear: both;
}

.icon-addon.addon-md .glyphicon,
.icon-addon .glyphicon, 
.icon-addon.addon-md .fa,
.icon-addon .fa {
    position: absolute;
    z-index: 2;
    left: 10px;
    font-size: 14px;
    width: 20px;
    margin-left: -2.5px;
    text-align: center;
    padding: 10px 0;
    top: 1px
}

.icon-addon.addon-lg .form-control {
    line-height: 1.33;
    height: 46px;
    font-size: 18px;
    padding: 10px 16px 10px 40px;
}

.icon-addon.addon-sm .form-control {
    height: 30px;
    padding: 5px 10px 5px 28px;
    font-size: 12px;
    line-height: 1.5;
}

.icon-addon.addon-lg .fa,
.icon-addon.addon-lg .glyphicon {
    font-size: 18px;
    margin-left: 0;
    left: 11px;
    top: 4px;
}

.icon-addon.addon-md .form-control,
.icon-addon .form-control {
    padding-left: 30px;
    float: left;
    font-weight: normal;
}

.icon-addon.addon-sm .fa,
.icon-addon.addon-sm .glyphicon {
    margin-left: 0;
    font-size: 12px;
    left: 5px;
    top: -1px
}

.icon-addon .form-control:focus + .glyphicon,
.icon-addon:hover .glyphicon,
.icon-addon .form-control:focus + .fa,
.icon-addon:hover .fa {
    color: #2580db;
}
.btn-info {
    position: relative;
    border-radius: 4px;
    background-color: #3ea8e5;
    background-image: -webkit-linear-gradient(top,#44b1e8,#3098de);
    background-image: linear-gradient(-180deg,#44b1e8,#3098de);
    box-shadow: 0 1px 0 0 rgba(46,86,153,.15),inset 0 1px 0 0 rgba(46,86,153,.1),inset 0 -1px 0 0 rgba(46,86,153,.4);
    font-size: 17px;
    line-height: 21px;
    height: 37px;
    font-weight: 700;
    text-shadow: 0 -1px 0 rgba(0,0,0,.12);
    color: #fff;
    cursor: pointer;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
.fa-credit-card-alt, .fa-cc-stripe, .fa-university {
	font-size:smaller !important;
}
</style>
<?php
$pquery = mysqli_query($mysqli, "select * from plans where uid='".$_SESSION["uidGO"]."' and active='1' and endDate IS NULL and period!='One-off' and period!='Set-up' LIMIT 1") or die(mysqli_error($mysqli));
$count = mysqli_num_rows($pquery);
if ($count==1) {
	$prow = mysqli_fetch_array($pquery);
	$plan = $prow["plan"];	 
	$pamount = $prow["amount"];	
	$costs = round($pamount*100); 
	$pperiod = $prow["period"];	 	
	$description = "Payment on renewal date";
	
	//s bepalen
	if ($pperiod=="month") {
		$date = strtotime("$nextYearMonth-$nextMonth-$startDay");
		if ($plan=="STARTER") {
			$s=1;
		}
		elseif ($plan=="VALUE") {
			$s=2;
		}
		elseif ($plan=="PRO") {
			$s=3;
		}
		elseif ($plan=="PREMIUM") {
			$s=4;
		}
	}
	elseif ($pperiod=="year") {
		$date = strtotime("$nextYear-$startMonth-$startDay");
		if ($plan=="STARTER") {
			$s=5;
		}
		elseif ($plan=="VALUE") {
			$s=6;
		}
		elseif ($plan=="PRO") {
			$s=7;
		}
		elseif ($plan=="PREMIUM") {
			$s=8;
		}
	}
?>
<!--START STRIPE BUTTON CARD-->
<?php echo "<form action=\"plans/change_paymethod.php?p=".basename($_SERVER['PHP_SELF'])."&method=1&s=".base64_encode($s)."&startdate=$date\" method=\"POST\" style=\"margin-bottom:5px !important;\">"; ?>
<button type="submit" class="btn btn-block btn-social btn-dropbox">    
 <i class="fa fa-cc-stripe" aria-hidden="true"></i> Credit card
<script
src="https://checkout.stripe.com/checkout.js" class="stripe-button"
data-key="pk_live_SAXNGqm8RSm1yEgjcIIaguaQ"
data-image="GO.png"
data-email="<?php echo "".$_SESSION["unameGO"].""; ?>"
data-name="GoDashboard account"
data-description="<?php echo "$description"; ?>"
data-amount="<?php echo "$costs"; ?>"
data-currency="eur"
data-label="Next payment with card">
</script>            
</button>
</form>
<!--START STRIPE BUTTON IDEAL-->
<a class="btn btn-block btn-social btn-dropbox" style="margin-bottom: 5px !important;"" data-toggle="modal" data-target="#myModalCHANGE1">
      <i class="fa fa-university" aria-hidden="true"></i> iDeal
</a>
<!--START DIRECT DEBIT BUTTON AND ALERT BOX-->
<a class="btn btn-block btn-social btn-dropbox" style="margin-bottom: 5px !important;"" data-toggle="modal" data-target="#myModalCHANGE2">
      <i class="fa fa-credit-card-alt" aria-hidden="true"></i> Direct debit / Incasso
</a>  	 
<?php } ?>