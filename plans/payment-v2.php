<style>
#myModalDEBIT1 .modal-dialog, #myModalINVOICE1 .modal-dialog, #myModalIDEAL1 .modal-dialog,
#myModalDEBIT2 .modal-dialog, #myModalINVOICE2 .modal-dialog, #myModalIDEAL2 .modal-dialog,
#myModalDEBIT3 .modal-dialog, #myModalINVOICE3 .modal-dialog, #myModalIDEAL3 .modal-dialog,
#myModalDEBIT4 .modal-dialog, #myModalINVOICE4 .modal-dialog, #myModalIDEAL4 .modal-dialog,
#myModalDEBIT5 .modal-dialog, #myModalINVOICE5 .modal-dialog, #myModalIDEAL5 .modal-dialog,
#myModalDEBIT6 .modal-dialog, #myModalINVOICE6 .modal-dialog, #myModalIDEAL6 .modal-dialog,
#myModalDEBIT7 .modal-dialog, #myModalINVOICE7 .modal-dialog, #myModalIDEAL7 .modal-dialog,
#myModalDEBIT8 .modal-dialog, #myModalINVOICE8 .modal-dialog, #myModalIDEAL8 .modal-dialog {
    width: 350px;
}
#myModalDEBIT1 .modal-header, #myModalINVOICE1 .modal-header, #myModalIDEAL1 .modal-header,
#myModalDEBIT2 .modal-header, #myModalINVOICE2 .modal-header, #myModalIDEAL2 .modal-header,
#myModalDEBIT3 .modal-header, #myModalINVOICE3 .modal-header, #myModalIDEAL3 .modal-header,
#myModalDEBIT4 .modal-header, #myModalINVOICE4 .modal-header, #myModalIDEAL4 .modal-header,
#myModalDEBIT5 .modal-header, #myModalINVOICE5 .modal-header, #myModalIDEAL5 .modal-header,
#myModalDEBIT6 .modal-header, #myModalINVOICE6 .modal-header, #myModalIDEAL6 .modal-header,
#myModalDEBIT7 .modal-header, #myModalINVOICE7 .modal-header, #myModalIDEAL7 .modal-header,
#myModalDEBIT8 .modal-header, #myModalINVOICE8 .modal-header, #myModalIDEAL8 .modal-header {
    text-align: center;
    background-color: #e8e9eb;
}
#myModalDEBIT1 .modal-body, #myModalINVOICE1 .modal-body, #myModalIDEAL1 .modal-body,
#myModalDEBIT2 .modal-body, #myModalINVOICE2 .modal-body, #myModalIDEAL2 .modal-body,
#myModalDEBIT3 .modal-body, #myModalINVOICE3 .modal-body, #myModalIDEAL3 .modal-body,
#myModalDEBIT4 .modal-body, #myModalINVOICE4 .modal-body, #myModalIDEAL4 .modal-body,
#myModalDEBIT5 .modal-body, #myModalINVOICE5 .modal-body, #myModalIDEAL5 .modal-body,
#myModalDEBIT6 .modal-body, #myModalINVOICE6 .modal-body, #myModalIDEAL6 .modal-body,
#myModalDEBIT7 .modal-body, #myModalINVOICE7 .modal-body, #myModalIDEAL7 .modal-body,
#myModalDEBIT8 .modal-body, #myModalINVOICE8 .modal-body, #myModalIDEAL8 .modal-body {		
	text-align:center !important;
	padding-top: 20px !important;	
	padding-right: 50px !important;	
	padding-left: 50px !important;
	padding-bottom: 50px !important;
}
.Header-loggedInBar {
    height: 1px;
    background-image: -webkit-radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
    background-image: radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
	background-color: #B7B7B7;
	width: 50%;
	margin-left:25%;
	margin-top: 0px;
	margin-bottom: 0px;
}
#myModalLabel {	
	margin: 1px !important;
}
.stripe-button-el span {
	display: none !important;
}
.stripe-button-el {
	display: none !important;
}
.center-block {
    float: none;
    margin-left: auto;
    margin-right: auto;
}

.input-group .icon-addon .form-control {
    border-radius: 0;
}

.icon-addon {
    position: relative;
    color: #555;
    display: block;
}

.icon-addon:after,
.icon-addon:before {
    display: table;
    content: " ";
}

.icon-addon:after {
    clear: both;
}

.icon-addon.addon-md .glyphicon,
.icon-addon .glyphicon, 
.icon-addon.addon-md .fa,
.icon-addon .fa {
    position: absolute;
    z-index: 2;
    left: 10px;
    font-size: 14px;
    width: 20px;
    margin-left: -2.5px;
    text-align: center;
    padding: 10px 0;
    top: 1px
}

.icon-addon.addon-lg .form-control {
    line-height: 1.33;
    height: 46px;
    font-size: 18px;
    padding: 10px 16px 10px 40px;
}

.icon-addon.addon-sm .form-control {
    height: 30px;
    padding: 5px 10px 5px 28px;
    font-size: 12px;
    line-height: 1.5;
}

.icon-addon.addon-lg .fa,
.icon-addon.addon-lg .glyphicon {
    font-size: 18px;
    margin-left: 0;
    left: 11px;
    top: 4px;
}

.icon-addon.addon-md .form-control,
.icon-addon .form-control {
    padding-left: 30px;
    float: left;
    font-weight: normal;
}

.icon-addon.addon-sm .fa,
.icon-addon.addon-sm .glyphicon {
    margin-left: 0;
    font-size: 12px;
    left: 5px;
    top: -1px
}

.icon-addon .form-control:focus + .glyphicon,
.icon-addon:hover .glyphicon,
.icon-addon .form-control:focus + .fa,
.icon-addon:hover .fa {
    color: #2580db;
}
.btn-info {
    position: relative;
    border-radius: 4px;
    background-color: #3ea8e5;
    background-image: -webkit-linear-gradient(top,#44b1e8,#3098de);
    background-image: linear-gradient(-180deg,#44b1e8,#3098de);
    box-shadow: 0 1px 0 0 rgba(46,86,153,.15),inset 0 1px 0 0 rgba(46,86,153,.1),inset 0 -1px 0 0 rgba(46,86,153,.4);
    font-size: 17px;
    line-height: 21px;
    height: 37px;
    font-weight: 700;
    text-shadow: 0 -1px 0 rgba(0,0,0,.12);
    color: #fff;
    cursor: pointer;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
.fa-credit-card-alt, .fa-cc-stripe, .fa-university {
	font-size:smaller !important;
}
</style>
<?php
//tussentijdse berekening opslaan
//alles ouder dan 0.5 dag weggooien
$olddate = date("Y-m-d H:i:s", mktime(date("H")-12, date("i"), date("s"), date("m"), date("d"), date("Y")));
mysqli_query($mysqli, "DELETE FROM paylog WHERE datetime<'$olddate'") or die(mysqli_error($mysqli));
//nieuwe sessies erin zetten
mysqli_query($mysqli, "INSERT INTO paylog (uid,setup,s) VALUES ('".$_SESSION['uidGO']."','$costs_setup','$s')") or die(mysqli_error($mysqli));
?>
<!--START STRIPE BUTTON CARD-->
<?php echo "<form action=\"stripe-v2/index.php?p=".basename($_SERVER['PHP_SELF'])."&reac=$reac&s=".base64_encode($s)."\" method=\"POST\" style=\"margin-bottom:5px !important;\">"; ?>
<button type="submit" class="btn btn-block btn-social btn-dropbox">    
 <i class="fa fa-cc-stripe" aria-hidden="true"></i> Credit card
<script
src="https://checkout.stripe.com/checkout.js" class="stripe-button"
data-key="pk_live_SAXNGqm8RSm1yEgjcIIaguaQ"
data-image="GO.png"
data-email="<?php echo "".$_SESSION["unameGO"].""; ?>"
data-name="GoDashboard account"
data-description="<?php echo "$description"; ?>"
data-amount="<?php echo "$costs"; ?>"
data-currency="eur"
data-label="Pay with card">
</script>            
</button>
</form>
<!--START STRIPE BUTTON IDEAL-->
<a class="btn btn-block btn-social btn-dropbox" style="margin-bottom: 5px !important;"" data-toggle="modal" data-target="#myModalIDEAL<?php echo $s; ?>">
      <i class="fa fa-university" aria-hidden="true"></i> iDeal
</a>
<div class="modal fade" id="myModalIDEAL<?php echo $s; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 31px !important; display: none;">
						 <?php echo "<form id=\"dd_$s\" role=\"form\" method=\"post\" action=\"stripe-v2/ideal.php?p=".basename($_SERVER['PHP_SELF'])."&reac=$reac&s=".base64_encode($s)."&amount=".round($costs,0)."\">"; ?>
							<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close " data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel"><b>GoDashboard account </b></h4><span class="text-muted small"><?php echo $description; ?>
                                                <hr class="Header-loggedInBar">
                                                <?php echo "".$_SESSION["unameGO"].""; ?>
											</div>
											<div class="modal-body">
                                            	<fieldset>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<?php include("sel-bank.php"); ?>
                                                        <label for="account" class="fa fa-university" rel="tooltip" title="bank"></label>           
                                                        </div>
                                                    </div>
  													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Account number" id="accountnumber" name="accountnumber" type="text" required>
                                                        <label for="accountnumber" class="glyphicon glyphicon-credit-card" rel="tooltip" title="accountnumber"></label>           
                                                        </div>
                                                    </div>                                                
                                                 </fieldset>
                                                 <button id="submit" name="submit" class="btn btn-block btn-info" type="submit">iDeal €<?php echo number_format(round($costs/100,2),2); ?></button>
												</div>
                                         </div>
									</div>
								   </form>
								</div>
<!--START DIRECT DEBIT BUTTON AND ALERT BOX-->
<a class="btn btn-block btn-social btn-dropbox" style="margin-bottom: 5px !important;"" data-toggle="modal" data-target="#myModalDEBIT<?php echo $s; ?>">
      <i class="fa fa-credit-card-alt" aria-hidden="true"></i> Direct debit / Incasso
</a>
<div class="modal fade" id="myModalDEBIT<?php echo $s; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 31px !important; display: none;">
						 <?php echo "<form id=\"dd_$s\" role=\"form\" method=\"post\" action=\"direct-debit-v2/index.php?p=".basename($_SERVER['PHP_SELF'])."&reac=$reac&s=".base64_encode($s)."\">"; ?>
							<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close " data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel"><b>GoDashboard account </b></h4><span class="text-muted small"><?php echo $description; ?>
                                                <hr class="Header-loggedInBar">
                                                <?php echo "".$_SESSION["unameGO"].""; ?>
											</div>
											<div class="modal-body">
                                            	<fieldset>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Account number" id="account" name="account" type="text" required>
                                                        <label for="account" class="glyphicon glyphicon-credit-card" rel="tooltip" title="account"></label>           
                                                        </div>
                                                    </div>
  													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Account holder" id="accountholder" name="accountholder" type="text" required>
                                                        <label for="accountholder" class="glyphicon glyphicon-user" rel="tooltip" title="accountholder"></label>           
                                                        </div>
                                                    </div>                                                 
                                                 </fieldset>
                                                 <button id="submit" name="submit" class="btn btn-block btn-info" type="submit">Direct debit €<?php echo number_format(round($costs/100,2),2); ?></button>
												</div>
                                         </div>
									</div>
								   </form>
								</div>    	 
