<?
// Config includen om met de MySQL database verbinding te maken
include("../config.php");	
require_once 'class.phpmailer.php';
require_once 'class.smtp.php';

$logData = serialize($_POST);
mysqli_query($mysqli, "INSERT INTO log (log) VALUES ('$logData')");

$method = $_GET["method"];
$p = $_GET["p"];

//stripe
if ($method==1) {
	$startdate = $_GET["startdate"];

	if (base64_decode($_GET['s'])==1) {
		$plan = "monthstarter"; //name of plan
	}
	elseif (base64_decode($_GET['s'])==2) {
		$plan = "monthvalue";
	}
	elseif (base64_decode($_GET['s'])==3) {
		$plan = "monthpro";
	}
	elseif (base64_decode($_GET['s'])==4) {
		$plan = "monthpremium"; //ultimate
	}
	elseif (base64_decode($_GET['s'])==5) {
		$plan = "yearstarter";
	}
	elseif (base64_decode($_GET['s'])==6) {
		$plan = "yearvalue";
	}
	elseif (base64_decode($_GET['s'])==7) {
		$plan = "yearpro";
	}
	elseif (base64_decode($_GET['s'])==8) {
		$plan = "yearpremium"; //ultimate
	}

	// If you're using Composer, use Composer's autoload:
	require_once('../../stripe/init.php');
	
	// Be sure to replace this with your actual test API key
	// (switch to the live key later)
	\Stripe\Stripe::setApiKey("sk_live_IyZCez0TNnkBlak8k5qvVXvJ");

	$qy = mysqli_query($mysqli, "SELECT t1.customerid_stripe, t2.subscriptionid FROM users as t1, stripe_subscriptions as t2 WHERE t1.uid='".$_SESSION["uidGO"]."' and t1.subid=t2.id and active='1'") or die(mysqli_error($mysqli));
	$gegevens = mysqli_fetch_array($qy);
	
	$customerid = $gegevens['customerid_stripe']; 	  
	$subscriptionid = $gegevens['subscriptionid']; 
	
	if ($subscriptionid!="") {
		//end current subscription
		try
		{
			$subscription = \Stripe\Subscription::retrieve("$subscriptionid");
			$subscription->cancel();
			
			mysqli_query($mysqli,"UPDATE stripe_subscriptions SET active='0' WHERE subscriptionid='$subscriptionid'") or die(mysqli_error($mysqli));
		}
		catch(Exception $e)
		{	
			mysqli_query($mysqli, "INSERT INTO stripe_errors (email,message) VALUES ('".$_POST['stripeEmail']."','".urlencode($e->getMessage())."')") or die(mysqli_error($mysqli));
		  
		  header("Location: ../".$_GET['p']."?change=error&message=".urlencode($e->getMessage())."");
		  exit;	
		}
	}

	//create new customer
	try
	{
	  $customer = \Stripe\Customer::create(array(
		'email' => $_POST['stripeEmail'],
		'source'  => $_POST['stripeToken'],
		'description' => 'GODASHBOARD'
	  ));
	  
	  $logData = serialize($customer);
	  mysqli_query($mysqli, "INSERT INTO log (log) VALUES ('$logData')");
	  
	  $customerid = $customer["id"];
	  
	  //asign to subscription
	  try
	  {
			$subscription = \Stripe\Subscription::create(array(
			  "customer" => "$customerid",
			  "plan" => "$plan",  	  
			  "trial_end" => "$startdate" //timestamp startdate
			));

			$logData = serialize($subscription);
			mysqli_query($mysqli, "INSERT INTO log (log) VALUES ('$logData')");
			
	  		$subscriptionid = $subscription["id"];
	  
			mysqli_query($mysqli, "INSERT INTO stripe_subscriptions (email,token,subscriptionid) VALUES ('".$_POST['stripeEmail']."','".$_POST['stripeToken']."','$subscriptionid')") or die(mysqli_error($mysqli));
			$subid = mysqli_insert_id($mysqli);
			  
			mysqli_query($mysqli,"UPDATE users SET account='stripe',customerid_stripe='$customerid',subid='$subid' WHERE uid='".$_SESSION["uidGO"]."'");

			//delete evt ideal of dd subscription
			mysqli_query($mysqli, "DELETE FROM ideal_subscriptions WHERE user='".$_SESSION["uidGO"]."' LIMIT 1") or die(mysqli_error($mysqli));
			
			$qy = mysqli_query($mysqli, "SELECT t1.subid FROM users as t1, dd_subscriptions as t2 WHERE t1.uid='".$_SESSION["uidGO"]."' and t1.subid=t2.id LIMIT 1") or die(mysqli_error($mysqli));
			$count = mysqli_num_rows($qy);
			if ($count==1) {
				$gegevens = mysqli_fetch_array($qy);
			
				$subid = $gegevens['subid']; 
				mysqli_query($mysqli, "DELETE FROM dd_subscriptions WHERE id='$subid' LIMIT 1") or die(mysqli_error($mysqli));	 
			}
			
			header("Location: ../".$_GET['p']."?change=success");
			exit;
	  }
	  catch(Exception $e)
	  {
			mysqli_query($mysqli, "INSERT INTO stripe_errors (email,message) VALUES ('".$_POST['stripeEmail']."','".$e->getMessage()."')") or die(mysqli_error($mysqli));
				
		  	header("Location: ../".$_GET['p']."?change=error&message=".urlencode($e->getMessage())."");
		  	exit;	
	  }		
	}
	catch(Exception $e)
	{
	  mysqli_query($mysqli, "INSERT INTO stripe_errors (email,message) VALUES ('".$_POST['stripeEmail']."','".$e->getMessage()."')") or die(mysqli_error($mysqli));
	  
		  header("Location: ../".$_GET['p']."?change=error&message=".urlencode($e->getMessage())."");
		  exit;	
	}
}
//ideal
elseif ($method==2) {
	$qy = mysqli_query($mysqli, "SELECT * FROM ideal_subscriptions WHERE user='".$_SESSION["uidGO"]."' LIMIT 1") or die(mysqli_error($mysqli));
	$count = mysqli_num_rows($qy);
	//update gegevens
	if ($count==1) { 
		mysqli_query($mysqli,"UPDATE ideal_subscriptions SET bank='".$_POST['bank']."', account='".$_POST['accountnumber']."' WHERE user='".$_SESSION["uidGO"]."'");	
	}
	//insert
	else {	  
		mysqli_query($mysqli, "INSERT INTO ideal_subscriptions (bank,account,user) VALUES ('".$_POST['bank']."','".$_POST['accountnumber']."','".$_SESSION["uidGO"]."')") or die(mysqli_error($mysqli));
		  
		mysqli_query($mysqli,"UPDATE users SET account='ideal' WHERE uid='".$_SESSION["uidGO"]."'");
	}
	
	//delete evt stripe of dd subscription
	$qy = mysqli_query($mysqli, "SELECT t1.subid FROM users as t1, stripe_subscriptions as t2 WHERE t1.uid='".$_SESSION["uidGO"]."' and t1.subid=t2.id LIMIT 1") or die(mysqli_error($mysqli));
	$count = mysqli_num_rows($qy);
	if ($count==1) {
		$gegevens = mysqli_fetch_array($qy);
	
		$subid = $gegevens['subid']; 
		mysqli_query($mysqli, "UPDATE stripe_subscriptions SET active='0' WHERE id='$subid' LIMIT 1") or die(mysqli_error($mysqli));	 
	}
	
	$qy = mysqli_query($mysqli, "SELECT t1.subid FROM users as t1, dd_subscriptions as t2 WHERE t1.uid='".$_SESSION["uidGO"]."' and t1.subid=t2.id LIMIT 1") or die(mysqli_error($mysqli));
	$count = mysqli_num_rows($qy);
	if ($count==1) {
		$gegevens = mysqli_fetch_array($qy);
	
		$subid = $gegevens['subid']; 
		mysqli_query($mysqli, "DELETE FROM dd_subscriptions WHERE id='$subid' LIMIT 1") or die(mysqli_error($mysqli));	 
	}
	
	header("Location: ../".$_GET['p']."?change=success");
	exit;
}
//dd
if ($method==3) {
	$qy = mysqli_query($mysqli, "SELECT t1.subid FROM users as t1, dd_subscriptions as t2 WHERE t1.uid='".$_SESSION["uidGO"]."' and t1.subid=t2.id LIMIT 1") or die(mysqli_error($mysqli));
	$count = mysqli_num_rows($qy);
	//update gegevens
	if ($count==1) {
		$gegevens = mysqli_fetch_array($qy);
	
		$subid = $gegevens['subid']; 	  

		mysqli_query($mysqli,"UPDATE dd_subscriptions SET account='".$_POST['account']."', accountholder='".$_POST['accountholder']."' WHERE id='$subid'");
	}
	//insert
	else {	  
		mysqli_query($mysqli, "INSERT INTO dd_subscriptions (account,accountholder) VALUES ('".$_POST['account']."','".$_POST['accountholder']."')") or die(mysqli_error($mysqli));
		$subid = mysqli_insert_id($mysqli);
		  
		mysqli_query($mysqli,"UPDATE users SET account='dd', subid='$subid' WHERE uid='".$_SESSION["uidGO"]."'");
	}
	
	//delete evt stripe of ideal subscription
	$qy = mysqli_query($mysqli, "SELECT t1.subid FROM users as t1, stripe_subscriptions as t2 WHERE t1.uid='".$_SESSION["uidGO"]."' and t1.subid=t2.id LIMIT 1") or die(mysqli_error($mysqli));
	$count = mysqli_num_rows($qy);
	if ($count==1) {
		$gegevens = mysqli_fetch_array($qy);
	
		$subid = $gegevens['subid']; 
		mysqli_query($mysqli, "UPDATE stripe_subscriptions SET active='0' WHERE id='$subid' LIMIT 1") or die(mysqli_error($mysqli));	 
	}
	
	mysqli_query($mysqli, "DELETE FROM ideal_subscriptions WHERE user='".$_SESSION["uidGO"]."' LIMIT 1") or die(mysqli_error($mysqli));
	
	header("Location: ../".$_GET['p']."?change=success");
	exit;
}
?>