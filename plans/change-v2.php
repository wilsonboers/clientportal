<?
// Config includen om met de MySQL database verbinding te maken
include("../config.php");	
require_once 'class.phpmailer.php';
require_once 'class.smtp.php';

$logData = serialize($_POST);
mysqli_query($mysqli, "INSERT INTO log (log) VALUES ('$logData')");

$uid = $_POST["uid"];
$account = $_POST["account"];
$costs = $_POST["costs"];
$costs_after = $_POST["costs_after"];
$oneoff = $_POST["oneoff"];
$date = $_POST["date"];
$plan = $_POST["plan"];
$period = $_POST["period"];
$rec = $_POST["rec"];

$array = explode("-", $date);
$day = $array[0];
$month = $array[1];
$year = $array[2];
$newDate = "$year-$month-$day";

//endDate = direct, nieuwe maandelijkse kosten
if (($costs!="") and ($rec==1)) mysqli_query($mysqli,"UPDATE plans SET endDate='".date("Y-m-d")."' WHERE uid='$uid' and endDate IS NULL") or die(mysqli_error($mysqli));
else mysqli_query($mysqli,"UPDATE plans SET endDate='$newDate' WHERE uid='$uid' and endDate IS NULL") or die(mysqli_error($mysqli));//endDate = startdate van nieuwe plan

//eenmalige kosten, alleen indien u[grade en van jaarlijks naar maandelijks //nr. 7
if ($oneoff!="") mysqli_query($mysqli, "INSERT INTO plans (uid,plan,period,startDate,amount) VALUES ('$uid','$plan','One-off','".date("Y-m-d")."','$oneoff')") or die(mysqli_error($mysqli));

//eenmalige kosten
if (($costs!="") and ($rec==0)) mysqli_query($mysqli, "INSERT INTO plans (uid,plan,period,startDate,amount) VALUES ('$uid','$plan','One-off','".date("Y-m-d")."','$costs')") or die(mysqli_error($mysqli));

//maandelijks tot startdatum nieuwe plan
if (($costs!="") and ($rec==1)) mysqli_query($mysqli, "INSERT INTO plans (uid,plan,period,startDate,endDate,amount) VALUES ('$uid','$plan','month','".date("Y-m-d")."','$newDate','$costs')") or die(mysqli_error($mysqli));

//recurring kosten
mysqli_query($mysqli, "INSERT INTO plans (uid,plan,period,startDate,amount) VALUES ('$uid','$plan','$period','$newDate','$costs_after')") or die(mysqli_error($mysqli));

//stuur e-mail
$subject = "Wijziging plan, controleer en pas evt. invoice en/of direct debit aan";
if (($costs!="") and ($rec==0)) {
	$message = "User: $uid<br>	
	Eenmalige kosten: $costs<br>
	Startdatum nieuwe plan: $newDate<br>
	Plan: $plan<br>
	Period: $period<br>
	Kosten na startdatum: $costs_after";
}
elseif (($costs!="") and ($rec==1)) {
	$message = "User: $uid<br>";
	if ($oneoff!="") $message .= "Eenmalige kosten: $oneoff<br>";
	$message .= "Maandelijkse kosten tot nieuwe startdatum: $costs<br>
	Startdatum nieuwe plan: $newDate<br>
	Plan: $plan<br>
	Period: $period<br>
	Kosten na startdatum: $costs_after";
}
else {
	$message = "User: $uid<br>	
	Startdatum nieuwe plan: $newDate<br>
	Plan: $plan<br>
	Period: $period<br>
	Kosten na startdatum: $costs_after";
}

$mail_office = new PHPMailer();
$mail_office->IsSMTP();
// set smtp login
$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
// $mail->SMTPDebug  = 2;
$mail_office->SMTPAuth = true;	  // enable SMTP authentication
$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
$mail_office->Password = "e9DcdmHE!"; // SMTP account password
$mail_office->IsHTML();
$mail_office->SetFrom('noreply@godashboard.nl', 'GoDashboard');

$mail_office->AddAddress("dr@godashboard.nl");
$mail_office->Subject = $subject;
$mail_office->Body = $message;

$mail_office->Send();
			
//indien betaling via stripe verloopt, dan wijzigingen doorvoeren in hun systeem
if ($account=="stripe") {
	if (($plan=="STARTER") and ($period=="month")){
		$plan = "monthstarter"; //name of plan
	}
	elseif (($plan=="VALUE") and ($period=="month")){
		$plan = "monthvalue";
	}
	elseif (($plan=="PRO") and ($period=="month")){
		$plan = "monthpro";
	}
	elseif (($plan=="PREMIUM") and ($period=="month")){
		$plan = "monthpremium"; //ultimate
	}
	elseif (($plan=="STARTER") and ($period=="year")){
		$plan = "yearstarter";
	}
	elseif (($plan=="VALUE") and ($period=="year")){
		$plan = "yearvalue";
	}
	elseif (($plan=="PRO") and ($period=="year")){
		$plan = "yearpro";
	}
	elseif (($plan=="PREMIUM") and ($period=="year")){
		$plan = "yearpremium"; //ultimate
	}
	
	//change 1: end current plan, bill a one-time item, add to new plan beginning from startdate
	if (($costs!="") and ($rec==0)) {
		$amount=round($costs*100,0); //costs in cents
		$params = "change1=1&amount=$amount&uid=$uid&plan=$plan&startdate=$newDate";
		$url = "https://godashboard.nl/go/client/stripe-v2/change1.php";

		$result = post_with_wait($url, $params);
		if ($result!="success") {
			//stuur e-mail
			$subject = "Error processing change payment stripe";
			$message = "Error processing change payment stripe. Error: $result. Check user: $uid";			 
			
			$mail_office = new PHPMailer();
			$mail_office->IsSMTP();
			// set smtp login
			$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
			// $mail->SMTPDebug  = 2;
			$mail_office->SMTPAuth = true;	  // enable SMTP authentication
			$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
			$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
			$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
			$mail_office->Password = "e9DcdmHE!"; // SMTP account password
			$mail_office->IsHTML();
			$mail_office->SetFrom('noreply@godashboard.nl', 'GoDashboard');
			
			$mail_office->AddAddress("dr@godashboard.nl");
			$mail_office->Subject = $subject;
			$mail_office->Body = $message;
			
			$mail_office->Send();			
		}
		else echo "OK";
	}
	//change 4: end current plan, dynamic create new plan and cancel_at_period_end, add to new plan beginning from startdate + oneoff payment		
	elseif (($oneoff!="") and ($rec==1)) {
		$amount=round($costs*100,0); //costs in cents
		$oneoff=round($oneoff*100,0); //oneoff in cents
		$params = "change4=1&oneoff=$oneoff&amount=$amount&uid=$uid&plan=$plan&startdate=$newDate";
		$url = "https://godashboard.nl/go/client/stripe-v2/change4.php";

		$result = post_with_wait($url, $params);
		if (preg_match("/success/i",$result)) {
			//stuur e-mail
			$subject = "Wijziging abo stripe";
			$message = "$result";			 
			
			$mail_office = new PHPMailer();
			$mail_office->IsSMTP();
			// set smtp login
			$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
			// $mail->SMTPDebug  = 2;
			$mail_office->SMTPAuth = true;	  // enable SMTP authentication
			$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
			$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
			$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
			$mail_office->Password = "e9DcdmHE!"; // SMTP account password
			$mail_office->IsHTML();
			$mail_office->SetFrom('noreply@godashboard.nl', 'GoDashboard');
			
			$mail_office->AddAddress("dr@godashboard.nl");
			$mail_office->Subject = $subject;
			$mail_office->Body = $message;
			
			$mail_office->Send();	
			
			echo "OK";			
		}
		else {
			//stuur e-mail
			$subject = "Error processing change payment stripe";
			$message = "Error processing change payment stripe. Error: $result. Check user: $uid";			 
			
			$mail_office = new PHPMailer();
			$mail_office->IsSMTP();
			// set smtp login
			$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
			// $mail->SMTPDebug  = 2;
			$mail_office->SMTPAuth = true;	  // enable SMTP authentication
			$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
			$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
			$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
			$mail_office->Password = "e9DcdmHE!"; // SMTP account password
			$mail_office->IsHTML();
			$mail_office->SetFrom('noreply@godashboard.nl', 'GoDashboard');
			
			$mail_office->AddAddress("dr@godashboard.nl");
			$mail_office->Subject = $subject;
			$mail_office->Body = $message;
			
			$mail_office->Send();			
		}	
	}	
	//change 2: end current plan, dynamic create new plan and cancel_at_period_end, add to new plan beginning from startdate		
	elseif (($costs!="") and ($rec==1)) {
		$amount=round($costs*100,0); //costs in cents
		$params = "change2=1&amount=$amount&uid=$uid&plan=$plan&startdate=$newDate";
		$url = "https://godashboard.nl/go/client/stripe-v2/change2.php";

		$result = post_with_wait($url, $params);
		if (preg_match("/success/i",$result)) {
			//stuur e-mail
			$subject = "Wijziging abo stripe";
			$message = "$result";			 
			
			$mail_office = new PHPMailer();
			$mail_office->IsSMTP();
			// set smtp login
			$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
			// $mail->SMTPDebug  = 2;
			$mail_office->SMTPAuth = true;	  // enable SMTP authentication
			$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
			$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
			$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
			$mail_office->Password = "e9DcdmHE!"; // SMTP account password
			$mail_office->IsHTML();
			$mail_office->SetFrom('noreply@godashboard.nl', 'GoDashboard');
			
			$mail_office->AddAddress("dr@godashboard.nl");
			$mail_office->Subject = $subject;
			$mail_office->Body = $message;
			
			$mail_office->Send();	
			
			echo "OK";			
		}
		else {
			//stuur e-mail
			$subject = "Error processing change payment stripe";
			$message = "Error processing change payment stripe. Error: $result. Check user: $uid";			 
			
			$mail_office = new PHPMailer();
			$mail_office->IsSMTP();
			// set smtp login
			$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
			// $mail->SMTPDebug  = 2;
			$mail_office->SMTPAuth = true;	  // enable SMTP authentication
			$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
			$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
			$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
			$mail_office->Password = "e9DcdmHE!"; // SMTP account password
			$mail_office->IsHTML();
			$mail_office->SetFrom('noreply@godashboard.nl', 'GoDashboard');
			
			$mail_office->AddAddress("dr@godashboard.nl");
			$mail_office->Subject = $subject;
			$mail_office->Body = $message;
			
			$mail_office->Send();			
		}	
	}
	//change 3: end current plan cancel_at_period_end, add to new plan beginning from startdate
	else {
		$amount=round($costs*100,0); //costs in cents
		$params = "change3=1&amount=$amount&uid=$uid&plan=$plan&startdate=$newDate";
		$url = "https://godashboard.nl/go/client/stripe-v2/change3.php";

		$result = post_with_wait($url, $params);
		if (preg_match("/success/i",$result)) {
			//stuur e-mail
			$subject = "Wijziging abo stripe";
			$message = "$result";			 
			
			$mail_office = new PHPMailer();
			$mail_office->IsSMTP();
			// set smtp login
			$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
			// $mail->SMTPDebug  = 2;
			$mail_office->SMTPAuth = true;	  // enable SMTP authentication
			$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
			$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
			$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
			$mail_office->Password = "e9DcdmHE!"; // SMTP account password
			$mail_office->IsHTML();
			$mail_office->SetFrom('noreply@godashboard.nl', 'GoDashboard');
			
			$mail_office->AddAddress("dr@godashboard.nl");
			$mail_office->Subject = $subject;
			$mail_office->Body = $message;
			
			$mail_office->Send();	
			
			echo "OK";			
		}
		else {
			//stuur e-mail
			$subject = "Error processing change payment stripe";
			$message = "Error processing change payment stripe. Error: $result. Check user: $uid";			 
			
			$mail_office = new PHPMailer();
			$mail_office->IsSMTP();
			// set smtp login
			$mail_office->Host = "n1plcpnl0059.prod.ams1.secureserver.net"; // SMTP server smtp.strato.de
			// $mail->SMTPDebug  = 2;
			$mail_office->SMTPAuth = true;	  // enable SMTP authentication
			$mail_office->SMTPSecure = "ssl"; // icm SMTPAuth altijd gebruiken
			$mail_office->Port = 465;	 // 25 of 465 set the SMTP port for the GMAIL server
			$mail_office->Username = "officemanager_nl@iventurecompany.com"; // SMTP account username
			$mail_office->Password = "e9DcdmHE!"; // SMTP account password
			$mail_office->IsHTML();
			$mail_office->SetFrom('noreply@godashboard.nl', 'GoDashboard');
			
			$mail_office->AddAddress("dr@godashboard.nl");
			$mail_office->Subject = $subject;
			$mail_office->Body = $message;
			
			$mail_office->Send();			
		}	
	}
}
else echo "OK";

?>