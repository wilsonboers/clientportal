<style>
#myModalIDEALCHANGE1 .modal-dialog,
#myModalIDEALCHANGE2 .modal-dialog,
#myModalIDEALCHANGE3 .modal-dialog,
#myModalIDEALCHANGE4 .modal-dialog,
#myModalIDEALCHANGE5 .modal-dialog,
#myModalIDEALCHANGE6 .modal-dialog,
#myModalIDEALCHANGE7 .modal-dialog,
#myModalIDEALCHANGE8 .modal-dialog {
    width: 350px;
}
#myModalIDEALCHANGE1 .modal-header,
#myModalIDEALCHANGE2 .modal-header,
#myModalIDEALCHANGE3 .modal-header,
#myModalIDEALCHANGE4 .modal-header,
#myModalIDEALCHANGE5 .modal-header,
#myModalIDEALCHANGE6 .modal-header,
#myModalIDEALCHANGE7 .modal-header,
#myModalIDEALCHANGE8 .modal-header {
    text-align: center;
    background-color: #e8e9eb;
}
#myModalIDEALCHANGE1 .modal-body,
#myModalIDEALCHANGE2 .modal-body,
#myModalIDEALCHANGE3 .modal-body,
#myModalIDEALCHANGE4 .modal-body,
#myModalIDEALCHANGE5 .modal-body,
#myModalIDEALCHANGE6 .modal-body,
#myModalIDEALCHANGE7 .modal-body,
#myModalIDEALCHANGE8 .modal-body {		
	text-align:center !important;
	padding-top: 20px !important;	
	padding-right: 50px !important;	
	padding-left: 50px !important;
	padding-bottom: 50px !important;
}
.Header-loggedInBar {
    height: 1px;
    background-image: -webkit-radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
    background-image: radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
	background-color: #B7B7B7;
	width: 50%;
	margin-left:25%;
	margin-top: 0px;
	margin-bottom: 0px;
}
#myModalLabel {	
	margin: 1px !important;
}
.stripe-button-el span {
	display: none !important;
}
.stripe-button-el {
	display: none !important;
}
.center-block {
    float: none;
    margin-left: auto;
    margin-right: auto;
}

.input-group .icon-addon .form-control {
    border-radius: 0;
}

.icon-addon {
    position: relative;
    color: #555;
    display: block;
}

.icon-addon:after,
.icon-addon:before {
    display: table;
    content: " ";
}

.icon-addon:after {
    clear: both;
}

.icon-addon.addon-md .glyphicon,
.icon-addon .glyphicon, 
.icon-addon.addon-md .fa,
.icon-addon .fa {
    position: absolute;
    z-index: 2;
    left: 10px;
    font-size: 14px;
    width: 20px;
    margin-left: -2.5px;
    text-align: center;
    padding: 10px 0;
    top: 1px
}

.icon-addon.addon-lg .form-control {
    line-height: 1.33;
    height: 46px;
    font-size: 18px;
    padding: 10px 16px 10px 40px;
}

.icon-addon.addon-sm .form-control {
    height: 30px;
    padding: 5px 10px 5px 28px;
    font-size: 12px;
    line-height: 1.5;
}

.icon-addon.addon-lg .fa,
.icon-addon.addon-lg .glyphicon {
    font-size: 18px;
    margin-left: 0;
    left: 11px;
    top: 4px;
}

.icon-addon.addon-md .form-control,
.icon-addon .form-control {
    padding-left: 30px;
    float: left;
    font-weight: normal;
}

.icon-addon.addon-sm .fa,
.icon-addon.addon-sm .glyphicon {
    margin-left: 0;
    font-size: 12px;
    left: 5px;
    top: -1px
}

.icon-addon .form-control:focus + .glyphicon,
.icon-addon:hover .glyphicon,
.icon-addon .form-control:focus + .fa,
.icon-addon:hover .fa {
    color: #2580db;
}
.btn-info {
    position: relative;
    border-radius: 4px;
    background-color: #3ea8e5;
    background-image: -webkit-linear-gradient(top,#44b1e8,#3098de);
    background-image: linear-gradient(-180deg,#44b1e8,#3098de);
    box-shadow: 0 1px 0 0 rgba(46,86,153,.15),inset 0 1px 0 0 rgba(46,86,153,.1),inset 0 -1px 0 0 rgba(46,86,153,.4);
    font-size: 17px;
    line-height: 21px;
    height: 37px;
    font-weight: 700;
    text-shadow: 0 -1px 0 rgba(0,0,0,.12);
    color: #fff;
    cursor: pointer;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
.fa-credit-card-alt, .fa-cc-stripe, .fa-university {
	font-size:smaller !important;
}
</style>
<?php
//tussentijdse berekening opslaan
//alles ouder dan 0.5 dag weggooien
$olddate = date("Y-m-d H:i:s", mktime(date("H")-12, date("i"), date("s"), date("m"), date("d"), date("Y")));
mysqli_query($mysqli, "DELETE FROM change_ideal WHERE datetime<'$olddate'") or die(mysqli_error($mysqli));

if (!isset($oneoff)) $oneoff="0.00";
if (!isset($costs)) $costs="0.00";
if (!isset($costs_after)) $costs_after="0.00";
mysqli_query($mysqli, "INSERT INTO change_ideal (uid,oneoff,costs,costs_after,date,plan,period,rec,s) VALUES ('".$_SESSION['uidGO']."','$oneoff','$costs','$costs_after','$date','$act_plan','$act_period','$rec','$s')") or die(mysqli_error($mysqli));

if ($rec==1) $costs=$oneoff;
$amount_ct = round(100*$costs);
?>
<!--START STRIPE BUTTON IDEAL-->
<div class="modal fade" id="myModalIDEALCHANGE<?php echo $s; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 31px !important; display: none;">
						 <?php echo "<form id=\"dd_$s\" role=\"form\" method=\"post\" action=\"stripe-v2/ideal_change.php?p=".basename($_SERVER['PHP_SELF'])."&s=".base64_encode($s)."&amount=$amount_ct\">"; ?>
							<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close " data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel"><b>GoDashboard account </b></h4><span class="text-muted small"><?php echo $description; ?>
                                                <hr class="Header-loggedInBar">
                                                <?php echo "".$_SESSION["unameGO"].""; ?>
											</div>
											<div class="modal-body">
                                            	<fieldset>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<?php include("sel-bank.php"); ?>
                                                        <label for="account" class="fa fa-university" rel="tooltip" title="bank"></label>           
                                                        </div>
                                                    </div>
  													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Account number" id="accountnumber" name="accountnumber" type="text" value="<?php echo "$pre_account"; ?>" required>
                                                        <label for="accountnumber" class="glyphicon glyphicon-credit-card" rel="tooltip" title="accountnumber"></label>           
                                                        </div>
                                                    </div>                                                
                                                 </fieldset>
                                                 <button id="submit" name="submit" class="btn btn-block btn-info" type="submit">iDeal €<?php echo $costs; ?></button>
												</div>
                                         </div>
									</div>
								   </form>
								</div>
