<?php
include("config.php");			
$error=0;

if ((isset($_GET["uname"])) and (isset($_GET["change"]))) {
	$uname = base64_decode($_GET["uname"]);
	$change = $_GET["change"];
	$today = date("Y-m-d");
		
	$res = mysqli_query($mysqli,"SELECT * FROM users WHERE uname='$uname' and confirmed='1' and (endDate IS NULL or endDate>'$today') and changeU='$change'");  
	$numrows = mysqli_num_rows($res);
	if ($numrows == 0) {
		$error=1;
		$result='<br><div class="alert alert-danger">Password has already been reset. If you would like to reset again, <a href="forgot.php">click here</a></div>';
	}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoDashboard Client Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
.login-panel {
    margin-top: 10% !important;
}
.margintop {
    margin-top: 5% !important;
}
</style>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>
<?php
if (isset($_POST["submit"])) {
	$ww1 = $_POST["ww1"];
	$ww2 = $_POST["ww2"];
	$password =  md5($ww1);
	$today = date("Y-m-d");

	// Check if name has been entered
	if (!$_POST['ww1']) {
		$errWW1 = '<br>Please enter your password';
	}

	// Check if name has been entered
	if (!$_POST['ww2']) {
		$errWW2 = '<br>Please re-enter your password';
	}

	if ($_POST['ww1']!=$_POST['ww2']) {
		$errWW2 = '<br>Password don\'t match';
	}
		
	if (!$errWW1 && !$errWW2) {
	  mysqli_query($mysqli,"UPDATE users SET password='$password',ingelogd=ingelogd+1,lastLogin='".date("Y-m-d H:i:s")."',changeU='' WHERE uname='$uname' LIMIT 1");

	  $result = mysqli_query($mysqli,"SELECT uid, contact FROM users WHERE uname='$uname' and confirmed='1' and (endDate IS NULL or endDate>'$today')");  
	  $numrows = mysqli_num_rows($result);
	  if ($numrows == 0) {
		  $result='<br><div class="alert alert-danger">Log in credentials not found or not confirmed</div>';
	  }
	  else {
		  // Sessie registreren 
		  $gegevens = mysqli_fetch_array($result);
	  
		  $_SESSION['uidGO'] = $gegevens['uid']; 
		  $_SESSION['unameGO'] = $uname; 
		  $_SESSION['contactGO'] = $gegevens['contact']; 

		  // Redirecten naar hoofdmenu.php 
		  echo "<script>
		  window.location = 'index.php';
		  </script>";

		  exit;
	  	}	
	}
}
?>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 margintop" align="center">
              <img src="GoDashboardLogo220x50.png">            
            </div>
        </div>
       <div class="row">               
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">                 
                    <div class="panel-body">
                    <div><h3>Reset Password</h3></div>
                        <div style="width:100%"><?php echo $result; ?></div>
                        <?php if ($error==0) {  ?>                 
                        <form role="form" method="post" action="<?php echo "reset.php?uname=".base64_encode($uname)."&change=$change"; ?>">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" id="ww1" name="ww1" type="password" value="" required>
                                    <?php echo "<p class='text-danger'>$errWW1</p>";?>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Retype password" id="ww2" name="ww2" type="password" value="" required>
                                    <?php echo "<p class='text-danger'>$errWW2</p>";?>
                                </div>

                                <button id="submit" name="submit" class="btn btn-lg btn-success btn-block" type="submit">Reset</button>
                            </fieldset>
                        </form>
                        <?php } ?>
                    </div>
                </div>
              </div>
        </div>
    </div>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
   
</body>
</html>
<? } ?>