<?php
// Config includen om met de MySQL database verbinding te maken
include("config.php");			

if (!is_logged_in()) redirect();
else {
	$result = $_GET["result"];
	if (isset($result)) $result ="<br><div class=\"alert alert-warning\">$result</div>";

	$temp = $_GET["temp"];
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoDashboard</title>
<link rel="shortcut icon" href="favicon.ico" />

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>
  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
img {
	margin-left:5px;
	margin-top:1px;
    max-width: 500px;
    max-height: 250px;
}
.row .show-grid {
	margin: 0;
}
.hoverDiv:hover {background: #f5f5f5 !important;}
</style>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>

<body onfocus="focuspage(event)">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="GoDashboardLogo220x50.png">
            </div>
<?php include("topnav.php"); ?>
            <div class="navbar-default sidebar" role="navigation">
<?php include("sidenav.php"); ?>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">                                
            <div class="row">
                <div class="col-lg-12">
                                        <div style="width:100%">
						<?php
                            	echo "<div class=\"modal fade\" id=\"myModalREA\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"top: 31px !important; display: none;\">
						 <form role=\"form\" method=\"post\" action=\"$rea_page\">
							<div class=\"modal-dialog\">
										<div class=\"modal-content\">
											<div class=\"modal-header\">
												<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
												<h4 class=\"modal-title\" id=\"myModalLabel\">"; 
												echo "Are you sure you would like to re-activate your subscription?
												</div>
												<div class=\"modal-footer\">
												<button id=\"submit\" name=\"submit\" class=\"btn btn-primary\" type=\"submit\">Re-activate subscription</button>"; 
												echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button></div>";
										echo "</div>
									</div>
								   </form>
								</div>";
			?>			<?php echo $result; ?></div>                     
                <br>
                 <button type="button" class="btn btn-default" onclick="goBack()"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Go Back</button><br><br>   
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->           
            <div class="row">
                <div class="col-lg-12">
                  <div class="table-responsive">
					<?php
					//check if all services that are needed are activated
					//e-commerce template: trustpilot, ccv shop
					if ($temp==1) {
						$query = mysqli_query($mysqli, "select * from ccvshop where uid='".$_SESSION["uidGO"]."' and apiKey!=''") or die(mysqli_error($mysqli));
						$numrows = mysqli_num_rows($query);
						
						if ($numrows==1) $ccvshop=1;
						else $ccvshop=0;
						
						$query = mysqli_query($mysqli, "select * from trustpilot where uid='".$_SESSION["uidGO"]."' and accesstoken!=''") or die(mysqli_error($mysqli));
						$numrows = mysqli_num_rows($query);
						
						if ($numrows==1) $trustpilot=1;
						else $trustpilot=0;
						
						if (($ccvshop==0) || ($trustpilot==0)) echo "<div class=\"alert alert-warning\">To install this template you must activate the services below. After this has been completed the install button will be activated.</div>";
						
						$name="E-commerce template";
					}
                    
  					?>
                     <table class="table table-bordered">
                       <tbody>
				<?php 
					$login = "editor.php";
					echo "<tr><td colspan=\"2\" align=\"center\"><div><h4><b>$name</b></h4></div><br><img src=\"../integrations/images/TEMPLATES/e-commerce.fw.png\" width=\"352\" height=\"176\"><br><br>";
					if ($temp==1) {
						if (($ccvshop==1) and ($trustpilot==1)) echo "<button type=\"button\" class=\"btn btn-warning btn-lg\">Install</button>";
						else echo "<button type=\"button\" class=\"btn btn-warning btn-lg disabled\">Install</button>";
					}
					echo "<br></td></tr>";

if (($temp==1) and ($ccvshop==1)) {
	echo "<tr><td width=\"50%\"><div><h4><b>CCV Shop</b></h4> <img src=\"../integrations/images/ccvshop.png\" width=\"44\" height=\"44\"><br></div></td><td align=\"center\"><button class=\"btn btn-success btn-lg\" onclick=\"myFunctionCCV();\">Activate Service</button></td></tr>";
}
if (($temp==1) and ($trustpilot==0)) {
	echo "<tr><td width=\"50%\"><div><h4><b>Trustpilot</b></h4> <img src=\"../integrations/images/trustpilot.fw.png\" width=\"44\" height=\"44\"><br></div></td><td align=\"center\"><button class=\"btn btn-success btn-lg\" onclick=\"myFunctionTrustpilot();\">Activate Service</button></td></tr>";
	
}
                    ?>
                    	</tbody>
                     </table> 
                     <br><br> 
              	</div>
        	</div>
        </div>
        				
					
				</div>								
					
				</div>
			<!-- einde pricing -->            

            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script>
    function goBack() {
        window.history.back();
    }
	function focuspage(event) {
		window.location.reload();
	}
	function myFunctionCCV() {
		var myWindow = window.open("https://godashboard.nl/go/ccvshop/get_host.php?<?php echo "uid=".$_SESSION["uidGO"].""; ?>", "ccvshop", "top=50,left=400,width=500,height=600").focus;
	}
	function myFunctionTrustpilot() {
		var myWindow = window.open("https://godashboard.nl/go/trustpilot/?<?php echo "uid=".$_SESSION["uidGO"].""; ?>", "trustpilot", "top=50,left=400,width=500,height=600").focus;
	}
    </script> 
</body>

</html>
<?php } ?>