<style>
#myModalRENEW .modal-dialog {
    width: 350px;
}
#myModalRENEW .modal-header {
    text-align: center;
    background-color: #e8e9eb;
}
#myModalRENEW .modal-body {		
	text-align:center !important;
	padding-top: 20px !important;	
	padding-right: 50px !important;	
	padding-left: 50px !important;
	padding-bottom: 50px !important;
}
.Header-loggedInBar {
    height: 1px;
    background-image: -webkit-radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
    background-image: radial-gradient(circle,hsla(0,0%,100%,.31),hsla(0,0%,100%,0));
	background-color: #B7B7B7;
	width: 50%;
	margin-left:25%;
	margin-top: 0px;
	margin-bottom: 0px;
}
#myModalLabel {	
	margin: 1px !important;
}
.stripe-button-el span {
	display: none !important;
}
.stripe-button-el {
	display: none !important;
}
.center-block {
    float: none;
    margin-left: auto;
    margin-right: auto;
}

.input-group .icon-addon .form-control {
    border-radius: 0;
}

.icon-addon {
    position: relative;
    color: #555;
    display: block;
}

.icon-addon:after,
.icon-addon:before {
    display: table;
    content: " ";
}

.icon-addon:after {
    clear: both;
}

.icon-addon.addon-md .glyphicon,
.icon-addon .glyphicon, 
.icon-addon.addon-md .fa,
.icon-addon .fa {
    position: absolute;
    z-index: 2;
    left: 10px;
    font-size: 14px;
    width: 20px;
    margin-left: -2.5px;
    text-align: center;
    padding: 10px 0;
    top: 1px
}

.icon-addon.addon-lg .form-control {
    line-height: 1.33;
    height: 46px;
    font-size: 18px;
    padding: 10px 16px 10px 40px;
}

.icon-addon.addon-sm .form-control {
    height: 30px;
    padding: 5px 10px 5px 28px;
    font-size: 12px;
    line-height: 1.5;
}

.icon-addon.addon-lg .fa,
.icon-addon.addon-lg .glyphicon {
    font-size: 18px;
    margin-left: 0;
    left: 11px;
    top: 4px;
}

.icon-addon.addon-md .form-control,
.icon-addon .form-control {
    padding-left: 30px;
    float: left;
    font-weight: normal;
}

.icon-addon.addon-sm .fa,
.icon-addon.addon-sm .glyphicon {
    margin-left: 0;
    font-size: 12px;
    left: 5px;
    top: -1px
}

.icon-addon .form-control:focus + .glyphicon,
.icon-addon:hover .glyphicon,
.icon-addon .form-control:focus + .fa,
.icon-addon:hover .fa {
    color: #2580db;
}
.btn-info {
    position: relative;
    border-radius: 4px;
    background-color: #3ea8e5;
    background-image: -webkit-linear-gradient(top,#44b1e8,#3098de);
    background-image: linear-gradient(-180deg,#44b1e8,#3098de);
    box-shadow: 0 1px 0 0 rgba(46,86,153,.15),inset 0 1px 0 0 rgba(46,86,153,.1),inset 0 -1px 0 0 rgba(46,86,153,.4);
    font-size: 17px;
    line-height: 21px;
    height: 37px;
    font-weight: 700;
    text-shadow: 0 -1px 0 rgba(0,0,0,.12);
    color: #fff;
    cursor: pointer;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
.fa-credit-card-alt, .fa-cc-stripe, .fa-university {
	font-size:smaller !important;
}
</style>
<?php 
$query = mysqli_query($mysqli, "select account, registration, endDate, reactivated from users where uid='".$_SESSION["uidGO"]."'") or die(mysqli_error($mysqli));
$row = mysqli_fetch_array($query);
$account = $row["account"];
$endDate = $row["endDate"];
$reactivated = $row["reactivated"];

if ($account=="FREE") { 
	$registration = strtotime($row["registration"]);
	$now = time(); // or your date as well
	$datediff = $now - $registration;
	$exp = 14 - round($datediff / (60 * 60 * 24));
?>            
    <a href="account-v2.php#pricing"><span class="text-muted small">Trial account, <?php echo "$exp"; ?> days left</span></a>
<?php
}                           
elseif ($endDate!="") { 
	$registration = strtotime($row["endDate"]);
	$now = time(); // or your date as well
	$datediff = $registration - $now;
	$exp = round($datediff / (60 * 60 * 24));
	$rea_page = "plans/reactivate.php?p=".basename($_SERVER['PHP_SELF'])."";	 
?>
    <a href="account-v2.php#pricing"><span class="text-muted small">You're subscription ended <?php echo "$exp"; ?> days left</span></a>
    <a href="#" data-toggle="modal" data-target="#myModalREA"><strong>RE-ACTIVATE ACCOUNT</strong></a>
<?php
}
else {
	$pquery = mysqli_query($mysqli, "select * from plans where uid='".$_SESSION["uidGO"]."' order by id DESC LIMIT 1") or die(mysqli_error($mysqli));
	$prow = mysqli_fetch_array($pquery);
	$pplan = $prow["plan"];
	$pperiod = $prow["period"];	 
	if ($pperiod=="year") $pperiod = "annual";	 
	elseif ($pperiod=="month") $pperiod = "monthly";	 
?>
    <a href="account-v2.php#pricing"><span class="text-muted small"><?php echo "$pplan"; ?> <?php echo "$pperiod subscription"; ?></span></a>
    <?php
    	$pquery = mysqli_query($mysqli, "select t2.id, t2.period, t2.startDate, t2.amount from ideal_subscriptions as t1, plans as t2 where t1.user='".$_SESSION["uidGO"]."' and t1.user=t2.uid and t2.active='1' and t2.endDate IS NULL and t2.period!='One-off' and t2.period!='Set-up' LIMIT 1") or die(mysqli_error($mysqli));
		$count = mysqli_num_rows($pquery);
		//check of er een ideal betaling moet worden gedaan
		if ($count==1) {
			$prow = mysqli_fetch_array($pquery);
			$planid = $prow["id"];	 
			$pperiod = $prow["period"];	 
			$date = $prow["startDate"];
			$array = explode("-", $date);
			$year = $array[0];
			$month = $array[1];
			$day = $array[2];
			$threeweeks = date("Y-m-d", mktime(date("H"), date("i"), date("s"), $month, $day-21, $year));
			$d1 = new DateTime($threeweeks);
			$oneweek = date("Y-m-d", mktime(date("H"), date("i"), date("s"), $month, $day-7, $year));
			$d2 = new DateTime($oneweek);			
			$amount_ct = round(100*$prow["amount"]);
			$today = date("Y-m-d");
			$d3 = new DateTime($today);			

			if ($pperiod=="year") {
				$diff = $d1->diff($d3,true);
				$years = floor($diff->y); //=aantal jaren dat er een renewed betaling moet zijn
				if ($years!=0) {
					$pq = mysqli_query($mysqli, "select * from renew_payment_ideal where uid='".$_SESSION["uidGO"]."' and planid='$planid' and payed='1'") or die(mysqli_error($mysqli));
					$count = mysqli_num_rows($pq);
					if ($count<$years) {
    					echo "<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModalRENEW\" class=\"btn btn-warning btn-xs\"><i class=\"fa fa-repeat\" aria-hidden=\"true\"></i> RENEW!</a>";
						?>
<!--START STRIPE BUTTON IDEAL-->
<div class="modal fade" id="myModalRENEW" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 31px !important; display: none;">
						 <?php echo "<form role=\"form\" id=\"renew\" method=\"post\" action=\"stripe-v2/ideal_renew.php?p=".basename($_SERVER['PHP_SELF'])."&s=".base64_encode("10")."&planid=$planid&amount=$amount_ct\">"; ?>
							<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close " data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel"><b>GoDashboard account </b></h4><span class="text-muted small"><?php echo $description; ?>
                                                <hr class="Header-loggedInBar">
                                                <?php echo "".$_SESSION["unameGO"].""; ?>
											</div>
											<div class="modal-body">
                                            	<fieldset>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<?php include("sel-bank.php"); ?>
                                                        <label for="account" class="fa fa-university" rel="tooltip" title="bank"></label>           
                                                        </div>
                                                    </div>
  													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Account number" id="accountnumber" name="accountnumber" type="text" required>
                                                        <label for="accountnumber" class="glyphicon glyphicon-credit-card" rel="tooltip" title="accountnumber"></label>           
                                                        </div>
                                                    </div>                                                
                                                 </fieldset>
                                                 <button id="submit" name="submit" class="btn btn-block btn-info" type="submit">iDeal €<?php echo round($amount_ct/100,2); ?></button>
												</div>
                                         </div>
									</div>
								   </form>
								</div>
                                <?php
					}
				}
			}
			elseif ($pperiod=="month") {
				$diff = $d2->diff($d3,true);
				$months = floor($diff->m); //=aantal maanden dat er een renewed betaling moet zijn
				if ($months!=0) {
					$pq = mysqli_query($mysqli, "select * from renew_payment_ideal where uid='".$_SESSION["uidGO"]."' and planid='$planid' and payed='1'") or die(mysqli_error($mysqli));
					$count = mysqli_num_rows($pq);
					if ($count<$months) {
    					echo "<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModalRENEW\" class=\"btn btn-warning btn-xs\"><i class=\"fa fa-repeat\" aria-hidden=\"true\"></i> RENEW!</a>";
						?>
<!--START STRIPE BUTTON IDEAL-->
<div class="modal fade" id="myModalRENEW" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 31px !important; display: none;">
						 <?php echo "<form role=\"form\" id=\"renew\" method=\"post\" action=\"stripe-v2/ideal_renew.php?p=".basename($_SERVER['PHP_SELF'])."&s=".base64_encode("10")."&planid=$planid&amount=$amount_ct\">"; ?>
							<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close " data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel"><b>GoDashboard account </b></h4><span class="text-muted small"><?php echo $description; ?>
                                                <hr class="Header-loggedInBar">
                                                <?php echo "".$_SESSION["unameGO"].""; ?>
											</div>
											<div class="modal-body">
                                            	<fieldset>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<?php include("sel-bank.php"); ?>
                                                        <label for="account" class="fa fa-university" rel="tooltip" title="bank"></label>           
                                                        </div>
                                                    </div>
  													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Account number" id="accountnumber" name="accountnumber" type="text" required>
                                                        <label for="accountnumber" class="glyphicon glyphicon-credit-card" rel="tooltip" title="accountnumber"></label>           
                                                        </div>
                                                    </div>                                                
                                                 </fieldset>
                                                 <button id="submit" name="submit" class="btn btn-block btn-info" type="submit">iDeal €<?php echo round($amount_ct/100,2); ?></button>
												</div>
                                         </div>
									</div>
								   </form>
								</div>
                                <?php
					}
				}
			}
		}
}
?>