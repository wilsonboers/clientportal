<?php
include("config.php");			
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoDashboard Client Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
.login-panel {
    margin-top: 10% !important;
}
.margintop {
    margin-top: 5% !important;
}
</style>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>
<?php
if (isset($_POST["submit"])) {
	$uname = $_POST["name"];

	// Check if name has been entered
	if (!$_POST['name']) {
		$errName = '<br>Please enter your username / e-mail';
	}

	if (!$errName) {
		  // Het wachtwoord wordt hier gecontroleerd		
		 $result = mysqli_query($mysqli,"SELECT uid, contact FROM users WHERE uname='$uname' and confirmed='1' and (endDate IS NULL or endDate>'$today')");  
		  $numrows = mysqli_num_rows($result);
		  if ($numrows == 0) {
			  $result='<br><div class="alert alert-danger">Username / e-mail not found or not confirmed</div>';
		  }
		  else {
				$url = "https://godashboard.nl/go/integrations/forgot.php";
				$params = "uname=$uname&change=1";
				$response = post_without_wait($url, $params);

			  	$result='<br><div class="alert alert-success">An e-mail with instructions to reset your password has been sent!</div>';	
		  }	
	}
}
?>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 margintop" align="center">
              <img src="GoDashboardLogo220x50.png">            
            </div>
        </div>
       <div class="row">               
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">                 
                    <div class="panel-body">
                    <div><p>Please fill your username / e-mail we will send you an e-mail with further instructions.</p></div>
                        <div style="width:100%"><?php echo $result; ?></div>                        
                        <form role="form" method="post" action="forgot.php">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username / e-mail" id="name" name="name" type="text" value="<?php if (isset($_POST['name'])) echo $_POST['name']; ?>" autofocus required>
                                     <?php echo "<p class='text-danger'>$errName</p>";?>
                                </div>
                                <button id="submit" name="submit" class="btn btn-lg btn-success btn-block" type="submit">Send</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
              </div>
        </div>
    </div>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    
</body>

</html>
