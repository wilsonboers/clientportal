<?php 
	$rec = "0";					

	if ($plan=="STARTER") $numplan = 1;
	elseif ($plan=="VALUE") $numplan = 2;
	elseif ($plan=="PRO") $numplan = 3;
	elseif ($plan=="PREMIUM") $numplan = 4;
	if ($act_plan=="STARTER") $act_numplan = 1;
	elseif ($act_plan=="VALUE") $act_numplan = 2;
	elseif ($act_plan=="PRO") $act_numplan = 3;
	elseif ($act_plan=="PREMIUM") $act_numplan = 4;

	//s bepalen
	if ($act_period=="month") {
		if ($act_plan=="STARTER") {
			$s=1;
		}
		elseif ($act_plan=="VALUE") {
			$s=2;
		}
		elseif ($act_plan=="PRO") {
			$s=3;
		}
		elseif ($act_plan=="PREMIUM") {
			$s=4;
		}
	}
	elseif ($act_period=="year") {
		if ($act_plan=="STARTER") {
			$s=5;
		}
		elseif ($act_plan=="VALUE") {
			$s=6;
		}
		elseif ($act_plan=="PRO") {
			$s=7;
		}
		elseif ($act_plan=="PREMIUM") {
			$s=8;
		}
	}
	
	if ($plan==NULL) {
		//show regular payment options depending on act_plan and act_period
		$btw = 1.21;
		if ($priceNew!=0){
			$costs_setup = round($priceNew*$btw,2);
		 	$costs_setup_ct = round(100*$costs_setup,0); //kosten in centen
		}
		else {
			$costs_setup = 0;
			$costs_setup_ct = 0;
		}
		
		if ($act_period=="month") {
			if ($act_plan=="STARTER") {
				$costswithVAT = $costs_starter_month;
				//$costs_setup = $costs1_setup;
				$less = 6;
				$costs = round((100*$costs_starter_month)+$costs_setup_ct,0); //kosten in centen
				$s=1;
			}
			elseif ($act_plan=="VALUE") {
				$costswithVAT = $costs_value_month;
				//$costs_setup = $costs2_setup;
				$less = 6;
				$costs = round((100*$costs_value_month)+$costs_setup_ct,0);
				$s=2;
			}
			elseif ($act_plan=="PRO") {
				$costswithVAT = $costs_pro_month;
				//$costs_setup = $costs3_setup;
				$less = 10;
				$costs = round((100*$costs_pro_month)+$costs_setup_ct,0);
				$s=3;
			}
			elseif ($act_plan=="PREMIUM") {
				$costswithVAT = $costs_premium_month;
				//$costs_setup = $costs4_setup;
				$less = 10;
				$costs = round((100*$costs_premium_month)+$costs_setup_ct,0);
				$s=4;
			}
			$description="Monthly subscription";
			
			echo "Set-up: €$costs_setup incl. VAT<br>Monthly: €$costswithVAT incl. VAT<br><br>Please select a payment method to proceed. If you received a coupon, please make sure you have saved it with your account settings above!<br><br>";	
			include("plans/payment-v2.php");
			echo "<br><hr><small><em>Pay annually and save €$less /mo!</em></small>";
	
		}
		elseif ($act_period=="year") {
			if ($act_plan=="STARTER") {
				$costswithVAT = $costs_starter_year;
				//$costs_setup = $costs5_setup;
				$costs = round((100*$costs_starter_year)+$costs_setup_ct,0);
				$s=5;
			}
			elseif ($act_plan=="VALUE") {
				$costswithVAT = $costs_value_year;
				//$costs_setup = $costs6_setup;
				$costs = round((100*$costs_value_year)+$costs_setup_ct,0);
				$s=6;
			}
			elseif ($act_plan=="PRO") {
				$costswithVAT = $costs_pro_year;
				//$costs_setup = $costs7_setup;
				$costs = round((100*$costs_pro_year)+$costs_setup_ct,0);
				$s=7;
			}
			elseif ($act_plan=="PREMIUM") {
				$costswithVAT = $costs_premium_year;
				//$costs_setup = $costs8_setup;
				$costs = round((100*$costs_premium_year)+$costs_setup_ct,0);
				$s=8;
			}
			$description="Yearly subscription";
			
			echo "Set-up: €$costs_setup incl. VAT<br>Annually: €$costswithVAT incl. VAT<br><br>Please select a payment method to proceed. If you received a coupon, please make sure you have saved it with your account settings above!<br><br>";
			include("plans/payment-v2.php");		
		}
	}
	else {
		if (($plan==$act_plan) && ($period=="month") && ($act_period=="year")) {	
			$d1 = new DateTime("$currYear-$startMonth-$startDay");
			$d2 = new DateTime("$today");
			$interval = date_diff($d1, $d2,true);
			$months_payed = ($interval->format("%m") + ($interval->format("%y") * 12)) + 1;
			
			if ($act_plan=="STARTER") {
				$costs = round($costs_starter_year - ($months_payed * $costs_starter_month),2);
				$costs_after = $costs_starter_year;
			}
			elseif ($act_plan=="VALUE") {
				$costs = round($costs_value_year - ($months_payed * $costs_value_month),2);
				$costs_after = $costs_value_year;
			}
			elseif ($act_plan=="PRO") {
				$costs = round($costs_pro_year - ($months_payed * $costs_pro_month),2);
				$costs_after = $costs_pro_year;
			}
			elseif ($act_plan=="PREMIUM") {
				$costs = round($costs_premium_year - ($months_payed * $costs_premium_month),2);
				$costs_after = $costs_premium_year;
			}
			
			$date = "$startDay-$startMonth-$nextYear";
			$rec = 0;
			
			//id = 1
			$message1 = "One-off payment of € $costs and from $date the costs will be € $costs_after /yr. All costs are included with VAT.";
			if ($account=="ideal") {
				$message2 = "&nbsp;To proceed please confirm and continue with iDeal payment.<br><br><button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#myModalIDEALCHANGE".$s."\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\">Cancel</button>";					
			}
			else {
				$message2 = "&nbsp;Please confirm if you'd like to proceed?<br><br><button type=\"button\" class=\"btn btn-success\" id=\"confirm1\" data-toggle=\"modal\" data-target=\"#myModal\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\" id=\"cancel1\">Cancel</button><input id=\"costs1\" value=\"$costs\" hidden><input id=\"costs_after1\" value=\"$costs_after\" hidden><input id=\"date1\" value=\"$date\" hidden><input id=\"plan1\" value=\"$act_plan\" hidden><input id=\"period1\" value=\"$act_period\" hidden><input id=\"rec1\" value=\"0\" hidden>";	//rec is 0 = one-of payment, 1 = monthly
			}
		}
		elseif (($plan==$act_plan) && ($period=="year") && ($act_period=="month")) {		
			if ($act_plan=="STARTER") $costs_after = $costs_starter_month;
			elseif ($act_plan=="VALUE") $costs_after = $costs_value_month;
			elseif ($act_plan=="PRO") $costs_after = $costs_pro_month;
			elseif ($act_plan=="PREMIUM") $costs_after = $costs_premium_month;
	
			$date = "$startDay-$startMonth-$nextYear";
			$rec = 0;
			
			//id = 2
			$message1 = "Beginning from $date the costs will be € $costs_after /mo. All costs are included with VAT.";
			//er hoeft geen ideal betaling te gebeuren dus alleen confirmen, geen one-off costs
			$message2 = "&nbsp;Please confirm if you'd like to proceed?<br><br><button type=\"button\" class=\"btn btn-success\" id=\"confirm2\" data-toggle=\"modal\" data-target=\"#myModal\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\" id=\"cancel2\">Cancel</button><input id=\"costs2\" value=\"$costs\" hidden><input id=\"costs_after2\" value=\"$costs_after\" hidden><input id=\"date2\" value=\"$date\" hidden><input id=\"plan2\" value=\"$act_plan\" hidden><input id=\"period2\" value=\"$act_period\" hidden><input id=\"rec2\" value=\"0\" hidden>";	//rec is 0 = one-of payment, 1 = monthly
		}
		elseif (($period==$act_period) && ($numplan<$act_numplan)) { //upgrade
			if ($period=="month") {
				$d1 = new DateTime("$lastMonthYear-$lastMonth-$startDay");
				$d2 = new DateTime("$today");
				$interval = date_diff($d1, $d2,true);
				$days_left = 30-($interval->format("%a"));
				
				if ($plan=="STARTER") {
					$costs_paid = round(($days_left / 30) * $costs_starter_month,2);
					$costs_paid = $costs_paid+$costs1_setup;//changed
				}
				elseif ($plan=="VALUE") {
					$costs_paid = round(($days_left / 30) * $costs_value_month,2);
					$costs_paid = $costs_paid+$costs2_setup;//changed
				}
				elseif ($plan=="PRO") {
					$costs_paid = round(($days_left / 30) * $costs_pro_month,2);
					$costs_paid = $costs_paid+$costs3_setup;//changed
				}
				
				
				if ($act_plan=="VALUE") {
					$costs = round(($days_left / 30) * $costs_value_month,2)-$costs_paid;
					$costs = $costs+$costs2_setup;//changed				
					$costs_after = $costs_value_month;
				}
				elseif ($act_plan=="PRO") {
					$costs = round(($days_left / 30) * $costs_pro_month,2)-$costs_paid;
					$costs = $costs+$costs3_setup;//changed				
					$costs_after = $costs_pro_month;
				}
				elseif ($act_plan=="PREMIUM") {
					$costs = round(($days_left / 30) * $costs_premium_month,2)-$costs_paid;
					$costs = $costs+$costs4_setup;//changed				
					$costs_after = $costs_premium_month;
				}
	
				$date = "$startDay-$nextMonth-$nextYearMonth";
				$rec = 0;
				
				//id = 3
				$message1 = "One-off payment of € $costs and from $date the costs will be € $costs_after /mo. All costs are included with VAT.";
				if ($account=="ideal") {
					$message2 = "&nbsp;To proceed please confirm and continue with iDeal payment.<br><br><button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#myModalIDEALCHANGE".$s."\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\">Cancel</button>";					
				}
				else {
					$message2 = "&nbsp;Please confirm if you'd like to proceed?<br><br><button type=\"button\" class=\"btn btn-success\" id=\"confirm3\" data-toggle=\"modal\" data-target=\"#myModal\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\" id=\"cancel3\">Cancel</button><input id=\"costs3\" value=\"$costs\" hidden><input id=\"costs_after3\" value=\"$costs_after\" hidden><input id=\"date3\" value=\"$date\" hidden><input id=\"plan3\" value=\"$act_plan\" hidden><input id=\"period3\" value=\"$act_period\" hidden><input id=\"rec3\" value=\"0\" hidden>";	//rec is 0 = one-of payment, 1 = monthly	
				}
			}
			elseif ($period=="year") {
				$d1 = new DateTime("$currYear-$startMonth-$startDay");
				$d2 = new DateTime("$today");
				$interval = date_diff($d1, $d2,true);
				
				$days_payed = ($interval->format("%a"))/365*12;
	
				if (($act_plan=="VALUE") && ($plan=="STARTER")) {
					$costs = round($costs_value_year-$costs_starter_year-($days_payed*$costs_starter_year/12),2);
					$costs = $costs-$costs5_setup+$costs6_setup;//changed

					$costs_after = $costs_value_year;
				}
				elseif (($act_plan=="PRO") && ($plan=="STARTER")) {
					$costs = round($costs_pro_year-$costs_starter_year-($days_payed*$costs_starter_year/12),2);
					$costs = $costs-$costs5_setup+$costs7_setup;//changed
					$costs_after = $costs_pro_year;
				}
				elseif (($act_plan=="PREMIUM") && ($plan=="STARTER")) {
					$costs = round($costs_premium_year-$costs_starter_year-($days_payed*$costs_starter_year/12),2);
					$costs = $costs-$costs5_setup+$costs8_setup;//changed
					$costs_after = $costs_premium_year;
				}
				elseif (($act_plan=="PRO") && ($plan=="VALUE")) {
					$costs = round($costs_pro_year-$costs_value_year-($days_payed*$costs_starter_year/12),2);
					$costs = $costs-$costs6_setup+$costs7_setup;//changed
					$costs_after = $costs_pro_year;
				}
				elseif (($act_plan=="PREMIUM") && ($plan=="VALUE")) {
					$costs = round($costs_premium_year-$costs_value_year-($days_payed*$costs_starter_year/12),2);
					$costs = $costs-$costs6_setup+$costs8_setup;//changed
					$costs_after = $costs_premium_year;
				}
				elseif (($act_plan=="PREMIUM") && ($plan=="PRO")) {
					$costs = round($costs_premium_year-$costs_pro_year-($days_payed*$costs_starter_year/12),2);
					$costs = $costs-$costs7_setup+$costs8_setup;//changed
					$costs_after = $costs_premium_year;
				}
	
				$date = "$startDay-$startMonth-$nextYear";
				$rec = 0;
		
				//id = 4
				$message1 = "One-off payment of € $costs and from $date the costs will be € $costs_after /yr.  All costs are included with VAT.";
				if ($account=="ideal") {
					$message2 = "&nbsp;To proceed please confirm and continue with iDeal payment.<br><br><button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#myModalIDEALCHANGE".$s."\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\">Cancel</button>";					
				}
				else {
					$message2 = "&nbsp;Please confirm if you'd like to proceed?<br><br><button type=\"button\" class=\"btn btn-success\" id=\"confirm4\" data-toggle=\"modal\" data-target=\"#myModal\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\" id=\"cancel4\">Cancel</button><input id=\"costs4\" value=\"$costs\" hidden><input id=\"costs_after4\" value=\"$costs_after\" hidden><input id=\"date4\" value=\"$date\" hidden><input id=\"plan4\" value=\"$act_plan\" hidden><input id=\"period4\" value=\"$act_period\" hidden><input id=\"rec4\" value=\"0\" hidden>";	//rec is 0 = one-of payment, 1 = monthly	
				}
			}
		}
		elseif (($period==$act_period) && ($numplan>$act_numplan)) { //downgrade
			if ($period=="month") {
				if ($act_plan=="STARTER") $costs_after = $costs_starter_month;
				elseif ($act_plan=="VALUE") $costs_after = $costs_value_month;
				elseif ($act_plan=="PRO") $costs_after = $costs_pro_month;
				elseif ($act_plan=="PREMIUM") $costs_after = $costs_premium_month;
	
				$date = "$startDay-$nextMonth-$nextYearMonth";
				$rec = 0;
				
				//id = 5
				$message1 = "Beginning from $date the costs will be € $costs_after /mo. All costs are included with VAT.";
				//er hoeft geen ideal betaling te gebeuren dus alleen confirmen, geen one-off costs
				$message2 = "&nbsp;Please confirm if you'd like to proceed?<br><br><button type=\"button\" class=\"btn btn-success\" id=\"confirm5\" data-toggle=\"modal\" data-target=\"#myModal\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\" id=\"cancel5\">Cancel</button><input id=\"costs5\" value=\"$costs\" hidden><input id=\"costs_after5\" value=\"$costs_after\" hidden><input id=\"date5\" value=\"$date\" hidden><input id=\"plan5\" value=\"$act_plan\" hidden><input id=\"period5\" value=\"$act_period\" hidden><input id=\"rec5\" value=\"0\" hidden>";	//rec is 0 = one-of payment, 1 = monthly			
			}
			elseif ($period=="year") {
				if ($act_plan=="STARTER") $costs_after = $costs_starter_year;
				elseif ($act_plan=="VALUE") $costs_after = $costs_value_year;
				elseif ($act_plan=="PRO") $costs_after = $costs_pro_year;
				elseif ($act_plan=="PREMIUM") $costs_after = $costs_premium_year;
	
				$date = "$startDay-$startMonth-$nextYear";
				$rec = 0;
				
				//id = 6
				$message1 = "Beginning from $date the costs will be € $costs_after /yr. All costs are included with VAT.";
				//er hoeft geen ideal betaling te gebeuren dus alleen confirmen, geen one-off costs
				$message2 = "&nbsp;Please confirm if you'd like to proceed?<br><br><button type=\"button\" class=\"btn btn-success\" id=\"confirm6\" data-toggle=\"modal\" data-target=\"#myModal\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\" id=\"cancel6\">Cancel</button><input id=\"costs6\" value=\"$costs\" hidden><input id=\"costs_after6\" value=\"$costs_after\" hidden><input id=\"date6\" value=\"$date\" hidden><input id=\"plan6\" value=\"$act_plan\" hidden><input id=\"period6\" value=\"$act_period\" hidden><input id=\"rec6\" value=\"0\" hidden>";	//rec is 0 = one-of payment, 1 = monthly	
			}
		}
		elseif (($period=="year") && ($act_period=="month") && ($numplan<$act_numplan)) { //upgrade en mnd ipv jaarlijks
			$d1 = new DateTime("$currYear-$startMonth-$startDay");
			$d2 = new DateTime("$today");
			$interval = date_diff($d1, $d2,true);
			$months_payed = ($interval->format("%m") + ($interval->format("%y") * 12)) + 1;
			
			if (($act_plan=="VALUE") && ($plan=="STARTER")) {
				$costs = round(((12 * $costs_value_month) - $costs_starter_year) / 12,2);
				$costs = $costs;//changed
				$costs_after = $costs_value_month;
				$oneoff = $costs2_setup-$costs5_setup;
			}
			elseif (($act_plan=="PRO") && ($plan=="STARTER")) {
				$costs = round(((12 * $costs_pro_month) - $costs_starter_year) / 12,2);
				$costs = $costs;//changed
				$costs_after = $costs_pro_month;
				$oneoff = $costs3_setup-$costs5_setup;
			}
			elseif (($act_plan=="PREMIUM") && ($plan=="STARTER")) {
				$costs = round(((12 * $costs_premium_month) - $costs_starter_year) / 12,2);
				$costs = $costs;//changed
				$costs_after = $costs_premium_month;
				$oneoff = $costs4_setup-$costs5_setup;
			}
			elseif (($act_plan=="PRO") && ($plan=="VALUE")) {
				$costs = round(((12 * $costs_pro_month) - $costs_value_year) / 12,2);
				$costs = $costs;//changed
				$costs_after = $costs_pro_month;
				$oneoff = $costs3_setup-$costs6_setup;
			}
			elseif (($act_plan=="PREMIUM") && ($plan=="VALUE")) {
				$costs = round(((12 * $costs_premium_month) - $costs_value_year) / 12,2);
				$costs = $costs;//changed
				$costs_after = $costs_premium_month;
				$oneoff = $costs4_setup-$costs6_setup;
			}
			elseif (($act_plan=="PREMIUM") && ($plan=="PRO")) {
				$costs = round(((12 * $costs_premium_month) - $costs_pro_year) / 12,2);
				$costs = $costs;//changed
				$costs_after = $costs_premium_month;
				$oneoff = $costs4_setup-$costs7_setup;
			}
	
			$date = "$startDay-$startMonth-$nextYear";
			$rec = 1;
			
			//id = 7
			$message1 = "One-off payment of € $oneoff and monthly costs will be € $costs /mo untill $date. Beginning from $startDay-$startMonth-$nextYear the costs will be € $costs_after /mo. All costs are included with VAT.";
			if ($account=="ideal") {
				$message2 = "&nbsp;To proceed please confirm and continue with iDeal payment.<br><br><button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#myModalIDEALCHANGE".$s."\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\">Cancel</button>";					
			}
			else {
				$message2 = "&nbsp;Please confirm if you'd like to proceed?<br><br><button type=\"button\" class=\"btn btn-success\" id=\"confirm7\" data-toggle=\"modal\" data-target=\"#myModal\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\" id=\"cancel7\">Cancel</button><input id=\"oneoff7\" value=\"$oneoff\" hidden><input id=\"costs7\" value=\"$costs\" hidden><input id=\"costs_after7\" value=\"$costs_after\" hidden><input id=\"date7\" value=\"$date\" hidden><input id=\"plan7\" value=\"$act_plan\" hidden><input id=\"period7\" value=\"$act_period\" hidden><input id=\"rec7\" value=\"1\" hidden>";	//rec is 0 = one-of payment, 1 = monthly
			}
		}
		elseif (($period=="year") && ($act_period=="month") && ($numplan>$act_numplan)) { //downgrade en mnd ipv jaarlijks
			if ($act_plan=="STARTER") $costs_after = $costs_starter_month;
			elseif ($act_plan=="VALUE") $costs_after = $costs_value_month;
			elseif ($act_plan=="PRO") $costs_after = $costs_pro_month;
			elseif ($act_plan=="PREMIUM") $costs_after = $costs_premium_month;
			
			$date = "$startDay-$startMonth-$nextYear";
			$rec = 0;
	
			//id = 8
			$message1 = "Beginning from $date the costs will be € $costs_after /mo. All costs are included with VAT.";
			//er hoeft geen ideal betaling te gebeuren dus alleen confirmen, geen one-off costs
			$message2 = "&nbsp;Please confirm if you'd like to proceed?<br><br><button type=\"button\" class=\"btn btn-success\" id=\"confirm8\" data-toggle=\"modal\" data-target=\"#myModal\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\" id=\"cancel8\">Cancel</button><input id=\"costs8\" value=\"$costs\" hidden><input id=\"costs_after8\" value=\"$costs_after\" hidden><input id=\"date8\" value=\"$date\" hidden><input id=\"plan8\" value=\"$act_plan\" hidden><input id=\"period8\" value=\"$act_period\" hidden><input id=\"rec8\" value=\"0\" hidden>";	//rec is 0 = one-of payment, 1 = monthly";
		}
		elseif (($period=="month") && ($act_period=="year") && ($numplan<$act_numplan)) { //upgrade en jaarlijks ipv mnd 
			$d1 = new DateTime("$currYear-$startMonth-$startDay");
			$d2 = new DateTime("$today");
			$interval = date_diff($d1, $d2,true);
			$months_payed = ($interval->format("%m") + ($interval->format("%y") * 12)) + 1;
	
			$d1 = new DateTime("$lastMonthYear-$lastMonth-$startDay");
			$d2 = new DateTime("$today");
			$interval = date_diff($d1, $d2,true);
			$days_used = $interval->format("%a");
				
			if (($act_plan=="VALUE") && ($plan=="STARTER")) {
				$costs = round($costs_value_year - ($months_payed * $costs_starter_month) - ($days_used * (($costs_value_year/365)-($costs_starter_month/30))),2);
				$costs = $costs-$costs1_setup+$costs6_setup;//changed
				$costs_after = $costs_value_year;
			}
			elseif (($act_plan=="PRO") && ($plan=="STARTER")) {
				$costs = round($costs_pro_year - ($months_payed * $costs_starter_month) - ($days_used * (($costs_pro_year/365)-($costs_starter_month/30))),2);
				$costs = $costs-$costs1_setup+$costs7_setup;//changed
				$costs_after = $costs_pro_year;
			}
			elseif (($act_plan=="PREMIUM") && ($plan=="STARTER")) {
				$costs = round($costs_premium_year - ($months_payed * $costs_starter_month) - ($days_used * (($costs_premium_year/365)-($costs_starter_month/30))),2);
				$costs = $costs-$costs1_setup+$costs8_setup;//changed
				$costs_after = $costs_premium_year;
			}
			elseif (($act_plan=="PRO") && ($plan=="VALUE")) {
				$costs = round($costs_pro_year - ($months_payed * $costs_value_month) - ($days_used * (($costs_pro_year/365)-($costs_value_month/30))),2);
				$costs = $costs-$costs2_setup+$costs7_setup;//changed
				$costs_after = $costs_pro_year;
			}
			elseif (($act_plan=="PREMIUM") && ($plan=="VALUE")) {
				$costs = round($costs_premium_year - ($months_payed * $costs_value_month) - ($days_used * (($costs_premium_year/365)-($costs_value_month/30))),2);
				$costs = $costs-$costs2_setup+$costs8_setup;//changed
				$costs_after = $costs_premium_year;
			}
			elseif (($act_plan=="PREMIUM") && ($plan=="PRO")) {
				$costs = round($costs_premium_year - ($months_payed * $costs_pro_month) - ($days_used * (($costs_premium_year/365)-($costs_pro_month/30))),2);
				$costs = $costs-$costs3_setup+$costs8_setup;//changed
				$costs_after = $costs_premium_year;
			}
	
			$date = "$startDay-$startMonth-$nextYear";
			$rec = 0;
			
			//id = 9
			$message1 = "One-off payment of € $costs and from $date the costs will be € $costs_after /yr. All costs are included with VAT.";
			if ($account=="ideal") {
				$message2 = "&nbsp;To proceed please confirm and continue with iDeal payment.<br><br><button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#myModalIDEALCHANGE".$s."\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\">Cancel</button>";					
			}
			else {
				$message2 = "&nbsp;Please confirm if you'd like to proceed?<br><br><button type=\"button\" class=\"btn btn-success\" id=\"confirm9\" data-toggle=\"modal\" data-target=\"#myModal\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\" id=\"cancel9\">Cancel</button><input id=\"costs9\" value=\"$costs\" hidden><input id=\"costs_after9\" value=\"$costs_after\" hidden><input id=\"date9\" value=\"$date\" hidden><input id=\"plan9\" value=\"$act_plan\" hidden><input id=\"period9\" value=\"$act_period\" hidden><input id=\"rec9\" value=\"0\" hidden>";	//rec is 0 = one-of payment, 1 = monthly	
			}
		}
		elseif (($period=="month") && ($act_period=="year") && ($numplan>$act_numplan)) { //downgrade en mnd ipv jaarlijks 
			if ($act_plan=="STARTER") $costs_after = $costs_starter_year;
			elseif ($act_plan=="VALUE") $costs_after = $costs_value_year;
			elseif ($act_plan=="PRO") $costs_after = $costs_pro_year;
			elseif ($act_plan=="PREMIUM") $costs_after = $costs_premium_year;
	
			$date = "$startDay-$nextMonth-$nextYearMonth";
			$rec = 0;
			
			//id = 10
			$message1 = "Beginning from $date the costs will be $costs_after /yr. All costs are included with VAT.";
			//er hoeft geen ideal betaling te gebeuren dus alleen confirmen, geen one-off costs
			$message2 = "&nbsp;Please confirm if you'd like to proceed?<br><br><button type=\"button\" class=\"btn btn-success\" id=\"confirm10\" data-toggle=\"modal\" data-target=\"#myModal\">Confirm</button>&nbsp;<button type=\"button\" class=\"btn btn-default cancel\" id=\"cancel10\">Cancel</button><input id=\"costs10\" value=\"$costs\" hidden><input id=\"costs_after10\" value=\"$costs_after\" hidden><input id=\"date10\" value=\"$date\" hidden><input id=\"plan10\" value=\"$act_plan\" hidden><input id=\"period10\" value=\"$act_period\" hidden><input id=\"rec10\" value=\"0\" hidden>";	//rec is 0 = one-of payment, 1 = monthly	
		}
		
		echo $message1;
		echo $message2;
		if ($account=="ideal") include("plans/payment-ideal.php");	
		unset($oneoff,$costs,$costs_after,$date);
	}
?>