<?php
// Config includen om met de MySQL database verbinding te maken
include("config-v2.php");			

if (!is_logged_in()) redirect();
else {
	$result = $_GET["result"];
	if (isset($result)) $result ="<br><div class=\"alert alert-warning alert-dismissable\">
	<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
	$result</div>";
	
	if ((isset($_POST["submit"])) or (isset($_POST["save"]))) {
		$password		= $_POST["password"];
		$password2		= $_POST["password2"];		
		$contact		= $_POST["contact"];		
		$company		= $_POST["company"];		
		$country		= $_POST["country"];		
		$vat			= $_POST["vat"];
		$phonenumber	= $_POST["phonenumber"];
		$email			= $_POST["email"];			
		$website		= $_POST["website"];		
		$coupon			= $_POST["coupon"];		
			
		if ($password!="") {
			if ($password==$password2) {
				$password = md5($password);
				mysqli_query($mysqli, "UPDATE users SET country='$country', 
													contact='$contact',
													company='$company',
													vat='$vat',
													phonenumber='$phonenumber',													
													website='$website',
													password='$password',
													email='$email',
													coupon='$coupon' WHERE uid='".$_SESSION['uidGO']."'");
				$result = "<br><div class=\"alert alert-success alert-dismissable\">
				<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
Settings updated.</div>";
			}
			else {
				mysqli_query($mysqli, "UPDATE users SET country='$country', 
													contact='$contact',
													company='$company',
													vat='$vat',
													phonenumber='$phonenumber',													
													website='$website',
													password='$password',
													email='$email',
													coupon='$coupon' WHERE uid='".$_SESSION['uidGO']."'");
				$result = "<br><div class=\"alert alert-danger alert-dismissable\">
				<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
				Settings updated. Passwords did not match and have not been saved!</div>";
			}
		}
		else {
			mysqli_query($mysqli, "UPDATE users SET country='$country', 
													contact='$contact',
													company='$company',
													vat='$vat',
													phonenumber='$phonenumber',													
													website='$website',
													email='$email',
													coupon='$coupon' WHERE uid='".$_SESSION['uidGO']."'");
				$result = "<br><div class=\"alert alert-success alert-dismissable\">
				<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
				Settings updated.</div>";
		}
	}

	$query = mysqli_query($mysqli, "select plan, period from plans where uid='".$_SESSION["uidGO"]."' and endDate IS NULL and period!='One-off' and period!='Set-up'") or die(mysqli_error($mysqli));
	$row = mysqli_fetch_array($query);
	$plan = $row["plan"];
	$period = $row["period"];
	
	$res		= mysqli_query($mysqli, "SELECT * FROM users WHERE uid='".$_SESSION['uidGO']."'");
	$myrow 		= mysqli_fetch_array($res);
	$uid		= $myrow["uid"];
	$account	= $myrow["account"]; //free, dev, pro
	$uname		= $myrow["uname"];	
	$country	= $myrow["country"];
	$contact	= $myrow["contact"];
	$company	= $myrow["company"];
	$vat		= $myrow["vat"];
	$phonenumber= $myrow["phonenumber"];
	$startDay	= $myrow["startDay"];
	$startMonth	= $myrow["startMonth"];
	$endDate	= $myrow["endDate"];
	$website		= $myrow["website"];	
	$email			= $myrow["email"];
	$a_check		= $myrow["access"];
	$currMonth	= date("m");
	$currDay	= date("d");
	$today 	= date("Y-m-d");
	$coupon		= $myrow["coupon"];
		
	if (($currMonth<=$startMonth) && ($currDay<$startDay)) {
		$currYear=date('Y', strtotime("-1 years", strtotime($today)));
		$nextYear=date("Y");
		$nextYearMonth=date("Y");
		$nextMonth=date("m");
		$lastMonth=date('m', strtotime("-1 months", strtotime($today)));
		$lastMonthYear=date('Y', strtotime("-1 months", strtotime($today)));
	}
	else {
		$currYear= date("Y");
		$nextYear=date('Y', strtotime("+1 years", strtotime($today)));
		$nextYearMonth=date('Y', strtotime("+1 months", strtotime($today)));
		$nextMonth = date('m', strtotime("+1 months", strtotime($today)));
		$lastMonth=date("m");
		$lastMonthYear=date("Y");
	}
	

	if ($endDate!="") $reac = 1;
	else $reac = 0;	
	
	//coupons
	$query = mysqli_query($mysqli, "select amount, eurosOperc, setupOsub from coupons where code='$coupon'") or die(mysqli_error($mysqli));
	$row = mysqli_fetch_array($query);
	$amount = $row["amount"];
	$eurosOperc = $row["eurosOperc"];
	$setupOsub = $row["setupOsub"];
	
	//pre defined bank account values
	if ($account=="ideal") {
		$qy = mysqli_query($mysqli, "SELECT * FROM ideal_subscriptions WHERE user='".$_SESSION["uidGO"]."' LIMIT 1") or die(mysqli_error($mysqli));
		$gegevens = mysqli_fetch_array($qy);
	
		$pre_bank = $gegevens['bank']; 	  
		$pre_account = $gegevens['account'];	  
	}
	//pre defined bank account values
	elseif ($account=="dd") {
		$qy = mysqli_query($mysqli, "SELECT t2.account,t2.accountholder FROM users as t1, dd_subscriptions as t2 WHERE t1.uid='".$_SESSION["uidGO"]."' and t1.subid=t2.id LIMIT 1") or die(mysqli_error($mysqli));
		$gegevens = mysqli_fetch_array($qy);
	
		$pre_accountholder = $gegevens['accountholder']; 	  
		$pre_account = $gegevens['account'];	  
	}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoDashboard</title>
<link rel="shortcut icon" href="favicon.ico" />

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendor/bootstrap-social/bootstrap-social.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script> 
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<style>
hr {
	margin-top:2px !important;
	margin-bottom:2px !important;
	color:#333 !important;
}
img {
	margin-left:5px;
	margin-top:1px;
}
.borderless td, .borderless th {
    border: none !important;
}
.btn-select {
	min-width:160px !important;
}
.non-highlight {
	top: 10px !important;
	background-color:
}
.col-lg-3 {
	padding-left: 2px !important;
	padding-right: 2px !important;
}
.col-md-3 {
	padding-left: 2px !important;
	padding-right: 2px !important;
}
.currency {
    display: inline-block;
    margin-top: 10px;
    vertical-align: top;
    font-size: 2rem;
    font-weight: 700;
}

.currency {
	padding:0 !important;
}
.duration {
	vertical-align:top !important;
	padding:0 !important;
}
.value {
    font-size: 3rem;
    font-weight: 300;
	vertical-align:bottom !important;
}
#message {
    position: fixed;
    top: 0;
    left: 62%;
    width: 22%;
	z-index:1000;
}
.alert-dismissable .close {
    right: 5px;
}
#arrow {
	position: relative;
    top: -20px;
    right: -62px;
}
#alert {
    margin: 0 auto;
	padding:2px;
	padding-left:20px;
	padding-top:10px;
}
/* Skills Progess Bar */
.progress {
	margin: 15px;
}
</style>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>

<body id="page">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="GoDashboardLogo220x50.png">
            </div>
<?php include("topnav-v2.php"); ?>
            <div class="navbar-default sidebar" role="navigation">
<?php include("sidenav-v2.php"); ?>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                        <div style="width:100%">
 						<?php
                            	echo "<div class=\"modal fade\" id=\"myModalREA\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"top: 31px !important; display: none;\">
						 <form role=\"form\" method=\"post\" action=\"$rea_page\">
							<div class=\"modal-dialog\">
										<div class=\"modal-content\">
											<div class=\"modal-header\">
												<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
												<h4 class=\"modal-title\" id=\"myModalLabel\">"; 
												echo "Are you sure you would like to re-activate your subscription?
												</div>
												<div class=\"modal-footer\">
												<button id=\"submit\" name=\"submit\" class=\"btn btn-primary\" type=\"submit\">Re-activate subscription</button>"; 
												echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button></div>";
										echo "</div>
									</div>
								   </form>
								</div>";
			?>
            			<?php echo $result; ?></div>      
                    &nbsp;
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
			<?php 
            if ($_GET['change']=="success") {
            ?>
             <div class="alert alert-success alert-dismissable">
            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Success! You're plan or payment method has been changed. If you have any questions please contact us!                
             </div>
            <?php
            }                      
            ?>
			<?php 
            if ($_GET['pay']=="success") {
            ?>
             <div class="alert alert-success alert-dismissable">
            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Success! You will receive an invoice per e-mail. If you have any questions please contact us!                
             </div>
            <?php
            }                      
            ?>
            <div id="message" hidden>
    			<div style="padding: 5px;">
                    <div class="alert alert-warning alert-dismissable" id="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="dismiss">×</button>                    
                    Click: <span class="alert-link">Open dashboard editor</span><br>to start creating dashboards!<img src="arrow.fw.png" id="arrow">
                    </div>
                </div>
			</div>
                    <div class="panel panel-default"> 
                    <div class="panel-heading">
                       <div class="row"> 
                       <div class="col-lg-4">Account Settings</div>                     
              		   <div class="col-lg-8" align="right"><?php include("registration_details.php"); ?></div>  
                       </div>
                    </div> 
                <form role="form" class="form-horizontal" action="account-v2.php" method="post" id="myForm">
                      <div class="panel-body">
                             <div class="row">
                               <div class="col-lg-6">                            
                         			<div class="form-group">
                                        <label for="static" class="control-label col-xs-4">Username</label>
                                        <div class="col-xs-8">
                                        	<p class="form-control-static" id="static"><?php echo $uname ?></p>
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label for="password" class="control-label col-xs-4">New password</label>
                                        <div class="col-xs-8">
                                        	<input class="form-control" type="password" name="password" id="password">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password2" class="control-label col-xs-4">Confirm password</label>
                                        <div class="col-xs-8">
                                        	<input class="form-control" type="password" name="password2" id="password2">
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label for="contact" class="control-label col-xs-4">Contact name</label>
                                        <div class="col-xs-8">
                                        	<input class="form-control" name="contact" id="contact" value="<?php echo $contact ?>" required>
                                        </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="company" class="control-label col-xs-4">Company</label>
                                        <div class="col-xs-8">
                                        	<input class="form-control" name="company" id="company" value="<?php echo $company ?>">
                                        </div>
                                  	</div>
                                   <div class="form-group">
                                        <label for="coupon" class="control-label col-xs-4">Coupon code</label>
                                        <div class="col-xs-8">
                                        	<input class="form-control" name="coupon" id="coupon" value="<?php echo $coupon ?>">
                                        </div>
                                  	</div>                                  </div>                              
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                            <label for="country" class="control-label col-xs-4">Country</label>
                                        <div class="col-xs-8">
                  							<?php include("sel-country.php"); ?>
                                         </div>
                                    </div> 
                                    <div class="form-group">
                                        <label for="vat" class="control-label col-xs-4">VAT</label>
                                        <div class="col-xs-8">
                                        	<input class="form-control" name="vat" id="vat" value="<?php echo $vat ?>">
                                        </div>
                                    </div>                                     
                                    <div class="form-group">
                                        <label for="phonenumber" class="control-label col-xs-4">Phonenumber</label>
                                        <div class="col-xs-8">
                                        	<input class="form-control" name="phonenumber" id="phonenumber" value="<?php echo $phonenumber ?>">
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label for="email" class="control-label col-xs-4">E-mail</label>
                                        <div class="col-xs-8">
                                        	<input class="form-control" name="email" id="email" value="<?php echo $email ?>" required>
                                        </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="website" class="control-label col-xs-4">Website</label>
                                        <div class="col-xs-8">
                                        	<input class="form-control" name="website" id="website" value="<?php echo $website ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-4">&nbsp;</div>
                                    <div class="col-xs-8"><button type="submit" class="btn btn-outline btn-primary" id="save" name="save">Save Changes</button>
                                    </div>                                    
                                    </div>
                            </div>
						</div>
              		</form>                                    
                    </div>
           
            <div class="panel panel-default"> 
              <div class="panel-heading">
              <div class="row"> 
              <div class="col-lg-4">Subscription</div>                     
              <div class="col-lg-8" align="right">    
                    <div class="btn-group" data-toggle="buttons">  
					  <label class="btn btn-default <?php if (($period=="year") || ($period=="")) echo "active"; ?>"><input type="radio" name="options" id="yearly" autocomplete="off" <?php if (($period=="year") || ($period=="")) echo "checked"; ?>> Yearly</label> 
                      <label class="btn btn-default <?php if ($period=="month") echo "active"; ?>"><input type="radio" name="options" id="monthly" autocomplete="off" <?php if ($period=="month") echo "checked"; ?>> Monthly</label>
                    </div>
               </div>
               <!-- /.col-lg-12 -->
               </div>                  
            </div>                                                    
            <!-- /.row -->
            <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
               <!-- /progressbar when account is changed or payed -->
                          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 51px !important; display: none;">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <h4 class="modal-title" id="myModalLabel">In Progress</h4>
                                  </div>
                                  <div class="modal-body">
                                  <div class="progress">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar"
                                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                        </div>
                                      </div>
                                      
                                  </div>
                              </div>
                              <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->
                </div>
                <!-- /.col-lg-12 -->
            </div>                     
            <div class="row" id="pricing">
             <!-- start pricing options FRONT -->               
              <div class="month" <?php if (($period=="year") || ($period=="")) echo "hidden"; ?>>             
              <div class="col-lg-12"> 	
                <!-- SHOW PRICE ITEM -->     
				<?php 
				if ($account=="FREE") {
                	echo "<div class=\"col-md-3 col-lg-3\">";
				}
                else { 
                	echo "<div class=\"col-md-3 col-lg-3 non-highlight\">";
				} 
				?>	
                		<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>TRIAL</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="value">FREE</span>
							</div>
                            <p class="text-muted">&nbsp;</p>
                            <hr>
                            <p class="text-muted">14 days trial</p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 5 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 7 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 1 hour</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support: e-mail<br><br></li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> No reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Basic Data Visuals</li>
						</ul>
						<div class="panel-footer paybuttons">
                        <?php 
							if ($account=="FREE") echo "<br><b>YOUR CURRENT PLAN</b><br><br>"; 
							else echo "<br><br>"; 						
						?>                        
                        </div>
					</div>
				</div>					
                <!-- /SHOW PRICE ITEM --> 
                <!-- SHOW PRICE ITEM -->				
               <?php 
				if (($period=="month") and ($plan=="STARTER")) {
                	echo "<div class=\"col-md-3 col-lg-3\">";
				}
                else { 
                	echo "<div class=\"col-md-3 col-lg-3 non-highlight\">";
				} 
				?>	
                		<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>STARTER</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs1Large,0); ?></span>
								<span class="duration">/mo</span>
							</div>
                            <p class="text-muted"><small>(billed monthly)</small></p>
                            <hr>
                            <p class="text-muted">Onboarding fee 
							<?php 
							if ($setupOsub==1) {
								$priceNew = discount($costs1_setupLarge, $amount, $eurosOperc);
								echo "<i style=\"text-decoration:line-through;\">€".round($costs1_setupLarge,0)."</i>";
								echo " €".round($priceNew,0).""; 
							}
							else {
								$priceNew = $costs1_setupLarge;
								echo "€".round($priceNew,0).""; 
							}
							?>
                            </p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 5 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 7 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 1 hour</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support: e-mail<br><br></li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> No reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Basic Data Visuals</li>
						</ul>
						<div class="panel-footer paybuttons" id="starter_month">
                        <?php
						if ($period=="month") {
							if (($plan==NULL) or ($plan!="STARTER")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_starter\">ACTIVATE</a>";
							elseif ($plan=="STARTER") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_starter\">ACTIVATE</a>";
						 ?>                            
                        </div>
                        <div class="panel-footer payplan" id="price_starter_month" hidden>
                        <!--payment option-->
                        <?php 
                        	$act_plan 	= "STARTER";
							$act_period = "month";						
							include("activate-v2.php"); 
						?>
                        <!-- /.payment option -->                        
                        </div>
					</div>
				</div>					
                <!-- /SHOW PRICE ITEM -->                     				
                <!-- SHOW PRICE ITEM -->				
               <?php 
				if (($period=="month") and ($plan=="PRO")) {
                	echo "<div class=\"col-md-3 col-lg-3\">";
				}
                else { 
                	echo "<div class=\"col-md-3 col-lg-3 non-highlight\">";
				} 
				?>	
                		<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>PRO</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs3Large,0); ?></span>
								<span class="duration">/mo</span>
							</div>
                            <p class="text-muted"><small>(billed monthly)</small></p>
                            <hr>
                            <p class="text-muted">Onboarding fee 
							<?php 
							if ($setupOsub==1) {
								$priceNew = discount($costs3_setupLarge, $amount, $eurosOperc);
								echo "<i style=\"text-decoration:line-through;\">€".round($costs3_setupLarge,0)."</i>";
								echo " €".round($priceNew,0).""; 
							}
							else {
								$priceNew = $costs3_setupLarge;
								echo "€".round($priceNew,0).""; 
							}
							?>
                            </p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 10 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 10 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 2 hours</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support: e-mail with priority<br></li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Enhanced reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Basic + Premium Data Visuals</li>
						</ul>
						<div class="panel-footer paybuttons" id="pro_month">
                        <?php
						if ($period=="month") {
							if (($plan==NULL) or ($plan!="PRO")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_pro\">ACTIVATE</a>";
							elseif ($plan=="PRO") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_pro\">ACTIVATE</a>";
						 ?>                            
                        </div>
                        <div class="panel-footer payplan" id="price_pro_month" hidden>
                        <!--payment option-->
                        <?php 
                        	$act_plan 	= "PRO";
							$act_period = "month";						
							include("activate-v2.php"); 
						?>
                        <!-- /.payment option -->                        
                        </div>
					</div>
				</div>					
                <!-- /SHOW PRICE ITEM -->
				<!-- SHOW PRICE ITEM -->                
               <?php 
				if (($period=="month") and ($plan=="PREMIUM")) {
                	echo "<div class=\"col-md-3 col-lg-3\">";
				}
                else { 
                	echo "<div class=\"col-md-3 col-lg-3 non-highlight\">";
				} 
				?>	
                		<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>ULTIMATE</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs4Large,0); ?></span>
								<span class="duration">/mo</span>
							</div>
                            <p class="text-muted"><small>(billed monthly)</small></p>
                            <hr>
                            <p class="text-muted">Onboarding fee 
							<?php 
							if ($setupOsub==1) {
								$priceNew = discount($costs4_setupLarge, $amount, $eurosOperc);
								echo "<i style=\"text-decoration:line-through;\">€".round($costs4_setupLarge,0)."</i>";
								echo " €".round($priceNew,0).""; 
							}
							else {
								$priceNew = $costs4_setupLarge;
								echo "€".round($priceNew,0).""; 
							}
							?>
                            </p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 25 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 25 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 4 hours</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support:  dedicated manager<br></li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Enhanced reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Basic + Premium Data Visuals</li>
						</ul>
						<div class="panel-footer paybuttons" id="premium_month">
						<?php
						if ($period=="month") {
							if (($plan==NULL) or ($plan!="PREMIUM")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_premium\">ACTIVATE</a>";
							elseif ($plan=="PREMIUM") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"month_premium\">ACTIVATE</a>";
                     	?>
                        </div>
                        <div class="panel-footer payplan" id="price_premium_month" hidden>
						<!--payment option-->
                        <?php 
                        	$act_plan 	= "PREMIUM";
							$act_period = "month";						
							include("activate-v2.php"); 
						?>
                        <!-- /.payment option -->                        
                        </div>
					</div>                   
                  </div>
				  <!-- /SHOW PRICE ITEM -->
                  </div>
                </div>                  
<!-- start pricing options BACK --> 
                <div class="year" <?php if ($period=="month") echo "hidden"; ?>>                          
                <div class="col-lg-12">                
               <!-- SHOW PRICE ITEM -->				
 				<?php 
				if ($account=="FREE") {
                	echo "<div class=\"col-md-3 col-lg-3\">";
				}
                else { 
                	echo "<div class=\"col-md-3 col-lg-3 non-highlight\">";
				} 
				?>	
                		<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>TRIAL</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="value">FREE</span>
							</div>
                            <p class="text-muted">&nbsp;</p>
                            <hr>
                            <p class="text-muted">14 days trial</p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 5 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 7 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 1 hour</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support: e-mail<br><br></li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> No reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Basic Data Visuals</li>
						</ul>
                        <div class="panel-footer paybuttons">
                        <?php 
							if ($account=="FREE") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
							else echo "<br><br>"; 						
						?>                        
                        </div>
					</div>
				</div>					
                <!-- /SHOW PRICE ITEM -->
                <!-- SHOW PRICE ITEM -->         
                <?php 
				if (($period=="year") and ($plan=="STARTER")) {
                	echo "<div class=\"col-md-3 col-lg-3\">";
				}
                else { 
                	echo "<div class=\"col-md-3 col-lg-3 non-highlight\">";
				} 
				?>	
                		<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>STARTER</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs5Large/12,0); ?></span>
								<span class="duration">/mo </span>
							</div>
                            <p class="text-muted"><small>(billed annually)</small></p>
                            <hr>
                            <p class="text-muted">Onboarding fee 
							<?php 
							if ($setupOsub==1) {
								$priceNew = discount($costs5_setupLarge, $amount, $eurosOperc);
								echo "<i style=\"text-decoration:line-through;\">€".round($costs5_setupLarge,0)."</i>";
								echo " €".round($priceNew,0).""; 
							}
							else {
								$priceNew = $costs5_setupLarge;
								echo "€".round($priceNew,0).""; 
							}
							?>
                            </p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 5 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 7 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 1 hour</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support: e-mail<br><br></li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> No reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Basic Data Visuals</li>
						</ul>
						<div class="panel-footer paybuttons" id="starter_year">
                        <?php
						if ($period=="year") {
							if (($plan==NULL) or ($plan!="STARTER")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_starter\">ACTIVATE</a>";
							elseif ($plan=="STARTER") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_starter\">ACTIVATE</a>";
						 ?>                            
                        </div>
                        <div class="panel-footer payplan" id="price_starter_year" hidden>
                        <!--payment option-->
                        <?php 
                        	$act_plan 	= "STARTER";
							$act_period = "year";						
							include("activate-v2.php"); 
						?>
                        <!-- /.payment option -->                        
                        </div>
					</div>
				</div>					
                <!-- /SHOW PRICE ITEM -->               			
                <!-- SHOW PRICE ITEM -->				
                <?php 
				if (($period=="year") and ($plan=="PRO")) {
                	echo "<div class=\"col-md-3 col-lg-3\">";
				}
                else { 
                	echo "<div class=\"col-md-3 col-lg-3 non-highlight\">";
				} 
				?>	
                		<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>PRO</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs7Large/12,0); ?></span>
								<span class="duration">/mo</span>
							</div>
                            <p class="text-muted"><small>(billed annually)</small></p>
                            <hr>
                            <p class="text-muted">Onboarding fee 
							<?php 
							if ($setupOsub==1) {
								$priceNew = discount($costs7_setupLarge, $amount, $eurosOperc);
								echo "<i style=\"text-decoration:line-through;\">€".round($costs7_setupLarge,0)."</i>";
								echo " €".round($priceNew,0).""; 
							}
							else {
								$priceNew = $costs7_setupLarge;
								echo "€".round($priceNew,0).""; 
							}
							?>
                            </p>
						</div>
						<ul class="list-group list-group-flush text-center" >
							<li class="list-group-item"><i class="icon-ok text-info"></i> 10 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 10 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 2 hours</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support: e-mail with priority<br></li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Enhanced reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Basic + Premium Data Visuals</li>
						</ul>
						<div class="panel-footer paybuttons" id="pro_year">
                        <?php
						if ($period=="year") {
							if (($plan==NULL) or ($plan!="PRO")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_pro\">ACTIVATE</a>";
							elseif ($plan=="PRO") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_pro\">ACTIVATE</a>";
						 ?>                            
                        </div>
                        <div class="panel-footer payplan" id="price_pro_year" hidden> 
                        <!--payment option-->
                        <?php 
                        	$act_plan 	= "PRO";
							$act_period = "year";						
							include("activate-v2.php"); 
						?>
                        <!-- /.payment option -->
						</div>	
					</div>
				</div>
				<!-- /SHOW PRICE ITEM -->   
				<!-- SHOW PRICE ITEM -->
                <?php 
				if (($period=="year") and ($plan=="PREMIUM")) {
                	echo "<div class=\"col-md-3 col-lg-3\">";
				}
                else { 
                	echo "<div class=\"col-md-3 col-lg-3 non-highlight\">";
				} 
				?>	
                		<div class="panel price panel-primary">
						<div class="panel-heading arrow_box text-center">
						<h3>ULTIMATE</h3>
						</div>
						<div class="panel-body text-center" style="background: #f8f8f8 !important;">
                        	<div>
								<span class="currency">€</span>
								<span class="value"><?php echo round($costs8Large/12,0); ?></span>
								<span class="duration">/mo</span>
							</div>
                            <p class="text-muted"><small>(billed annually)</small></p>
                            <hr>
                            <p class="text-muted">Onboarding fee 
							<?php 
							if ($setupOsub==1) {
								$priceNew = discount($costs8_setupLarge, $amount, $eurosOperc);
								echo "<i style=\"text-decoration:line-through;\">€".round($costs8_setupLarge,0)."</i>";
								echo " €".round($priceNew,0).""; 
							}
							else {
								$priceNew = $costs8_setupLarge;
								echo "€".round($priceNew,0).""; 
							}
							?>
                            </p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-info"></i> 25 Users</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> 25 Dashboards</li>
							<li class="list-group-item"><i class="icon-ok text-info"></i> User management</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> DataGuru consult: 4 hours</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Fanatical support:  dedicated manager<br></li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Enhanced reporting function</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> Basic + Premium Data Visuals</li>
						</ul>
						<div class="panel-footer paybuttons" id="premium_year">
                        <?php
                  		if ($period=="year") {
							if (($plan==NULL) or ($plan!="PREMIUM")) echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_premium\">ACTIVATE</a>";
							elseif ($plan=="PREMIUM") echo "<br><b>YOUR CURRENT PLAN</b><br><br>";
						}
						else echo "<a class=\"btn btn-lg btn-block btn-primary\" id=\"year_premium\">ACTIVATE</a>";
						?>
                        </div>
                        <div class="panel-footer payplan" id="price_premium_year" hidden>
                        <!--payment option-->
                        <?php 
                        	$act_plan 	= "PREMIUM";
							$act_period = "year";						
							include("activate-v2.php"); 
						?>
                        <!-- /.payment option -->	
                        </div>				
                      </div>                
                  </div>
				  <!-- /SHOW PRICE ITEM -->
                    </div>
                   </div>			          
				</div>
            	</div>
            </div>

            <!-- einde pricing -->
            
            <br>
			<div class="row">
                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                    <div class="panel-heading">
                       <div class="row"> 
                       <div class="col-lg-4">History and current plan</div>                     
              		   <div class="col-lg-8" align="right"><?php include("change_pay_method.php"); ?></div>  
                       </div>
                    </div> 
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
 									<?php 
										//alles ouder dan 11 mnd nog niet tonen
										$future = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m")+11, date("d"), date("Y")));
	
                                        $query = mysqli_query($mysqli, "select * from plans where uid='".$_SESSION["uidGO"]."' order by id DESC") or die(mysqli_error($mysqli));
                                        $meerdan = mysqli_num_rows($query);
                                        if ($meerdan > 0) {
											echo "<thead>
												<tr>
													<th>Plan</th>
													<th>Period</th>
													<th>Start date</th>
													<th>End date</th>
													<th>Amount incl VAT</th>
													<th>In Process</th>
												</tr>
											</thead>";										
                                            while ($row = mysqli_fetch_array($query)) {
												$plan = $row["plan"];
												$period = $row["period"];
                                                $startDate = $row["startDate"];
												if ($startDate<$future) $billed="<i class=\"fa fa-check\" style=\"color:green;\"></i>";
												else $billed="";
												$startDate = date("d M Y", strtotime($startDate));
												$endDate = $row["endDate"];
												if (($period=="Set-up") || ($period=="One-off")) $endDate="-";
												elseif ($endDate!=NULL) $endDate = date("d M Y", strtotime($endDate));
												if ($period=="Set-up") $period="Onboarding fee";

												
												$amount = $row["amount"];

                                                echo "<tr>
                                                       <td>$plan</td>
                                                       <td>$period</td>
													   <td>$startDate</td>
													   <td>$endDate</td>
													   <td>€ $amount</td>
													   <td>$billed</td>
                                                      </tr>";
                                            }
                                        }
										else echo "<tr>
                                                       <td>You currently have a trial account and have all options available as described in the Ultimate plan, with exception of DataGuru consult.</td>
												   </tr>";
                                    ?>                                   
                                    </tbody>
                                </table>
                                <!--START CHANGE PAY METHOD MODALS-->
                              <div class="modal fade" id="myModalCHANGE1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 31px !important; display: none;">
                             <?php echo "<form id=\"ideal\" role=\"form\" method=\"post\" action=\"plans/change_paymethod.php?p=".basename($_SERVER['PHP_SELF'])."&s=".base64_encode($s)."&method=2&startdate=$date\">"; ?>
							<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close " data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel"><b>GoDashboard account </b></h4><span class="text-muted small"><?php echo $description; ?>
                                                <hr class="Header-loggedInBar">
                                                <?php echo "".$_SESSION["unameGO"].""; ?>
											</div>
											<div class="modal-body">
                                            	<fieldset>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<?php include("sel-bank.php"); ?>
                                                        <label for="account" class="fa fa-university" rel="tooltip" title="bank"></label>           
                                                        </div>
                                                    </div>
  													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Account number" id="accountnumber" name="accountnumber" type="text" value="<?php echo "$pre_account"; ?>" required>
                                                        <label for="accountnumber" class="glyphicon glyphicon-credit-card" rel="tooltip" title="accountnumber"></label>           
                                                        </div>
                                                    </div>                                                
                                                 </fieldset>
                                                 <button id="submit" name="submit" class="btn btn-block btn-info" type="submit">Pay €<?php echo $pamount; ?></button>
												</div>
                                         </div>
									</div>
								   </form>
								</div>
							<div class="modal fade" id="myModalCHANGE2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top: 31px !important; display: none;">
						 <?php echo "<form id=\"dd\" role=\"form\" method=\"post\" action=\"plans/change_paymethod.php?p=".basename($_SERVER['PHP_SELF'])."&s=".base64_encode($s)."&method=3&startdate=$date\">"; ?>
							<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close " data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel"><b>GoDashboard account </b></h4><span class="text-muted small"><?php echo $description; ?>
                                                <hr class="Header-loggedInBar">
                                                <?php echo "".$_SESSION["unameGO"].""; ?>
											</div>
											<div class="modal-body">
                                            	<fieldset>
													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Account number" id="account" name="account" type="text" value="<?php echo "$pre_account"; ?>" required>
                                                        <label for="account" class="glyphicon glyphicon-credit-card" rel="tooltip" title="account"></label>           
                                                        </div>
                                                    </div>
  													<div class="form-group">
													<div class="icon-addon addon-sm">
														<input class="form-control" placeholder="Account holder" id="accountholder" name="accountholder" type="text" value="<?php echo "$pre_accountholder"; ?>" required>
                                                        <label for="accountholder" class="glyphicon glyphicon-user" rel="tooltip" title="accountholder"></label>           
                                                        </div>
                                                    </div>                                                 
                                                 </fieldset>
                                                 <button id="submit" name="submit" class="btn btn-block btn-info" type="submit">Pay €<?php echo $pamount; ?></button>
												</div>
                                         </div>
									</div>
								   </form>
								</div>  
                                <!--END CHANGE PAY METHOD MODALS-->
                              </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                </div>
            </div> 
		</div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Flot Charts JavaScript -->
    <script src="../vendor/flot/excanvas.min.js"></script>
    <script src="../vendor/flot/jquery.flot.js"></script>
    <script src="../vendor/flot/jquery.flot.pie.js"></script>
    <script src="../vendor/flot/jquery.flot.resize.js"></script>
    <script src="../vendor/flot/jquery.flot.time.js"></script>
    <script src="../vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>
    <!-- <script src="../data/flot-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <script>
	function myTimeout10() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '10%');}, 500); // WAIT 1 second
		  return(myVar);
	}
	function myTimeout40() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '40%');}, 500); // WAIT 1 second
		  return(myVar);
	}
	function myTimeout90() {  
		  // Skills Progress Bar
		  var $progress = $('.progress');
		  var $progressBar = $('.progress-bar');
	  
		  var myVar = setTimeout(function() {$progressBar.css('width', '90%');}, 500); // WAIT 1 second
		  return(myVar);
	}
	function postPayment(confirm) {
		myTimeout10();
		myTimeout40();
		myTimeout90();
		
		$(':button').prop('disabled', true); // Disable all the buttons
		$('a').attr('disabled', true);
  		$('#myModal').modal("show");
		
		var oneoff = "";
		
		//check which pricing was confirmed
		if (confirm=="confirm1") {
	    	var costs=$('#costs1').val();	
			var costs_after=$('#costs_after1').val();
			var date=$('#date1').val();
			var plan=$('#plan1').val();
			var period=$('#period1').val();
			var rec=$('#rec1').val();
		}
		else if (confirm=="confirm2") {
	    	var costs=$('#costs2').val();	
			var costs_after=$('#costs_after2').val();
			var date=$('#date2').val();
			var plan=$('#plan2').val();
			var period=$('#period2').val();
			var rec=$('#rec2').val();
		}
		else if (confirm=="confirm3") {
	    	var costs=$('#costs3').val();	
			var costs_after=$('#costs_after3').val();
			var date=$('#date3').val();
			var plan=$('#plan3').val();
			var period=$('#period3').val();
			var rec=$('#rec3').val();
		}
		else if (confirm=="confirm4") {
	    	var costs=$('#costs4').val();	
			var costs_after=$('#costs_after4').val();
			var date=$('#date4').val();
			var plan=$('#plan4').val();
			var period=$('#period4').val();
			var rec=$('#rec4').val();
		}
		else if (confirm=="confirm5") {
	    	var costs=$('#costs5').val();	
			var costs_after=$('#costs_after5').val();
			var date=$('#date5').val();
			var plan=$('#plan5').val();
			var period=$('#period5').val();
			var rec=$('#rec5').val();
		}
		else if (confirm=="confirm6") {
	    	var costs=$('#costs6').val();	
			var costs_after=$('#costs_after6').val();
			var date=$('#date6').val();
			var plan=$('#plan6').val();
			var period=$('#period6').val();
			var rec=$('#rec6').val();
		}
		else if (confirm=="confirm7") {
	    	var oneoff=$('#oneoff7').val();	
	    	var costs=$('#costs7').val();	
			var costs_after=$('#costs_after7').val();
			var date=$('#date7').val();
			var plan=$('#plan7').val();
			var period=$('#period7').val();
			var rec=$('#rec7').val();
		}
		else if (confirm=="confirm8") {
	    	var costs=$('#costs8').val();	
			var costs_after=$('#costs_after8').val();
			var date=$('#date8').val();
			var plan=$('#plan8').val();
			var period=$('#period8').val();
			var rec=$('#rec8').val();
		}
		else if (confirm=="confirm9") {
	    	var costs=$('#costs9').val();	
			var costs_after=$('#costs_after9').val();
			var date=$('#date9').val();
			var plan=$('#plan9').val();
			var period=$('#period9').val();
			var rec=$('#rec9').val();
		}
		else if (confirm=="confirm10") {
	    	var costs=$('#costs10').val();	
			var costs_after=$('#costs_after10').val();
			var date=$('#date10').val();
			var plan=$('#plan10').val();
			var period=$('#period10').val();
			var rec=$('#rec10').val();
		}

		$.post( 
			"plans/change-v2.php",
			{ uid: <?php echo $_SESSION['uidGO']; ?>,
			  account: '<?php echo $account; ?>',
			  oneoff: oneoff,
			  costs: costs,
			  costs_after: costs_after,
			  date: date,
			  plan: plan,
			  period: period,
			  rec: rec },
			function(data) {
				//alert(data);
				console.log(data);
				if ( data.length !== 0 ) {
					window.location = 'account-v2.php?change=success';
				}
				//show error
				else {
					window.location = 'account-v2.php?change=error';
				}
			}
		 );
	}
	
	$(document).ready(function(){
	  //if dismiss does not have a value then
	  if (!localStorage.getItem("dismiss")) {
			$("#message").fadeIn();

			$(window).scroll(function() {
			
				if ($(this).scrollTop()>0)
				 {
					$('#message').fadeOut();
				 }
				else
				 {
				  $('#message').fadeIn();
				 }
			 });		
		  	$('#dismiss').click(function() {
			  localStorage.setItem("dismiss", "true");
		  	});
	  }
	  //show alert
	  //else $('#alert').hide();

	  //clear localStorage, leave in for testing purpose only otherwise slash out
	  //localStorage.removeItem("dismiss");
	  
	  $("#confirm1").click(function(){
		postPayment('confirm1');			
	  });
	  $("#confirm2").click(function(){
		postPayment('confirm2');			
	  });
	  $("#confirm3").click(function(){
		postPayment('confirm3');			
	  });
	  $("#confirm4").click(function(){
		postPayment('confirm4');			
	  });
	  $("#confirm5").click(function(){
		postPayment('confirm5');			
	  });
	  $("#confirm6").click(function(){
		postPayment('confirm6');			
	  });
	  $("#confirm7").click(function(){
		postPayment('confirm7');			
	  });
	  $("#confirm8").click(function(){
		postPayment('confirm8');			
	  });
	  $("#confirm9").click(function(){
		postPayment('confirm9');			
	  });
	  $("#confirm10").click(function(){
		postPayment('confirm10');			
	  });
	  
	  $(".cancel").click(function(){
		$('.paybuttons').fadeIn();
		$('.payplan').hide();						
	  });
      $("input[id='monthly']").change(function(){
		$('.year').hide();        
		$('.month').fadeIn();        
	  });
	  $("input[id='yearly']").change(function(){
		$('.month').hide();           
		$('.year').fadeIn();
      });
	  $("#month_starter").click(function(){
		$('.paybuttons').fadeIn();
		$('#starter_month').hide();
		$('.payplan').hide();		          
		$('#price_starter_month').fadeIn();		
      });
	  $("#month_pro").click(function(){
		$('.paybuttons').fadeIn();
		$('#pro_month').hide();
		$('.payplan').hide();					
		$('#price_pro_month').fadeIn(); 		
      });
	  $("#month_premium").click(function(){
		$('.paybuttons').fadeIn();
		$('#premium_month').hide();
		$('.payplan').hide();			
		$('#price_premium_month').fadeIn(); 		
      });
	  $("#year_starter").click(function(){
		$('.paybuttons').fadeIn();
		$('#starter_year').hide();
		$('.payplan').hide();
		$('#price_starter_year').fadeIn(); 		
      });
	  $("#year_pro").click(function(){
		$('.paybuttons').fadeIn();		
		$('#pro_year').hide();
		$('.payplan').hide();
		$('#price_pro_year').fadeIn(); 		
      });
	  $("#year_premium").click(function(){
		$('.paybuttons').fadeIn();			
		$('#premium_year').hide();
		$('.payplan').hide();
		$('#price_premium_year').fadeIn(); 		
      });
	});
    </script>
</body>
</html>
<?php } ?>