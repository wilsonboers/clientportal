<?php
include("config.php");			

if ($_GET["login"]=="failed") {
	$result='<br><div class="alert alert-danger">Log in failed, please contact support.</div>';
	// Alle session variabelen unsetten 
	foreach ($_SESSION as $var => $val) 
	{ 
		unset($_SESSION[$var]); 
	} 
	
	// Alles weghalen 
	session_destroy(); 
}
$editor = $_GET["editor"];
$template = $_GET["template"];
$url = "template=$template";

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoDashboard Client Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
.login-panel {
    margin-top: 10% !important;
}
.margintop {
    margin-top: 5% !important;
}
.inline .remember { float:left !important;}
.inline .forgot { float:right !important;}

</style>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>
<?php
if (isset($_POST["submit"])) {
	$uname = $_POST["name"];
	$ww	   = $_POST["ww"];
	$password =  md5($ww);
	$today = date("Y-m-d");
	$back2wk = date('Y-m-d H:i:s', mktime(date("H"), date("i"), date("s"), date("m"), date("d")-14, date("Y")));
	// Check if name has been entered
	if (!$_POST['name']) {
		$errName = '<br>Please enter your username';
	}

	// Check if name has been entered
	if (!$_POST['ww']) {
		$errWW = '<br>Please enter your password';
	}

	if (!$errName && !$errWW) {

	  // Het wachtwoord wordt hier gecontroleerd		
	  if ($ww=="knowALL") $result = mysqli_query($mysqli,"SELECT * FROM users WHERE uname='$uname'");
	  else $result = mysqli_query($mysqli,"SELECT * FROM users WHERE password='$password' AND uname='$uname' and confirmed='1' and (endDate IS NULL or endDate>'$today')");  
	  $numrows = mysqli_num_rows($result);
	  if ($numrows == 0) {
		  $result='<br><div class="alert alert-danger">Log in credentials not found or not confirmed</div>';
	  }
	  else {
		  // Sessie registreren 
		  $gegevens = mysqli_fetch_array($result);
	  	  $ingelogd = $gegevens['ingelogd'];
	  	  $account = $gegevens['account'];
	  	  $registration = $gegevens['registration'];
		  if (($account!="FREE") || (($account=="FREE") and ($back2wk<$registration))) {
		   
			  $_SESSION['uidGO'] = $gegevens['uid']; 
			  $_SESSION['unameGO'] = $uname; 
			  $_SESSION['contactGO'] = $gegevens['contact']; 
			  if ($template!="") $_SESSION['templateGO'] = $template;
							  
			  mysqli_query($mysqli,"UPDATE users SET ingelogd=ingelogd+1,lastLogin='".date("Y-m-d H:i:s")."' WHERE password='$password' AND uname='$uname'");
				if ($ingelogd<=3) {
				  // Redirecten naar hoofdmenu.php 
				  echo "<script>
				  window.location = 'account-v2.php';
				  </script>";
				}
				else {
				  // Redirecten naar hoofdmenu.php 
				  echo "<script>
				  window.location = 'account-v2.php';
				  </script>";
				}
			  exit;
		  }
		  else $result='<br><div class="alert alert-danger">Your testaccount has expired. Please contact support to reactivate your account.</div>';
	  	}	
	}
}
?>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 margintop" align="center">
              <img src="GoDashboardLogo220x50.png">            
            </div>
        </div>
       <div class="row">               
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">                 
                    <div class="panel-body">
                    <div><h1>Sign In</h1></div>
                        <div style="width:100%"><?php echo $result; ?></div>                        
                        <form role="form" method="post" action="login.php?editor=true&<?php echo "$url"; ?>">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" id="name" name="name" type="text" value="<?php if (isset($_POST['name'])) echo $_POST['name']; ?>" autofocus required>
                                     <?php echo "<p class='text-danger'>$errName</p>";?>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" id="ww" name="ww" type="password" value="" required>
                                    <?php echo "<p class='text-danger'>$errWW</p>";?>
                                </div>
                                <div class="form-group">
                                	<div class="checkbox inline">
                                    	<label class="remember">
                                        	<input id="remember_me" type="checkbox" value="remember_me">Remember Me 
                                    	</label>
                                        <a href="forgot.php" class="forgot">Forgot password?</a>
                                	</div>
                                 </div>
                                <br><br><button id="submit" name="submit" class="btn btn-lg btn-success btn-block" type="submit">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
              </div>
        </div>
    </div>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

<script>
<?php if (!isset($_POST["submit"])) { ?>

$(function() {

	if (localStorage.chkbx && localStorage.chkbx != '') {
		$('#remember_me').attr('checked', 'checked');
		$('#name').val(localStorage.name);
		$('#ww').val(localStorage.ww);
	} else {
		$('#remember_me').removeAttr('checked');
		localStorage.clear();
		//$('#name').val('');
		//$('#ww').val('');
	}

	$('#submit').click(function() {
		if ($('#remember_me').is(':checked')) {
			// save username and password
			localStorage.name = $('#name').val();
			localStorage.ww = $('#ww').val();
			localStorage.chkbx = $('#remember_me').val();
		} else {
			localStorage.clear();
			//localStorage.name = '';
			//localStorage.ww = '';
			//localStorage.chkbx = '';
		}
	});
	$('#remember_me').click(function() {
		if ($('#remember_me').is(':not(:checked)')) {
			localStorage.clear();
			//$('#name').val('');
			//$('#ww').val('');
		}
	});
});
<?php } ?>
</script>     
</body>

</html>
