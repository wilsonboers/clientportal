<?php
include("config.php");
include('../integrations/KFAPI/class.php');

echo "<script src=\"../vendor/jquery/jquery.min.js\"></script>";
		
if (!is_logged_in()) redirect();
else {
  //is session gs_add gezet dan bekijk of er een nw lege template moet worden aangemaakt
  if ($_SESSION['gs_add']==1) {
	$res		= mysqli_query($mysqli, "SELECT clientidKF,uidKF,getstarted FROM users WHERE uid='".$_SESSION['uidGO']."'");
	$myrow 		= mysqli_fetch_array($res);
	$getstarted	= $myrow["getstarted"];
	$clientidKF = $myrow['clientidKF'];
	$uidKF = $myrow['uidKF'];
	if ($getstarted==0) {
		$curl = new GoDashboard("4ee7578e56bf1e12452bd922ea6042d0954b1462");

	//import tabs into library
		$tabid = "29e2c40b7ce4bb2023f1de99ab816285";			
		$curl->setUrl("https://app.klipfolio.com/api/1.0/tabs/". $tabid ."/@/import");
	
		$data_array = array(	
			"client_id" 	=> 		"$clientidKF"
		);
	
		$data = json_encode($data_array);
	
		$curl->setCurlPost($data);
	
		$curl->parseJson();
		
		//get new tab id (tab only created in library
		$tabidlocation = $curl->data_string->meta->location;
	
		$tabidarray = explode("/", $tabidlocation);
	
		$new_tabid = end(array_values($tabidarray));
	//end import tabs into library			
	//place new tab in My Dashboards overview		
		$curl->setUrl("https://app.klipfolio.com/api/1.0/users/". $uidKF ."/tab-instances");
	
		$data_array = array(	
			"tab_ids" 	=> 		["$new_tabid"]
		);
	
		$data = json_encode($data_array);
		
		$curl->setCurlPut($data);
	
		$curl->parseJson();			
		//end place new tab in My Dashboards overview
		
		unset($curl);

		mysqli_query($mysqli,"UPDATE users SET getstarted='1' WHERE uid='".$_SESSION['uidGO']."'");
		mysqli_query($mysqli, "INSERT INTO KFclients (uid,clientid,uidKF,imported_tabid,new_tabid) VALUES ('".$_SESSION['uidGO']."','$clientidKF','$uidKF','$tabid','$new_tabid')") or die(mysqli_error($mysqli));
	}
  }
  $template=$_GET["template"];
 // $starturl = "dashboard/?template=$template";
  //if ($template=="") $template=$_SESSION["templateGO"]; //oude redirect om de oorspronkelijk bij registratie geselecteerde gallyery item te openen wanneer men de editor opent  
  /*if (isset($template)) {
	  	//laatst toegevoegde tab altijd mee openen
		$res		= mysqli_query($mysqli, "SELECT new_tabid FROM KFclients WHERE uid='".$_SESSION['uidGO']."' ORDER BY id DESC LIMIT 1");
		$myrow 		= mysqli_fetch_array($res);
		$new_tabid	= $myrow["new_tabid"];
		if ($new_tabid!="") $starturl = "dashboard/?template=$template#tab-$new_tabid";
		else $starturl = "dashboard/?template=$template";
  }
  else $starturl = "dashboard";*/

  $starturl = "dashboard/?template=$template";
  
  $exactonline=$_GET["exactonline"];
  $magento=$_GET["magento"];
  $fbc=$_GET["fbc"];
  $trustpilot=$_GET["trustpilot"];
  $ccvshop=$_GET["ccvshop"];
  $intid=$_GET["intid"];
  $ds_datasource=$_GET["datasource"];
  $ds_type=$_GET["type"];
  
  if ($exactonline==1) {
	include_once("includes/exactonline.php");
  }
  elseif ($magento==1) {
	include_once("includes/magento.php");
  }
  elseif ($fbc==1) {
	include_once("includes/fbc.php");
  }
  elseif ($trustpilot==1) {
	include_once("includes/trustpilot.php");
  }
  elseif ($ccvshop==1) {
	include_once("includes/ccvshop.php");
  }
  elseif ($ds_datasource==1) {
	  if ($ds_type==1) $starturl = "datasources/setup?execution=e1s1&type=file_upload";
	  elseif ($ds_type==2) $starturl = "datasources/setup?execution=e1s1&type=simple_rest";
	  elseif ($ds_type==3) $starturl = "datasources/setup?execution=e1s1&type=db";
	  elseif ($ds_type==4) $starturl = "datasources/setup?execution=e1s1&type=ftp";
  }

  //bedoeld om bij start een nieuwe template te activeren waar gebruiker direct op komt, uitgezet
  /*if ($getstarted==0) {
	  $starturl = $starturl ."#tab-$new_tabid";
  }*/
  //onderstaande zorgt ervoor dat data visuals automatisch kunnen worden toegevoegd na 1x inloggen (in create_user wordt de juiste link gemaakt en in confirm wordt er een sessie gezet, functie nu uitgeschakeld
  /*elseif (isset($_SESSION["templateGO"])) {
	  $starturl = "klips/addKlip/".$_SESSION["templateGO"]."";
	  //na de eerste keer unsetten
	  unset($_SESSION["templateGO"]);
  }*/
  if ($_SESSION['uidGO']>=185) $token = sso($_SESSION['unameGO'],"https://godashboard.nl/go/client/end.php","1");
  else $token = sso($_SESSION['unameGO'],"https://godashboard.nl/go/client/end.php","0");
  $klipfolioCompanyId = "2739cb1f0c6c0c6c0950b5d3533fcb0f";
		
  echo "<script>
  function initSSO(url){
		$.ajax({	 
			url : \"https://app.klipfolio.com/users/sso_auth\",
			type: \"post\",
			xhrFields:{
				withCredentials:true
			},
			headers:{
				\"KF-SSO\":\"$token\",
				\"KF-Company\":\"$klipfolioCompanyId\"
			},
			dataType:\"json\",
		   
			// on success, the data object will contain information about
			// the authenticated user
			// ----------------------------------------------------------
			success : function(data){
				window.location.href = 'https://app.klipfolio.com/$starturl';
			},
		 
			// on error, err.responseJSON will contain an error code
			// -----------------------------------------------------
			error : function(err){
				window.location.href = 'logout.php';
			}
		})
	};	
	
	//sso op achtergrond herstellen en redirect naar index
	$(initSSO());
	
	</script>";
}

?>