<?php
// Vul hier je MySQL database gegevens in 
$db['host'] = '5.79.106.159'; 			   // MySQL host
$db['user'] = 'go';            // MySQL gebruikersnaam 
$db['pass'] = 'JsH77rSWyD0YQh2a';      // Wachtwoord voor bovenstaande gebruiker 
$db['name'] = 'godashboard';        // Database naam 

// Sessie starten 
session_start(); 
header("Cache-control: no-cache");
header('content-type:text/html;charset=utf-8');

//define costs
$btw = 1.21;
$costs_starter_month = round(35*$btw,2);
$costs1Large = 35;

$costs_value_month = "";

$costs_pro_month = round(59*$btw,2);
$costs3Large = 59;

$costs_premium_month = round(99*$btw,2);
$costs4Large = 99;

$costs_starter_year = round(348*$btw,2);
$costs5Large = 348;

$costs_value_year = "";

$costs_pro_year = round(588*$btw,2);
$costs7Large = 588;

$costs_premium_year = round(1068*$btw,2);
$costs8Large = 1068;

// MySQL verbinding maken 
$mysqli = mysqli_connect($db['host'],$db['user'],$db['pass'],$db['name']);
/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

// Inlog functie 
// Retourneerd TRUE als user is ingelogd 
function is_logged_in() 
{ 
    global $_SESSION; 
		
	if ((!isset($_SESSION['unameGO'])) || (!isset($_SESSION['uidGO']))) {
		$return = 0;		
	}
	else $return = 1;

	return ($return); 		
}
function redirect() 
{
	if (headers_sent()) { 
		echo "<script language=\"JavaScript\">"; 
		echo "document.location='logout.php'"; 
		echo "</script>"; 
		exit();
	} 
	else { 
		header("location: logout.php"); 
		exit();
	}  		 
}
//wacht niet op server response
function post_without_wait($url, $params)
{
	exec ("/usr/bin/curl -d \"$params\" $url > /dev/null 2>&1 &");
}
//server response wel teruggeven
function post_with_wait($url, $params)
{
	exec ("/usr/bin/curl -d \"$params\" $url", $result);
	return($result[0]);
}

function sso($email,$logout_url,$newuser) {
	$klipfolioCompanyId = "2739cb1f0c6c0c6c0950b5d3533fcb0f";// Your Klipfolio Company ID
	$sso_key = "7adbef7351d433148a69a46f83e166838325f7b7";// Your SSO Key
	
	$salted = $sso_key . $klipfolioCompanyId;
	$hash = hash('sha1',$salted,true);
	$saltedHash = substr($hash,0,16);
	
	$iv = substr(md5(microtime()),rand(0,16),16); //Generate random 16 bit string
	
	$expires = time() + (24 * 60 * 60); //24 uur * 60 * 60 sec = 1 dag ingelogd

	if ($newuser==1) {
		list($email2,$domain)=explode('@',$email);
		$emaillogin = $email2.".".$domain."@godashboard.nl";
	}
	else $emaillogin = $email;
		
	//Your User Data
	$user_data = array(
	  "logout_url" => "$logout_url",
	  "expires" => "$expires",
	  "email" => "$emaillogin"
	);
	
	$data = json_encode($user_data);
	$data = $iv . $data;
	
	$pad = 16 - (strlen($data) % 16);
	$data = $data . str_repeat(chr($pad), $pad);
	
	$cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128,'','cbc','');
	mcrypt_generic_init($cipher, $saltedHash, $iv);
	$encryptedData = mcrypt_generic($cipher,$data);
	mcrypt_generic_deinit($cipher);
	
	$encryptedData = base64_encode($encryptedData);
	
	return($encryptedData);
}
?>