<?php
// Config includen om met de MySQL database verbinding te maken
include("config.php");			

if (!is_logged_in()) redirect();
else {
	$result = $_GET["result"];
	if (isset($result)) $result ="<br><div class=\"alert alert-warning\">$result</div>";

	unset($_SESSION['gs_add']);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GoDashboard</title>
<link rel="shortcut icon" href="favicon.ico" />

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
img {
	margin-left:5px;
	margin-top:1px;
}
.row .show-grid {
	margin: 0;
}
.hoverDiv:hover {background: #f5f5f5 !important;}
</style>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>

<body onfocus="focuspage(event)">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="GoDashboardLogo220x50.png">
            </div>
<?php include("topnav.php"); ?>
            <div class="navbar-default sidebar" role="navigation">
<?php include("sidenav.php"); ?>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
                                <div style="width:100%">
						<?php
                            	echo "<div class=\"modal fade\" id=\"myModalREA\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"top: 31px !important; display: none;\">
						 <form role=\"form\" method=\"post\" action=\"$rea_page\">
							<div class=\"modal-dialog\">
										<div class=\"modal-content\">
											<div class=\"modal-header\">
												<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
												<h4 class=\"modal-title\" id=\"myModalLabel\">"; 
												echo "Are you sure you would like to re-activate your subscription?
												</div>
												<div class=\"modal-footer\">
												<button id=\"submit\" name=\"submit\" class=\"btn btn-primary\" type=\"submit\">Re-activate subscription</button>"; 
												echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button></div>";
										echo "</div>
									</div>
								   </form>
								</div>";
			?>			<?php echo $result; ?></div>          
 <?php if ($_GET["step"]==2) { ?>
            <div class="row">
                <div class="col-lg-12">
                 <br>
                  <div class="well">
                         <h2><button type="button" class="btn btn-default" onclick="goBack()"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></button> Getting started 2/3</h2>        
                         <br>
                         <h4>Ok great choice!<br><br>
After pressing the greenbutton “Add Data Visual” you will automatically be directed to your dashboard in a brand new browser tab! Now you know, if you want to come back here to add more data visuals just switch browser tabs.<br><br>
Ready to create your first dashboard... press the button!<br><br></h4>
                  </div> 
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->                     
            
            <div class="row">
                 <div class="col-lg-12">
                  <div class="table-responsive">
					<?php
					$id = $_GET["id"];
					$name = $_GET["name"];                    
					
					if ($id==51) echo "<div class=\"alert alert-warning\">If you're having trouble with retrieving your data after Oauth, please read <a href=\"javascript:window.open('http://developersuite.nl/magento/explain.php', 'Explain', 'top=0,left=0,width=screen.availWidth,height=screen.availHeight').focus;\">this article</a> or contact us for support.</div>";
  					?>                  
                     <table class="table table-bordered">
                       <tbody>
				<?php 
					echo "<tr><td colspan=\"2\"><div><h4><b>$name</b></h4></div></td></tr>";
					$_SESSION['gs_add'] = 1; //get started add service
					$login = "editor.php";	
					$query = mysqli_query($mysqli, "select * from KFintegrations where serviceid='$id' and  active='1' and getstarted='1' order by id") or die(mysqli_error($mysqli));
					while ($row = mysqli_fetch_array($query)) {						
                    	$intid = $row["id"];
                    	$heading = $row["heading"];
						$description = $row["description"];
						$url = $row["url"];
						$image = $row["image"];						
						$klipid = $row["klipid"];
						
						echo "<tr>
                               <td width=\"50%\"><div><h4><b>$heading</b></h4></div><div>$description</div><br><div>";
						if ($id<50) {
							echo "<a href=\"".$login."?".$url."\" class=\"btn btn-success btn-lg\" target=\"editor\">Add Data Visual</a>";
						}
						//exactonline
						elseif ($id==50) {	
							//check if oauth al is uit  gevoerd
							$qs = mysqli_query($mysqli,"SELECT * FROM exactonline WHERE uid='".$_SESSION["uidGO"]."' and authorizationcode IS NOT NULL LIMIT 1") or die(mysqli_error($mysqli));
							$numrows = mysqli_num_rows($qs);
							if ($numrows == 0) { //popup naar oauth process			
								echo "<button class=\"btn btn-success btn-lg\" onclick=\"myFunction()\">
										Add Data Visual
									</button>";
							}
							else { //naar editor, daar eerst import van visual uitvoeren		
								echo "<a href=\"".$login."?exactonline=1&intid=$klipid\" class=\"btn btn-success btn-lg\" target=\"editor\">Add Data Visual</a>";
							}
						}
						//einde exactonline
						//Magento
						elseif ($id==51) {	
							//check if oauth al is uit  gevoerd
							$qs = mysqli_query($mysqli,"SELECT * FROM magento WHERE uid='".$_SESSION["uidGO"]."' and token IS NOT NULL LIMIT 1") or die(mysqli_error($mysqli));
							$numrows = mysqli_num_rows($qs);
							if ($numrows == 0) { //popup naar oauth process			
								echo "<button class=\"btn btn-success btn-lg\" onclick=\"myFunction()\">
										Add Data Visual
									</button>";
							}
							else { //naar editor, daar eerst import van visual uitvoeren		
								echo "<a href=\"".$login."?magento=1&intid=$klipid\" class=\"btn btn-success btn-lg\" target=\"editor\">Add Data Visual</a>";
							}
						}
						//einde Magento
						//FBC, feedbackcompany
						elseif ($id==52) { //naar editor, daar eerst import van visual uitvoeren		
							echo "<a href=\"".$login."?fbc=1&intid=$klipid\" class=\"btn btn-success btn-lg\" target=\"editor\">Add Data Visual</a>";
						}
						//einde FBC
						//Trustpilot
						elseif ($id==53) { //naar editor, daar eerst import van visual uitvoeren		
							//check if oauth al is uit  gevoerd
							$qs = mysqli_query($mysqli,"SELECT * FROM trustpilot WHERE uid='".$_SESSION["uidGO"]."' and accesstoken IS NOT NULL and businessunit_id IS NOT NULL LIMIT 1") or die(mysqli_error($mysqli));
							$numrows = mysqli_num_rows($qs);
							if (($numrows == 0) and ($klipid!="94e64671cdad3b8a72941d432fcfc29d")) { //popup naar oauth process			
								echo "<button class=\"btn btn-success btn-lg\" onclick=\"myFunction()\">
										Add Data Visual
									</button>";
							}
							else { //naar editor, daar eerst import van visual uitvoeren		
								echo "<a href=\"".$login."?trustpilot=1&intid=$klipid\" class=\"btn btn-success btn-lg\" target=\"editor\">Add Data Visual</a>";
							}
						}
						//einde Trustpilot
						//CCV Shop
						elseif ($id==54) { //naar editor, daar eerst import van visual uitvoeren		
							//check if connectie al is uit  gevoerd
							$qs = mysqli_query($mysqli,"SELECT * FROM ccvshop WHERE uid='".$_SESSION["uidGO"]."' LIMIT 1") or die(mysqli_error($mysqli));
							$numrows = mysqli_num_rows($qs);
							if ($numrows == 0) { //popup naar oauth process			
								echo "<button class=\"btn btn-success btn-lg\" onclick=\"myFunction()\">
										Add Data Visual
									</button>";
							}
							else { //naar editor, daar eerst import van visual uitvoeren		
								echo "<a href=\"".$login."?ccvshop=1&intid=$klipid\" class=\"btn btn-success btn-lg\" target=\"editor\">Add Data Visual</a>";
							}
						}
						//einde CCV Shop
						echo "</div>
							   </td>
                               <td align=\"center\"><img src=\"../integrations/images/".$name."_images/".$image.".png\" style=\"max-height:200px; max-width:500px;\"></td>
							 </tr>";				

					}
					//exactonline
					if ($id==50) {
						echo "<script>
							function myFunction() {
								var myWindow = window.open(\"$url?uid=".$_SESSION["uidGO"]."&intid=$klipid\", \"exact\", \"top=100,left=500,width=300,height=400\").focus;

						}
						</script>";
					}
					//einde exactonline
					//Magento
					if ($id==51) {
						echo "<script>
							function myFunction() {
								var myWindow = window.open(\"$url?uid=".$_SESSION["uidGO"]."&intid=$klipid\", \"magento\", \"top=50,left=400,width=500,height=600\").focus;

						}
						</script>";
					}
					//einde Magento
					//Trustpilot
					if ($id==53) {
						echo "<script>
							function myFunction() {
								var myWindow = window.open(\"$url?uid=".$_SESSION["uidGO"]."&intid=$klipid\", \"trustpilot\", \"top=50,left=400,width=500,height=600\").focus;

						}
						</script>";
					}
					//einde Trustpilot
					//CCV Shop
					if ($id==54) {
						echo "<script>
							function myFunction() {
								var myWindow = window.open(\"$url?uid=".$_SESSION["uidGO"]."&intid=$klipid\", \"ccvshop\", \"top=50,left=400,width=500,height=600\").focus;

						}
						</script>";
					}
					//einde CCV Shop
                    ?>
                    	</tbody>
                     </table> 
                     <br><br> 
              	</div>
        	</div>
                    <br><br>
        	</div>
<?php } 
else { ?>
            <div class="row">
                <div class="col-lg-12">
                <br>
                  <div class="well">
                                <h2>Getting started 1/3</h2>        
                                <br>
                                <h4>In 3 steps we'll show you how to set-up your first Dashboard!<br><br> Normally you're starting point is from <i class="fa fa-th fa-fw"></i> Data Visual Gallery. However for now let's pick an online service from below. Make sure you have a valid account and your login credentials at hand!<br><br></h4>
                  </div>                  
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->                     
            
            <div class="row">
                <div class="col-lg-12">
				<?php 	
					$x=0;
					$query = mysqli_query($mysqli, "select * from services where active='1' and getstarted='1' order by name ASC") or die(mysqli_error($mysqli));
					while ($row = mysqli_fetch_array($query)) {						
                    	$id = $row["id"];						
                    	$name = $row["name"];
						$icon = $row["icon"];
						if ($x % 3 == 0) echo "</div>";						
						if (($x==0) || ($x % 3 == 0)) echo "<div class=\"row show-grid\">";						
						echo "<a href=\"getstarted.php?id=$id&name=$name&step=2\"><div class=\"col-md-4 hoverDiv\" align=\"center\"><div><img src=\"../integrations/images/".$icon.".png\" width=\"44\" height=\"44\"></div><h5>$name</h5></div></a>";

						$x++;
					}
					if ($x % 3 == 0) echo "</div>";					
					elseif (($x-2) % 3 == 0) echo "<div class=\"col-md-4\" style=\"min-height:102px\">&nbsp;</div></div>";	
					else echo "<div class=\"col-md-8\" style=\"min-height:102px\">&nbsp;</div></div>";	
                    ?>
              		</div>
                    <br><br>
        	</div>
<?php } ?>           
        </div>				
					
				</div>								
					
				</div>
			<!-- einde pricing -->            

            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
<script>
	function goBack() {
		window.history.back();
	}
	function focuspage(event) {
		window.location.reload();
	}
</script>
</body>

</html>
<?php } ?>