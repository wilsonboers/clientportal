<?php
//import data visual intid
$result = mysqli_query($mysqli,"SELECT clientidKF FROM users WHERE uid='".$_SESSION["uidGO"]."' LIMIT 1");
$gegevens = mysqli_fetch_array($result);
$clientidKF = $gegevens['clientidKF'];

//hoe scoren NL e-commerce sites, geen oauth voor nodig dus andere methode van import
if ($intid=="94e64671cdad3b8a72941d432fcfc29d") {
	$h=md5($clientidKF);
	$e="uid=".$_SESSION["uidGO"]."&h=$h";
	//post de user naar db zodat er controle plaatsvindt en gebruikers niet bij elkaar kunnen kijken
	$url_tp = "http://developersuite.nl/Trustpilot/connect.php";
	post_without_wait($url_tp, $e);
	  
	$curl = new GoDashboard("4ee7578e56bf1e12452bd922ea6042d0954b1462");
	//importeer data visual	
	$curl->setUrl("https://app.klipfolio.com/api/1.0/klips/". $intid ."/@/import");
	
	$data_array = array(	
		"client_id" 	=> 		"$clientidKF"
	);
	
	$data = json_encode($data_array);
	
	$curl->setCurlPost($data);
	
	$curl->parseJson();
	
	$kidlocation = $curl->data_string->meta->location; //get new data visual id
	$kidarray = explode("/", $kidlocation);
	$kidKF = end(array_values($kidarray));
	//end importeer data visual
	
	//get schema new data visual
	$curl->setUrl("https://app.klipfolio.com/api/1.0/klips/". $kidKF ."/schema");
	
	$curl->setCurlGet();
	
	$curl->parseJson();
	
	$schema = $curl->data_string->data->schema;	
	$string = json_encode($schema);
	$data = preg_replace("/uid=55/","".$e."",$string);
	
	//edit data visual change url
	//PUT /klips/{id}/schema    
	  $curl->setUrl("https://app.klipfolio.com/api/1.0/klips/". $kidKF ."/schema");
										  
	  $curl->setCurlPut($data);
	
	  $curl->parseJson();
	//end get new data visual datasources ids	  
	//end import Acces Token TFC into library
}
else {
	//check if timezone etc has been added before
	$result = mysqli_query($mysqli,"SELECT tz, add_min FROM trustpilot WHERE uid='".$_SESSION["uidGO"]."' LIMIT 1");
	$gegevens = mysqli_fetch_array($result);
	$tz = $gegevens['tz'];
	$min = $gegevens['add_min'];
	$hash = md5("".$_SESSION["uidGO"]."".$tz."");
		
	if ($tz==NULL) {
		//select a random timezone + min to add
		$input = array("act", "agt", "bet", "cat", "eet", "ist", "pst");
		$tz = $input[array_rand($input)];
		$min = rand(100, 999);
		$hash = md5("".$_SESSION["uidGO"]."".$tz."");
	
		mysqli_query($mysqli,"UPDATE trustpilot SET tz='".$tz."', add_min='".$min."' WHERE uid='".$_SESSION["uidGO"]."'") or die(mysqli_error($mysqli));
	}
	
	$e="uid=".$_SESSION["uidGO"]."&h=".$hash."&e={date.tz('".$tz."').addMinutes(".$min.").format('epochTime')}000";		  //import klip Acces Token TFC into library
	
	//$intid = "05a3b5e8d0b813dc14515c6e3b51604b"; //for test use this data visual
	
	$curl = new GoDashboard("4ee7578e56bf1e12452bd922ea6042d0954b1462");
	//importeer data visual	
	$curl->setUrl("https://app.klipfolio.com/api/1.0/klips/". $intid ."/@/import");
	
	$data_array = array(	
		"client_id" 	=> 		"$clientidKF"
	);
	
	$data = json_encode($data_array);
	
	$curl->setCurlPost($data);
	
	$curl->parseJson();
	
	$kidlocation = $curl->data_string->meta->location; //get new data visual id
	$kidarray = explode("/", $kidlocation);
	$kidKF = end(array_values($kidarray));
	//end importeer data visual
	  
	//get new data visual datasources ids
	$curl->setUrl("https://app.klipfolio.com/api/1.0/klips/". $kidKF ."/schema");
	
	$curl->setCurlGet();
	
	$curl->parseJson();
	
	$dsarray = $curl->data_string->data->schema->workspace->datasources; //get new datasources
	
	foreach ($dsarray as $dsvalue) {
		//lees datasource in en pas de datasource aan
		$curl->setUrl("https://app.klipfolio.com/api/1.0/datasources/". $dsvalue ."/properties");
	
		$curl->setCurlGet();
	
		$curl->parseJson();
	
		$dsurl = $curl->data_string->data->properties->endpoint_url; //get url
		
		//split on ? save first part change second part
		list($url,$variabelen) = split("\?",$dsurl,2);
		list($user,$resource_path) = split("\&",$variabelen);
		$new_url = $url."?".$e."&".$resource_path;
		
		//update the datasource properties
		$curl->setUrl("https://app.klipfolio.com/api/1.0/datasources/". $dsvalue ."/properties");
		
		$arr = array();
		$arr["endpoint_url"] = $new_url;
		$properties = (object) $arr;
							
		$data_array = array(
			"properties"		=>	$properties
		);
		$data = json_encode($data_array);
								
		$curl->setCurlPut($data);
	
		$curl->parseJson();
		
		//refresh datasource, zodat deze de data van de user toont, de refresh gaat in de wachtrij en kan dus even duren
		$curl->setUrl("https://app.klipfolio.com/api/1.0/datasource-instances/". $dsvalue ."/@/refresh");
		
		$data_array = array(	
			"client_id" 	=> 		"$clientidKF"
		);
		
		$data = json_encode($data_array);
		
		$curl->setCurlPost($data);
		
		$curl->parseJson();
	} 
	//end get new data visual datasources ids
	//trustpilot reviews + reply functie moet ook op inhoud worden bewerkt
	if ($intid=="3e43ad470c229495de9c08ddd2f59937") {
		//get schema new data visual
		$curl->setUrl("https://app.klipfolio.com/api/1.0/klips/". $kidKF ."/schema");
		
		$curl->setCurlGet();
		
		$curl->parseJson();
		
		$schema = $curl->data_string->data->schema;	
		$string = json_encode($schema);
		$data = preg_replace("/uid=55/","uid=".$_SESSION["uidGO"]."",$string);
		
		//edit data visual change url
		//PUT /klips/{id}/schema    
		  $curl->setUrl("https://app.klipfolio.com/api/1.0/klips/". $kidKF ."/schema");
											  
		  $curl->setCurlPut($data);
		
		  $curl->parseJson();
	}
}
$starturl = "klips/addKlip/$kidKF";	

?>

