<style>
.nav {
	padding-top:10px;
	padding-right:30px;
}
</style>
<?php 
$query = mysqli_query($mysqli, "select account, registration, endDate, reactivated from users where uid='".$_SESSION["uidGO"]."'") or die(mysqli_error($mysqli));
$row = mysqli_fetch_array($query);
$account = $row["account"];
$endDate = $row["endDate"];
$reactivated = $row["reactivated"];

if ($account=="FREE") { 
	$registration = strtotime($row["registration"]);
	$now = time(); // or your date as well
	$datediff = $now - $registration;
	$exp = 14 - round($datediff / (60 * 60 * 24));
?>            
			<ul class="nav navbar-top-links navbar-right">
                    <a class="btn btn-primary btn-sm" href="editor.php" target="editor" title="This will open a new browser tab to the dashboard editor">
                        <i class="fa fa-external-link"></i> Open dashboard editor
                    </a>
            </ul>
<?php
}                           
elseif ($endDate!="") { 
	$registration = strtotime($row["endDate"]);
	$now = time(); // or your date as well
	$datediff = $registration - $now;
	$exp = round($datediff / (60 * 60 * 24));
	$rea_page = "plans/reactivate.php?p=".basename($_SERVER['PHP_SELF'])."";	 
?>
			<ul class="nav navbar-top-links navbar-right">
                    <a class="btn btn-primary btn-sm" href="editor.php" target="editor" title="This will open a new browser tab to the dashboard editor">
                        <i class="fa fa-external-link"></i> Open dashboard editor
                    </a>
            </ul>
<?php 
}
else {
	$pquery = mysqli_query($mysqli, "select * from plans where uid='".$_SESSION["uidGO"]."' order by id DESC LIMIT 1") or die(mysqli_error($mysqli));
	$prow = mysqli_fetch_array($pquery);
	$pplan = $prow["plan"];
	$pperiod = $prow["period"];	 
	if ($pperiod=="year") $pperiod = "Annual";	 
	elseif ($pperiod=="month") $pperiod = "Monthly";	 
?>
			<ul class="nav navbar-top-links navbar-right">
                    <a class="btn btn-primary btn-sm" href="editor.php" target="editor" title="This will open a new browser tab to the dashboard editor">
                        <i class="fa fa-external-link"></i> Open dashboard editor
                    </a>
            </ul>
<?php
}
?>