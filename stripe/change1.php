<?php 
// Config includen om met de MySQL database verbinding te maken
include("../config.php");

// If you're using Composer, use Composer's autoload:
require_once('../../stripe/init.php');

// Be sure to replace this with your actual test API key
// (switch to the live key later)
\Stripe\Stripe::setApiKey("sk_live_IyZCez0TNnkBlak8k5qvVXvJ");

if ($_POST["change1"]!="1") exit;
else {
	$uid = $_POST["uid"];
	$amount = $_POST["amount"];
	$plan = $_POST["plan"];
	$startdate = strtotime($_POST["startdate"]);
	$now = time();
		
	$qy = mysqli_query($mysqli, "SELECT t1.customerid_stripe, t2.subscriptionid FROM users as t1, stripe_subscriptions as t2 WHERE t1.uid='$uid' and t1.subid=t2.id and active='1'") or die(mysqli_error($mysqli));
	$gegevens = mysqli_fetch_array($qy);
	
	$customerid = $gegevens['customerid_stripe']; 	  
	$subscriptionid = $gegevens['subscriptionid']; 
	
	//end subscription
	try
	{
		$subscription = \Stripe\Subscription::retrieve("$subscriptionid");
		$subscription->cancel();
		
		mysqli_query($mysqli,"UPDATE stripe_subscriptions SET active='0' WHERE subscriptionid='$subscriptionid'") or die(mysqli_error($mysqli));
	}
	catch(Exception $e)
	{	
		mysqli_query($mysqli, "INSERT INTO stripe_errors (email,message) VALUES ('$uid','".urlencode($e->getMessage())."')") or die(mysqli_error($mysqli));

		die("error 1");
	}	
	
	//bill one-off payment
	try
	{
		\Stripe\Charge::create(array(
		  "amount" => $amount,
		  "currency" => "eur",
		  "customer" => "$customerid", // obtained with Stripe.js
		  "statement_descriptor" => "GODASHBOARD"
		));		
	}	
	catch(Exception $e)
	{	
		mysqli_query($mysqli, "INSERT INTO stripe_errors (email,message) VALUES ('$uid','".urlencode($e->getMessage())."')") or die(mysqli_error($mysqli));

		die("error 2");
	}
	
	//asign to new subscription
	try
	{
		$subscription = \Stripe\Subscription::create(array(
		  "customer" => "$customerid",
		  "plan" => "$plan",  	  
		  "trial_end" => "$startdate" //timestamp startdate
		));
		  
		$logData = serialize($customer);
		mysqli_query($mysqli, "INSERT INTO log (log) VALUES ('$logData')");
		
		$subscriptionid = $subscription["id"];
		
		mysqli_query($mysqli, "INSERT INTO stripe_subscriptions (subscriptionid) VALUES ('$subscriptionid')") or die(mysqli_error($mysqli));
		$subid = mysqli_insert_id($mysqli);
		
		mysqli_query($mysqli,"UPDATE users SET subid='$subid' WHERE uid='$uid'");
  
		echo "success";	
	}
	catch(Exception $e)
	{
		mysqli_query($mysqli, "INSERT INTO stripe_errors (email,message) VALUES ('$uid','".$e->getMessage()."')") or die(mysqli_error($mysqli));
		
		die("error 3");
	}		
}
?>