<?php
include("config.php");			

$login = "index.php";
$id = $_GET["id"];
$add = $_GET["add"];

//onderstaande was indien er een data visual vanuit de gallery was geselecteerd, momenteel uitgezet
/*if ($add!="") $template=$add;
else {
	$query = mysqli_query($mysqli, "select url from KFintegrations where id='$id'") or die(mysqli_error($mysqli));
	$row = mysqli_fetch_array($query);						
	$url = explode("=", $row["url"]);
	$template = end(array_values($url));
}*/

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Confirm</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
.login-panel {
    margin-top: 10% !important;
}
.margintop {
    margin-top: 5% !important;
}
</style>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-80320540-1','auto');ga('send','pageview');</script>
</head>
<body>
<?php 
if ((isset($_GET["uname"])) and (isset($_GET["cid"]))) {
	$uname = base64_decode($_GET["uname"]);
	$cid = $_GET["cid"];
	$qy = mysqli_query($mysqli, "SELECT uid FROM users WHERE uid='$cid' and uname='$uname' and confirmed='0'");  
	$numrows = mysqli_num_rows($qy);
	if ($numrows == 0) {
		$result='<br><div class="alert alert-danger">Username  / id not found or already confirmed.</div>';
	}
	else {
		  // Sessie registreren 
		  $gegevens = mysqli_fetch_array($qy);
	  
		  $_SESSION['uidGO'] = $gegevens['uid']; 
		  $_SESSION['unameGO'] = $uname; 
		  if ($template!="") $_SESSION['templateGO'] = $template;
		  	  
		  mysqli_query($mysqli,"UPDATE users SET ingelogd=ingelogd+1,lastLogin='".date("Y-m-d H:i:s")."', confirmed='1' WHERE uname='$uname'");

	  	  //start ActiveCam
		  $ac_url = 'https://iventure.api-us1.com';
		  
		  $ac_params = array(
			  "api_key"      => "8bdd980664a4ae416aafa99123e598956dcfea1c12722e19b50c6d31873a183ad32b188b",
			  "api_action"   => "contact_tag_add",
			  "api_output"   => "serialize"
		  );
		  
		  $ac_post = array(
			  "email"                    => $uname,
			  "tags"               		 => "confirmed"
		  );
		  
		  // This section takes the input fields and converts them to the proper format
		  $ac_query = "";
		  foreach( $ac_params as $ac_key => $ac_value ) $ac_query .= $ac_key . '=' . urlencode($ac_value) . '&';
		  $ac_query = rtrim($ac_query, '& ');
		  
		  // This section takes the input data and converts it to the proper format
		  $ac_data = "";
		  foreach( $ac_post as $ac_key => $ac_value ) $ac_data .= $ac_key . '=' . urlencode($ac_value) . '&';
		  $ac_data = rtrim($ac_data, '& ');
		  
		  // clean up the url
		  $ac_url = rtrim($ac_url, '/ ');
		  
		  // define a final API request - GET
		  $ac_api = $ac_url . '/admin/api.php?' . $ac_query;
		  
		  $ac_request = curl_init($ac_api); // initiate curl object
		  curl_setopt($ac_request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		  curl_setopt($ac_request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		  curl_setopt($ac_request, CURLOPT_POSTFIELDS, $ac_data); // use HTTP POST to send form data
		  curl_setopt($ac_request, CURLOPT_FOLLOWLOCATION, true);
		  curl_setopt($ac_request, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
				  
		  $ac_response = (string)curl_exec($ac_request); // execute curl post and store results in $response
		  
		  // additional options may be required depending upon your server configuration
		  // you can find documentation on curl options at http://www.php.net/curl_setopt
		  curl_close($ac_request); // close curl object

	  	  //end of ActiveCam

		  // Google Code for Proefaccount aangemaakt Conversion Page 
		  echo "<script type=\"text/javascript\">
			  /* <![CDATA[ */
			  var google_conversion_id = 867741973;
			  var google_conversion_language = \"en\";
			  var google_conversion_format = \"3\";
			  var google_conversion_color = \"ffffff\";
			  var google_conversion_label = \"Mkh2CMOqtm4QleLinQM\"; var google_remarketing_only = false;
			  /* ]]> */
			  </script>
			  <script type=\"text/javascript\"  
			  src=\"//www.googleadservices.com/pagead/conversion.js\">
			  </script>
			  <noscript>
			  <div style=\"display:inline;\">
			  <img height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\"  
			  src=\"//www.googleadservices.com/pagead/conversion/867741973/?label=Mkh2CMOqtm4QleLinQM&amp;guid=ON&amp;script=0\"/>
			  </div>
		  </noscript>";
			  
		  // Redirecten naar hoofdmenu.php 
		  echo "<script>
		  window.location = 'account-v2.php';
		  </script>";
		  
		  exit;
	}
}
?>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 margintop" align="center">
              <img src="GoDashboardLogo220x50.png">            
            </div>
        </div>        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <div style="width:100%"><?php echo $result; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
     
</body>

</html>                    